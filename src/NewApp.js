import React, { Component } from "react";
import { Switch, BrowserRouter as Router } from "react-router-dom";
import { connect } from "react-redux";

import VerticalLayout from "./components/VerticalLayout/index";
import HorizontalLayout from "./components/HorizontalLayout/index";

import { authProtectedRoutes} from "./routes/";
import AppRoute from "./routes/route";


// Import scss
import "./assets/scss/theme.scss";
import Rightbar from "./components/CommonForBoth/Rightbar";
import Login from "./pages/Login";
import NonAuthLayout from "./NonAuthLayout";

class NewApp extends Component {
	constructor(props) {
		super(props);
        this.state = {  };
        this.getLayout = this.getLayout.bind(this);
    }

    getLayout = () => {
		let layoutCls = VerticalLayout;

		switch (this.props.layout.layoutType) {
			case "horizontal":
				layoutCls = HorizontalLayout;
				break;

				case "zoom":
				layoutCls =  VerticalLayout
				break;
			default:
				layoutCls = VerticalLayout;
				break;
		}
		return layoutCls;
	};


	render() {
     		const Layout = this.getLayout(); 

		return (			
            <React.Fragment>

			{authProtectedRoutes.map((route, idx) => (
				<AppRoute
					path={route.path}
					layout={NonAuthLayout}
					component={route.component}
					key={idx}
					isAuthProtected={true}
				/>
			))}
			</React.Fragment>
		);
	}
}

const mapStateToProps = state => {
	return {
		layout: state.Layout
	};
};

export default connect(mapStateToProps, null)(NewApp);
