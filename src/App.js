import React, { Component } from "react";
import { Switch, BrowserRouter as Router, Redirect } from "react-router-dom";
import { connect } from "react-redux";

import VerticalLayout from "./components/VerticalLayout/index";
import HorizontalLayout from "./components/HorizontalLayout/index";

import { authProtectedRoutes, publicRoutes, authProtectedRoutesClient } from "./routes/";
import AppRoute from "./routes/route";


// Import scss
import "./assets/scss/theme.scss";
import Rightbar from "./components/CommonForBoth/Rightbar";
import NonAuthLayout from "./NonAuthLayout";

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isAuthenticated:false,
			UserDetails : []
		  };
		this.getLayout = this.getLayout.bind(this);
	}

	componentDidMount(){
	var isAuthenticated=JSON.parse(localStorage.getItem('loginedUser'));


	// console.log(isAuthenticated[0])

	if(isAuthenticated != null){
		this.setState({...this.state,
			isAuthenticated:isAuthenticated==null?false:true,
			UserDetails : isAuthenticated[0]
			})
	}
	

}
 
 	/**
	 * Returns the layout
	 */
	getLayout = () => {
		let layoutCls = VerticalLayout;

		switch (this.props.layout.layoutType) {
			case "horizontal":
				layoutCls = HorizontalLayout;
				break;

				case "zoom":
				layoutCls =  VerticalLayout
				break;
			default:
				layoutCls = VerticalLayout;
				break;
		}
		return layoutCls;
	};



	render() {
		const Layout = this.getLayout();

		return (
			<React.Fragment>

			<Router>
					<Switch>
						{/* {publicRoutes.map((route, idx) => (
						
							<AppRoute
								path={route.path}
								layout={NonAuthLayout}
								component={route.component}
								key={idx}
								isAuthProtected={false}
							/>
						))} */}

{this.state.isAuthenticated?
					this.state.UserDetails.fld_userid != undefined ? authProtectedRoutes.map((route, idx) => (
							<AppRoute
								path={route.path}
								layout={Layout}
								component={route.component}
								key={idx}
								isAuthProtected={true}
							/>
							)) : 
							authProtectedRoutesClient.map((route, idx) => (
								<AppRoute
									path={route.path}
									layout={Layout}
									component={route.component}
									key={idx}
									isAuthProtected={true}
								/>
								))
							:
							publicRoutes.map((route, idx) => (
						
								<AppRoute
									path={route.path}
									layout={NonAuthLayout}
									component={route.component}
									key={idx}
									isAuthProtected={false}
								/>
							))
						}
					</Switch>
				</Router>
			</React.Fragment>
		);
	}
}

const mapStateToProps = state => {
	return {
		layout: state.Layout
	};
};

export default connect(mapStateToProps, null)(App);
