import React, { Component } from "react";
import { Container, Row, Col, Button, Card, CardBody, CardTitle, CardSubtitle, Modal, ModalHeader, ModalBody, ModalFooter, Media, Table } from "reactstrap";
import { Link, Redirect } from "react-router-dom";
import { MDBDataTable } from "mdbreact";
//Import Breadcrumb
import Breadcrumbs from '../components/Common/Breadcrumb';
import GetApiCall from '../API/GetAPI'
import PostApiCall from '../API/PostAPI'
import Notiflix from "notiflix";

//i18n
import { withNamespaces } from 'react-i18next';
import GridTables from "./Dashboard/GridTables";

class RegisterCounstomer extends Component {
    constructor(props) {
        super(props);
        this.state = {
          allDetails:[],
        };
    }

    componentDidMount=()=>{
      Notiflix.Loading.Init({
        svgColor : '#880105'
       
      });
      
      Notiflix.Loading.Dots('Please wait...'); 
     
      GetApiCall.getRequest("Get_MonthlyClientPending").then(resultdes =>
        resultdes.json().then(obj => {
          // console.log(obj.data)
            this.setState({
                allDetails:obj.data
        })
        Notiflix.Loading.Remove();
     
      }))


    }


    view=(x)=>{
      return(
        localStorage.setItem('Registered',[JSON.stringify(x)]),
        this.props.history.push('./registration-approval')
      )}

    render()  {


          let rowData=  this.state.allDetails.map(info=>{
            return{ name:info.fld_name,
             mobNo:info.fld_mobile,
             email:info.fld_email,
             id:info.fld_id}
         })  
         
      
              const data = {
                  columns: [
                    {
                      label: "Name",
                      field: "name",
                      sort: "asc",
                      width: 150
                    },
                    {
                      label: "Mobile Number",
                      field: "mobNo",
                      sort: "asc",
                      width: 100
                    },
                    {
                      label: "Email",
                      field: "email",
                      sort: "asc",
                      width: 150
                    },
                    {
                      label: "Action",
                      field: "action",
                      sort: "asc",
                      width: 100
                    }
                  ],
                  rows:
                   rowData.map(x=>{return{ 
                     name:x.name,
                     mobNo:x.mobNo,
                     email:x.email,
                     id:x.id,
                     action: <div><i className="bx bx-show" onClick={()=>this.view(x)} style={{fontSize:"22px"}}></i>
                            </div>
                  }})
                };
        return (
            <React.Fragment>
                <div className="page-content">
                    <Container fluid>
                          <div className="px-4">
                            <Breadcrumbs title={this.props.t('Registered Customers')} breadcrumbItem={this.props.t('Registered Customers')} />
                          </div>                        
                        <React.Fragment>
                        <div className="page-content pt-0 m-0">
                          <div className="container">
                
                            <Row>
                              <Col className="col-12">
                                <Card>
                                  <CardBody>
                                    <CardTitle>List of Registered Customer </CardTitle>
                                    <CardSubtitle className="mb-3">
                                        Click on any particular Customer to see all the chits related to them to edit, approve or reject the chit.
                                    </CardSubtitle>
                
                                    <MDBDataTable responsive striped bordered data={data} />
                
                                  </CardBody>
                                </Card>
                              </Col>
                            </Row>
                          </div>
                        </div>
                      </React.Fragment>
                


                        </Container>
                </div>
          
                </React.Fragment>
        );
    }
}

export default withNamespaces()(RegisterCounstomer);




// const data = {
//   columns: [
//     {
//       label: "Name",
//       field: "name",
//       sort: "asc",
//       width: 150
//     },
//     {
//       label: "User Name",
//       field: "user",
//       sort: "asc",
//       width: 270
//     },
//     {
//       label: "Date",
//       field: "date",
//       sort: "asc",
//       width: 200
//     },
//     {
//       label: "Email",
//       field: "email",
//       sort: "asc",
//       width: 200
//     },
//     {
//       label: "Action",
//       field: "action",
//       sort: "asc",
//       width: 200
//     }
//   ],
//   rows: [
//     {
//       name: "Tiger Nixon",
//       user: "System@Architect",
//       date: "2011/04/25",
//       email: "abc@gmail.com",
//       action:<div><i className="bx bx-show" style={{fontSize:"22px"}}></i></div>,
//       clickEvent:() =>{this.view()},
//     },
//     {
//       name: "Tiger Nixon",
//       user: "System@Architect",
//       date: "2011/04/25",
//       email: "abc@gmail.com"
//     },
//     {
//       name: "Tiger Nixon",
//       user: "System@Architect",
//       date: "2011/04/25",
//       email: "abc@gmail.com"
//     },
//     {
//       name: "Tiger Nixon",
//       user: "System@Architect",
//       date: "2011/04/25",
//       email: "abc@gmail.com"
//     },
//     {
//       name: "Tiger Nixon",
//       user: "System@Architect",
//       date: "2011/04/25",
//       email: "abc@gmail.com"
//     },
//     {
//       name: "Tiger Nixon",
//       user: "System@Architect",
//       date: "2011/04/25",
//       email: "abc@gmail.com"
//     }
//   ]
// };
