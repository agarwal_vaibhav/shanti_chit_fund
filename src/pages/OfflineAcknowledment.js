import React, { Component } from "react";
import { Row, Col, Card, CardBody, FormGroup, Button, CardTitle, CardSubtitle, Label, Input ,Container, UncontrolledTooltip, Table} from "reactstrap";
import { Link } from "react-router-dom";
import { AvForm, AvField } from "availity-reactstrap-validation";
import PostApiCall from '../API/PostAPI';
import Notiflix from "notiflix";
import moment from 'moment'


//Import Breadcrumb
import Breadcrumbs from '../components/Common/Breadcrumb';

//i18n
import { withNamespaces } from 'react-i18next';

class OfflinePayment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            installment:'',
            accountNo:'',
            weight:'',
            email:''
        };
    }

    installmentChange=(e)=>{
        this.setState({
            installment :e.target.value
        })
    }

    accountHandler=(e)=>{
        this.setState({
            accountNo:e.target.value
        })
    }
    
    mailHandler=(e)=>{
        this.setState({
            email:e.target.value
        })
    }

    WeightHandler=(e)=>{
        this.setState({
            weight:e.target.value
        })
    }
    
    handleSubmit=()=>{
        if(this.state.installment==''){            
            document.getElementById('installmentVal').innerHTML="Enter Your Installment Number"
            document.getElementById("installmentDiv").style.borderColor = "#f46a6a"
        }else{
            document.getElementById('installmentVal').innerHTML=""
            document.getElementById("installmentDiv").style.borderColor = ""

            Notiflix.Loading.Dots('Please wait...');
            PostApiCall.postRequest({
                accountNo:this.state.accountNo,
                InastallmentNo:this.state.installment,
                Weight:this.state.weight,
                email:this.state.email
            }, "Mailer").then((result)=>
                result.json().then(obj => {
                if(result.status == 200 || result.status == 201){
                   
                    console.log(obj)     
                    Notiflix.Notify.Success('Payment Completed.')
                    Notiflix.Loading.Remove();
                    window.location.reload()
                  
                }else
                {
                  Notiflix.Notify.Failure('Something went wrong, try again later.')
                }
            })
            )

        }
    }


    render() {
        return (
            <React.Fragment>
                <div className="page-content">
                    <Container fluid>

                        {/* Render Breadcrumb */}
                        <Breadcrumbs title={this.props.t('Offline Payment')} breadcrumbItem={this.props.t('Acknowledgement')} />
                    
                        <Card>
                                    <CardBody>
                                        <AvForm className="needs-validation" >
                                            <Row>
                                                <Col md="6">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">Enter Account Number</Label>
                                                        <AvField
                                                          name="Account"
                                                          placeholder="Account Number"
                                                          type="text"
                                                          errorMessage="Enter Valid Account Number"
                                                          className="form-control"
                                                          validate={{ required: { value: true } }}
                                                          id="validationCustom01"
                                                          value={this.state.accountNo}
                                                          onChange={this.accountHandler}
                                                        />
                                                    </FormGroup>
                                                </Col>
                                                <Col md="6">
                                                <FormGroup>
                                                    <Label htmlFor="validationCustom01">Email-Id</Label>
                                                    <AvField
                                                      name="mail"
                                                      placeholder="Email-Id"
                                                      type="email"
                                                      errorMessage="Enter valid Email-Id"
                                                      className="form-control"
                                                      validate={{ required: { value: true } }}
                                                      id="validationCustom01"
                                                      value={this.state.email}
                                                      onChange={this.mailHandler}
                                                    />
                                                </FormGroup>
                                                </Col>  
                                            </Row>

                                            <Row>
                                                
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom03">Insatllment Number</Label>
                                                        <select class="form-control" value={this.state.installment} onChange={this.installmentChange} id="installmentDiv"> 
                                                        <option>Select Installment Number</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5</option>
                                                        <option>6</option>
                                                    </select>
                                                    <p id="installmentVal" style={{color:'#f46a6a', fontSize:"80%"}}></p>
                                                    </FormGroup>
                                                </Col>  
                                                <Col md="4">
                                                <FormGroup>
                                                    <Label htmlFor="validationCustom01">Weight</Label>
                                                    <AvField
                                                      name="Weight"
                                                      placeholder="Weight"
                                                      type="number"
                                                      errorMessage="Enter Any Valid Weight"
                                                      className="form-control"
                                                      validate={{ required: { value: true } }}
                                                      id="validationCustom01"
                                                      value={this.state.weight}
                                                      onChange={this.WeightHandler}
                                                    />
                                                </FormGroup>
                                                </Col>                                 
                                            </Row>
                                                                                       
                                            <button type="submit" onClick={this.handleSubmit} className="btn"
                                             style={{backgroundColor:'#880105', color:'#e1b761', float:"right", marginRight:'2%'}}>Send Acknowledgement</button>                         
                                        </AvForm>
                                    </CardBody>
                                </Card>
                    </Container>
                        
                </div>
          
                </React.Fragment>
        );
    }
}

export default withNamespaces()(OfflinePayment);
