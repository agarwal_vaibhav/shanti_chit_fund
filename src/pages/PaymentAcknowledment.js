import React, { Component } from "react";
import { Row, Col, Card, CardBody, FormGroup, Button, CardTitle, CardSubtitle, Label, Input ,Container, UncontrolledTooltip, Table} from "reactstrap";
import { Link } from "react-router-dom";
import { AvForm, AvField } from "availity-reactstrap-validation";



//Import Breadcrumb
import Breadcrumbs from '../components/Common/Breadcrumb';

//i18n
import { withNamespaces } from 'react-i18next';

class PaymentAcknowledment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            installment:''
        };
    }


    dataDropdown=(e)=>{
        this.setState({
            installment:e.target.value
        })
    }

    submitHandler=()=>{
    if(this.state.installment==''){
        document.getElementById('instalmentVal').innerHTML="Select Your Installment Number"
        document.getElementById("installment").style.borderColor = "#f46a6a"           
    }
    }

    render() {
        return (
            <React.Fragment>
                <div className="page-content">
                    <Container fluid>

                        {/* Render Breadcrumb */}
                        <Breadcrumbs title={this.props.t('Daily Collection')} breadcrumbItem={this.props.t('Payment Acknowledgement')} />
                    
                        <Card>
                                    <CardBody>
                                        <AvForm className="needs-validation" >
                                            <Row>
                                                <Col md="12">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">Enter Account Number</Label>
                                                        <AvField
                                                          name="Account"
                                                          placeholder="Account Number"
                                                          type="text"
                                                          errorMessage="Enter Valid Account Number"
                                                          className="form-control"
                                                          validate={{ required: { value: true } }}
                                                          id="validationCustom01"
                                                        />
                                                    </FormGroup>
                                                </Col>
                                                
                                            </Row>

                                            <Row>
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">Amount To Pay</Label>
                                                        <AvField
                                                        name="Amount"
                                                        placeholder="Amount "
                                                        type="number"
                                                        errorMessage="Enter Valid Amount To Pay"
                                                        className="form-control"
                                                        validate={{ required: { value: true } }}
                                                        id="validationCustom01"
                                                        />
                                                    </FormGroup>
                                                </Col>   
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">Payment Date</Label>
                                                        <AvField
                                                        name="date"
                                                        placeholder="date"
                                                        type="date"
                                                        errorMessage="Enter The Payment Date"
                                                        className="form-control"
                                                        validate={{ required: { value: true } }}
                                                        id="validationCustom01"
                                                        />
                                                    </FormGroup>
                                                </Col>    
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">Payment Mode</Label>
                                                        <AvField
                                                        name="Mode"
                                                        placeholder="Payment Mode"
                                                        type="text"
                                                        errorMessage="Enter The Payment Mode"
                                                        className="form-control"
                                                        validate={{ required: { value: true } }}
                                                        id="validationCustom01"
                                                        />
                                                    </FormGroup>
                                                </Col>                                       
                                            </Row>

                                            <Row>
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">Current Gold Rate</Label>
                                                        <AvField
                                                        name="GoldRate"
                                                        placeholder="Current Gold Rate"
                                                        type="number"
                                                        errorMessage="Enter the current gold rate"
                                                        className="form-control"
                                                        validate={{ required: { value: true } }}
                                                        id="validationCustom01"
                                                        />
                                                    </FormGroup>
                                                </Col>   
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">Consolidated Weight</Label>
                                                        <AvField
                                                        name="Consolidated"
                                                        placeholder="Consolidated Weight"
                                                        type="number"
                                                        errorMessage="Enter Your Consolidated Weight"
                                                        className="form-control"
                                                        validate={{ required: { value: true } }}
                                                        id="validationCustom01"
                                                        />
                                                    </FormGroup>
                                                </Col>    
                                                <Col md="4">
                                                <FormGroup>
                                                    <Label htmlFor="validationCustom03">Insatllment Number</Label>
                                                    <select class="form-control"  id="installment" value={this.state.installment}  onChange = {this.dataDropdown}>
                                                    <option>Select Installment Number</option>
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                    <option>6</option>
                                                </select>
                                                <p id="instalmentVal" style={{color:'#f46a6a', fontSize:"80%"}}></p>
                                                </FormGroup>
                                            </Col>  
                                                                                    
                                            </Row>
                                                                                       
                                            <button type="submit" onClick={this.submitHandler} 
                                            style={{float:'right',  marginRight:'2%',backgroundColor:'#880105', color:'#e1b761'}}>Send Acknowledgement</button>
                                           
                                        </AvForm>
                                    </CardBody>
                                </Card>
                    </Container>
                        
                </div>
          
                </React.Fragment>
        );
    }
}

export default withNamespaces()(PaymentAcknowledment);








 