import React,{Component} from 'react'
import { Card,CardBody ,Container,Row,Col,FormGroup,Label,Input} from 'reactstrap'
import { AvForm, AvField } from "availity-reactstrap-validation";
import Breadcrumbs from '../components/Common/Breadcrumb';



class AddStaff extends Component {
    state={
        fname:'',
        lname:'',
        PermanentSame:false,
        name:'',
        email:'',
        mobile:'',
        gender:'male',
        dob:'',
        pan:'',
        address:'',
        country:'',
        staate:'',
        city:'',
        designation:'',
        department:'',
        addhar:'',
        pincode:'',
        touched1:false,
        touched2:false,
        touched3:false,
        touched4:false,
        touched5:false,
        touched6:false,
        touched7:false,
        touched8:false,
        touched9:false,
        touched10:false,
        touched11:false,
        touched12:false,
        touched13:false,
        touched14:false,
        touched15:false,
        
    }


    onChange=(name)=>e=>{
        this.setState({...this.state,[name]:e.target.value})
    }
  render(){
    return (
        <React.Fragment>
        <div className="page-content">
            <Container fluid>

                {/* Render Breadcrumb */}
                <Breadcrumbs title={'Manage Staffs'} breadcrumbItem={'Add New Staff'} />
                <AvForm className="needs-validation" >
                <Card>
                     <h4 className={"pt-2 pl-4 mb-0 mt-2"}>Personal Information</h4>
                     <p className={"ml-4"}>Enter Personal Information Of Staff</p>
                            <CardBody>


                        <form >
                            <div className='row'>
                            <div className='col col-6'>
                        <div className="form-group">
                        <label  htmlFor="validationCustom01">First Name<span style={{color:'red'}}>*</span></label>
                        <input 
                    
                        name='fname'
                         placeholder="Enter First Name"
                         type="text"
                         className={`form-control ${this.state.touched1&&this.state.fname===''?'is-invalid':""}`}
                         value={this.state.fname}
                         onChange={this.onChange('fname')}
                         onBlur={e=>{
                             this.setState({...this.state,
                            touched1:true
                            })
                         }}
                      />
                  
                  
                  {this.state.touched1&&this.state.fname===''&&<small id="emailHelp" class="form-text text-danger">Please Enter your First Name</small>}
                        </div>
                        </div>
                           
                           
                           
                           
                            <div className='col col-6'>
                        <div className="form-group">
                        <label  htmlFor="validationCustom01">Last Name<span style={{color:'red'}}>*</span></label>
                        <input 
                    
                        name='lname'
                        placeholder="Enter Your Last Name"
                        type="text"
                        className={`form-control ${this.state.touched2&&this.state.lname===''?'is-invalid':""}`}
                        value={this.state.lname}
                        onBlur={
                            e=>{
                                this.setState({...this.state,
                                touched2:true})
                            }
                        }
                        onChange={this.onChange('lname')}
                         
                         
                      />

                  {this.state.touched2&&this.state.lname===''&&<small id="emailHelp" class="form-text text-danger">Please Enter your Last Name</small>}

                        </div>
                        </div>


                            <div className='col col-12'>
                        <div className="form-group">
                        <label  htmlFor="validationCustom01">Email<span style={{color:'red'}}>*</span></label>
                        <input 
                    
                    placeholder="xyz@gmail.com"
                    type="email"
                    
                    className={`form-control ${this.state.touched3&&this.state.email===''?'is-invalid':""}`}
                    validate={{ required: { value: true,errorMessage:"Enter Valid email" } }}
                    value={this.state.email}
                    onBlur={
                        e=>{
                            this.setState({...this.state,
                            touched3:true})
                        }
                    }
                    onChange={this.onChange('email')}
                         
                         
                      />

                  {this.state.touched3&&this.state.email===''&&<small id="emailHelp" class="form-text text-danger">Please Enter Valid email</small>}

                        </div>
                        </div>
                            <div className='col col-6'>
                        <div className="form-group">
                        <label  htmlFor="validationCustom01">Mobile<span style={{color:'red'}}>*</span></label>
                                                      <input 
                                                          name="mobile"
                                                          placeholder="Enter Your Mobile Number"
                                                          type="number"
                                                         className={`form-control ${this.state.touched4&&this.state.mobile===''?'is-invalid':""}`}
                                                          value={this.state.mobile}
                                                          onBlur={
                                                              e=>{
                                                                  this.setState({...this.state,
                                                                touched4:true})
                                                              }
                                                          }
                                                          onChange={this.onChange('mobile')}
                                                         />
                  {this.state.touched4&&this.state.mobile===''&&<small id="emailHelp" class="form-text text-danger">Please Enter Your Mobile Number</small>}

                        </div>
                        </div>
                    <div className='col col-6'>
                <div className="form-group">
                <label  htmlFor="validationCustom01">Gender<span style={{color:'red'}}>*</span></label>
                <br/>  <span className='my-auto'> <div class="form-check form-check-inline">
                    <input class="form-check-input " type="radio"
                    checked={this.state.gender=='male'}
                    onChange={e=>{
                        this.setState({...this.state,
                        gender:'male'})
                    }} name="exampleRadios" id="exampleRadios1" value="option1" />
                    <label class="form-check-label" for="exampleRadios1">
                        Male
                    </label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input
                    checked={this.state.gender=='female'}
                    
                    onChange={e=>{
                        this.setState({...this.state,
                        gender:'female'})
                    }} class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2" />
                    <label class="form-check-label" for="exampleRadios2">
                        Female
                    </label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input
                    checked={this.state.gender=='others'}
                    
                    onChange={e=>{
                        this.setState({...this.state,
                        gender:'others'})
                    }} class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3" value="option3" />
                    <label class="form-check-label" for="inlineCheckbox3">Others</label>
                    </div>
                    </span>

                        </div>
                        </div>



                        <div className='col col-6'>
                        <div className="form-group">
                        <label  htmlFor="validationCustom01">DOB<span style={{color:'red'}}>*</span></label>
                                                      <input 
                                                         name="DOB"
                                                         placeholder="Enter Your DOB"
                                                         type="date"
                                                         className={`form-control ${this.state.touched6&&this.state.dob===''?'is-invalid':""}`}
                                                         value={this.state.dob}
                                                         onBlur={
                                                             e=>{
                                                                 this.setState({...this.state,
                                                                touched6:true})
                                                             }
                                                         }
                                                         onChange={this.onChange('dob')}
                                                         />
                  {this.state.touched6&&this.state.dob===''&&<small id="emailHelp" class="form-text text-danger">Please Enter your First Name</small>}

                        </div>
                        </div>
                        <div className='col col-6'>
                        <div className="form-group">
                        <label  htmlFor="validationCustom01">Designation<span style={{color:'red'}}>*</span></label>
                                                      <input 
                                                         name="designation"
                                                         placeholder="Enter Your Designation"
                                                         type="text"
                                                         
                                                         className={`form-control ${this.state.touched7&&this.state.designation===''?'is-invalid':""}`}
                                                         validate={{required: {value: true, errorMessage: 'Enter Designation'}, }}
                                                         
                                                         value={this.state.designation}
                                                         onBlur={
                                                             e=>{
                                                                 this.setState({...this.state,
                                                                touched7:true})
                                                             }
                                                         }
                                                         onChange={this.onChange('designation')}
                                                         />
                  {this.state.touched7&&this.state.designation===''&&<small id="emailHelp" class="form-text text-danger">Please Enter your First Name</small>}

                        </div>
                        </div>


                        
                        <div className='col col-6'>
                        <div className="form-group">
                        <label  htmlFor="validationCustom01">Department<span style={{color:'red'}}>*</span></label>
                                                      <input 
                                                         name="department"
                                                         placeholder="Enter Your Department"
                                                          type="text"
                                                         
                                                          className={`form-control ${this.state.touched8&&this.state.department===''?'is-invalid':""}`}
                                                          validate={{ required: { value: true , errorMessage:"Enter Department"} }}
                                                          value={this.state.department}
                                                          onBlur={
                                                              e=>{
                                                                  this.setState({...this.state,
                                                                touched8:true})
                                                              }
                                                          }
                                                          onChange={this.onChange('department')}
                                                        />
                  {this.state.touched8&&this.state.department===''&&<small id="emailHelp" class="form-text text-danger">Please Enter your First Name</small>}

                        </div>
                        </div>


                        <div className='col col-6'>
                        <div className="form-group">
                        <label  htmlFor="validationCustom01">Addhar Card<span style={{color:'red'}}>*</span></label>
                                                      <input 
                                                         name="addhar"
                                                         placeholder="Enter Your Addhar Card"
                                                         type="text"
                                                        
                                                         className={`form-control ${this.state.touched9&&this.state.addhar===''?'is-invalid':""}`}
                                                         validate={{ required: { value: true, errorMessage:"Enter Addhar Card" } }}
                                                         value={this.state.addhar}
                                                         onBlur={
                                                             e=>{
                                                                 this.setState({...this.state,
                                                                touched9:true})
                                                             }
                                                         }
                                                         onChange={this.onChange('addhar')}
                                                       />
                  {this.state.touched9&&this.state.addhar===''&&<small id="emailHelp" class="form-text text-danger">Please Enter your First Name</small>}

                        </div>
                        </div>


                        <div className='col col-6'>
                        <div className="form-group">
                        <label  htmlFor="validationCustom01">PAN<span style={{color:'red'}}>*</span></label>
                                                      <input 
                                                          name="pan"
                                                          placeholder="Enter Your Full Name"
                                                          type="text"
                                                          
                                                          className={`form-control ${this.state.touched10&&this.state.pan===''?'is-invalid':""}`}
                                                          validate={{ required: { value: true,errorMessage:"Enter Permanent Address Number" } }}
                                                          value={this.state.pan}
                                                          onBlur={
                                                              e=>{
                                                                  this.setState({...this.state,
                                                                touched10:true})
                                                              }
                                                          }
                                                          onChange={this.onChange('pan')}
                                                         />
                  {this.state.touched10&&this.state.pan===''&&<small id="emailHelp" class="form-text text-danger">Please Enter Permanent Address Number</small>}

                        </div>
                        </div>


                        <div className='col col-6'>
                        <div className="form-group">
                        <label  htmlFor="validationCustom01">Address<span style={{color:'red'}}>*</span></label>
                                                      <input 
                                                          name="address"
                                                          placeholder="Enter Your Address"
                                                          type="text"
                                                          
                                                          className={`form-control ${this.state.touched11&&this.state.address===''?'is-invalid':""}`}
                                                          validate={{ required: { value: true ,errorMessage:"Enter Address"} }}
                                                          value={this.state.address}
                                                          onBlur={
                                                              e=>{
                                                                  this.setState({...this.state,
                                                                touched11:true})
                                                              }
                                                          }
                                                          onChange={this.onChange('address')}
                                                         />
                  {this.state.touched11&&this.state.address===''&&<small id="emailHelp" class="form-text text-danger">Please Enter your Address</small>}

                        </div>
                        </div>


                        <div className='col col-6'>
                        <div className="form-group">
                        <label  htmlFor="validationCustom01">Country<span style={{color:'red'}}>*</span></label>
                                                      <select 
                                                         name="country"
                                                         placeholder="Enter Your Country"
                                                         type="select"
                                                         
                                                         className={`form-control ${this.state.touched12&&this.state.country===''?'is-invalid':""}`}
                                                         validate={{ required: { value: true,errorMessage:"Select Your Country" } }}
                                                         value={this.state.country}
                                                         onBlur={
                                                             e=>{
                                                                 this.setState({...this.state,
                                                                touched12:true})
                                                             }
                                                         }
                                                         onChange={this.onChange('country')}
                                                       >
                                                                    <option value={''} >Select Country</option>
                                                                    <option value={'country 1'} >Country 1</option>
                                                                   <option value={'country 2'}>Country 2</option>
                                                                   <option value={'country 3'}>Country 3</option>
                                                                   <option value={'country 4'}>Country 4</option>
                                                                   <option value={'country 5'}>Country 5</option>
                                                      </select>
                  {this.state.touched12&&this.state.country===''&&<small id="emailHelp" class="form-text text-danger">Please Select Your Country</small>}

                                                        </div>
                                                        </div>


                        <div className='col col-6'>
                        <div className="form-group">
                        <label  htmlFor="validationCustom01">State<span style={{color:'red'}}>*</span></label>
                                                      <select 
                                                         name="State"
                                                         placeholder="Enter Your State"
                                                         type="select"
                                                         
                                                         className={`form-control ${this.state.touched13&&this.state.staate===''?'is-invalid':""}`}
                                                         validate={{ required: { value: true,errorMessage:"Enter " } }}
                                                         value={this.state.staate}
                                                         onBlur={
                                                             e=>{
                                                                 this.setState({...this.state,
                                                                touched13:true})
                                                             }
                                                         }
                                                         onChange={this.onChange('staate')}
                                                       >
                                                                   <option value={''} >Select State</option>
                                                                    <option value={'state 1'} >state 1</option>
                                                                   <option value={'state 2'}>state 2</option>
                                                                   <option value={'state 3'}>state 3</option>
                                                                   <option value={'state 4'}>state 4</option>
                                                                   <option value={'state 5'}>state 5</option>
                
                                                         </select>
                  {this.state.touched13&&this.state.staate===''&&<small id="emailHelp" class="form-text text-danger">Please Select your State</small>}

                        </div>
                        </div>

                        <div className='col col-6'>
                        <div className="form-group">
                        <label  htmlFor="validationCustom01">City<span style={{color:'red'}}>*</span></label>
                                                      <select 
                                                         name="City"
                                                         placeholder="Enter Select City"
                                                         type="select"
                                                         
                                                         className={`form-control ${this.state.touched14&&this.state.city===''?'is-invalid':""}`}
                                                         validate={{ required: { value: true,errorMessage:"Select Any City" } }}
                                                         value={this.state.city}
                                                         onBlur={
                                                             e=>{
                                                                 this.setState({...this.state,
                                                                touched14:true})
                                                             }
                                                         }
                                                         onChange={this.onChange('city')}
                                                       >
                                                           <option value={''} >Select City</option>
                                                                    <option value={'city 1'} >city 1</option>
                                                                   <option value={'city 2'}>city 2</option>
                                                                   <option value={'city 3'}>city 3</option>
                                                                   <option value={'city 4'}>city 4</option>
                                                                   <option value={'city 5'}>city 5</option>
                                                     
                                                         </select>
                  {this.state.touched14&&this.state.city===''&&<small id="emailHelp" class="form-text text-danger">Please Select Your City</small>}

                        </div>
                        </div>

                        <div className='col col-6'>
                        <div className="form-group">
                        <label  htmlFor="validationCustom01">PIN Code<span style={{color:'red'}}>*</span></label>
                                                      <input 
                                                           name="pin"
                                                           placeholder="Enter Your Full Name"
                                                           type="number"
                                                           
                                                           className={`form-control ${this.state.touched15&&this.state.pincode===''?'is-invalid':""}`}
                                                           validate={{ required: { value: true,errorMessage:"Pin Code" } }}
                                                           value={this.state.pincode}
                                                           onBlur={
                                                               e=>{
                                                                   this.setState({...this.state,
                                                                touched15:true})
                                                               }
                                                           }
                                                           onChange={this.onChange('pincode')}
                                                         />
                  {this.state.touched15&&this.state.pincode===''&&<small id="emailHelp" class="form-text text-danger">Please Enter Pin Code</small>}

                        </div>
                        </div>







                        </div>
                        </form>
                       

                          
                            
     

                                            

                            </CardBody>
                            </Card>

                            <Card>
                                   <CardBody className={"py-1"}>
                                        <AvForm className="needs-validation" >  
                                            <Row>
                                                <Col md="6">
                                                    <FormGroup className={"pt-1"}>
                                                        <div className="custom-control custom-checkbox pt-2">
                                                        <input type="checkbox"  class="form-check-input"
                                                        checked={this.state.PermanentSame}
                                                       
                                                        onChange={()=>{
                                                            this.setState({
                                                                PermanentSame:!this.state.PermanentSame
                                                            })
                                                        }}
                                                       />
                                                        <label class="form-check-label" for="exampleCheck1" style={{verticalAlign:'middle'}}>Agree to terms and conditions</label>
                                                            
                                                        </div>
                                                    </FormGroup>
                                                </Col>
                                                <Col md='6' style={{textAlign:"end", marginTop:'.2rem'}}>
                                                    <button disabled={this.state.PermanentSame===true?false:true} className={"btn"} type="submit" style={{backgroundColor:'#880105', color:'#e1b761'}} onClick={this.addClientBtn}>Add Staff</button>
                                                    <button className={"ml-2 btn"} style={{backgroundColor:'#880105', color:'#e1b761'}} type="button">Cancel</button>
                                                </Col>
                                            </Row>
                                        </AvForm>
                                    </CardBody>
                                </Card>
                              
                            </AvForm>

                            </Container>
                            </div>
                            </React.Fragment>
    )
  }
}
export default AddStaff