import React, { Component } from "react";
import { Row, Col, Card, CardBody, FormGroup, Button, CardTitle, CardSubtitle, Label, Input ,Container, UncontrolledTooltip, Table} from "reactstrap";
import { Link } from "react-router-dom";
import { AvForm, AvField } from "availity-reactstrap-validation";



//Import Breadcrumb
import Breadcrumbs from '../components/Common/Breadcrumb';

//i18n
import { withNamespaces } from 'react-i18next';

class DailyCollection extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }


    render() {
        return (
            <React.Fragment>
                <div className="page-content">
                    <Container fluid>

                        {/* Render Breadcrumb */}
                        <Breadcrumbs title={this.props.t('DailyCollection')} breadcrumbItem={this.props.t('DailyCollection')} />
                    
                        <Card>
                                    <CardBody>
                                        <AvForm className="needs-validation" >
                                            <Row>
                                                <Col md="6">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">Account Number</Label>
                                                        <AvField
                                                          name="Account"
                                                          placeholder="Account Number"
                                                          type="text"
                                                          errorMessage="Enter Account Number"
                                                          className="form-control"
                                                          validate={{ required: { value: true } }}
                                                          id="validationCustom01"
                                                        />
                                                    </FormGroup>
                                                </Col>
                                                <Col md="6">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">Email-Id</Label>
                                                        <AvField
                                                          name="EmailId"
                                                          placeholder="Enter Email-Id"
                                                          type="email"
                                                          errorMessage="Enter Email-ID"
                                                          className="form-control"
                                                          validate={{ required: { value: true } }}
                                                          id="validationCustom01"
                                                        />
                                                    </FormGroup>
                                                </Col>

                                            </Row>

                                            <Row>
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom03">Insatllment Number</Label>
                                                        <select class="form-control">
                                                        <option>Select Installment Number</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5</option>
                                                        <option>6</option>
                                                    </select>
                                                    </FormGroup>
                                                </Col>   
                                                <Col md="4">
                                                 <FormGroup>
                                                    <Label htmlFor="validationCustom02">Installment Amount</Label>
                                                    <AvField
                                                      name="Installment"
                                                      placeholder="Installment Number"
                                                      type="text"
                                                      errorMessage="Enter Installment Number"
                                                      className="form-control"
                                                      validate={{ required: { value: true } }}
                                                      id="validationCustom02"
                                                    />
                                                 </FormGroup>
                                                </Col>   
                                                <Col md="4">
                                                <FormGroup>
                                                    <Label htmlFor="validationCustom04">Current Gold Rate</Label>
                                                    <AvField
                                                    name="rate"
                                                    placeholder="Current Gold Rate"
                                                    type="number"
                                                    errorMessage="Please provide a valid rate."
                                                    className="form-control"
                                                    validate={{ required: { value: true } }}
                                                    id="validationCustom04"
                                                    />
                                                </FormGroup>
                                            </Col>                                       
                                            </Row>
                                            <Row>
                                                <Col md="4">
                                                 <FormGroup>
                                                    <Label htmlFor="validationCustom02">Consolidated Weight</Label>
                                                    <AvField
                                                      name="Consolidated_Weight"
                                                      placeholder="Consolidated Weight"
                                                      type="number"
                                                      errorMessage="Enter Consolidated Weight"
                                                      className="form-control"
                                                      validate={{ required: { value: true } }}
                                                      id="validationCustom02"
                                                    />
                                                 </FormGroup>
                                                </Col>   
                                                                                       
                                            </Row>
                                                                                       
                                            <Button style={{backgroundColor:'#880105', color:'#e1b761'}} className={'btn'} type="submit">Submit</Button>
                                           
                                        </AvForm>
                                    </CardBody>
                                </Card>
                    </Container>
                        
                </div>
          
                </React.Fragment>
        );
    }
}

export default withNamespaces()(DailyCollection);

 
 
 
 
 