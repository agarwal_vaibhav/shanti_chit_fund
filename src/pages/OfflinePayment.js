import React, { Component } from "react";
import { Container, Row, Col, Button, Card, CardBody, CardTitle, Modal, ModalHeader, ModalBody, ModalFooter, Media, Table } from "reactstrap";
import { Link } from "react-router-dom";

//Import Breadcrumb
import Breadcrumbs from '../components/Common/Breadcrumb';

//i18n
import { withNamespaces } from 'react-i18next';
import GridTables from "./Dashboard/GridTables";

class OfflinePayment extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }


    render() {
        return (
            <React.Fragment>
                <div className="page-content">
                    <Container fluid>

                        {/* Render Breadcrumb */}
                        <Breadcrumbs title={this.props.t('OfflinePayment')} breadcrumbItem={this.props.t('OfflinePayment')} />
                            <GridTables/>
                        </Container>
                </div>
          
                </React.Fragment>
        );
    }
}

export default withNamespaces()(OfflinePayment);