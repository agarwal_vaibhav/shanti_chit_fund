import React, { Component } from 'react';
import { Row, Col, CardBody, Card, Alert,Container } from "reactstrap";
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import PostApiCall from '../API/PostAPI';
import Notiflix from "notiflix";
class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            Mobile:'',
            password:'',
            visible:false,
            NumRegex: /^[0-9]*$/,
            AlphaNumericRegex : /^[a-zA-Z0-9]*$/,
            EmailRegex :  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            UrlRegex : /^(https:\/\/www\.|https:\/\/www\.|https:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/,
          
        }

        // handleValidSubmit
        this.handleValidSubmit = this.handleValidSubmit.bind(this);
    }

    
    componentDidMount=()=>{
        Notiflix.Loading.Init({
          svgColor : '#880105'
         
        });
    }

    // handleValidSubmit
    handleValidSubmit(event, values) {
        this.props.loginUser(values, this.props.history);
    }
    loginbtn=()=>{
        // console.log("LLLL");
        Notiflix.Loading.Dots('Please wait...');
        if(this.state.Mobile!==''&& this.state.Mobile.length==10 &&this.state.password!=''){
          PostApiCall.postRequest({mobile:this.state.Mobile,
        password:this.state.password},'New_Admin_Login').then((result) =>
      
        result.json().then(obj => {
            console.log(result)
            if(result.status == 200 || result.status == 201){
                // console.log(obj)
                localStorage.setItem('loginedUser',JSON.stringify(obj.data))
            window.location.href='/dashboard'
            // this.props.history.location.push('/dashboard')
             Notiflix.Loading.Remove();
        
           
            }else
            {
                Notiflix.Loading.Remove();
                Notiflix.Notify.Failure('Please enter vaild mobile or password')
              }
        })
        )
        
            
        }
        else
            {
                Notiflix.Loading.Remove();
                Notiflix.Notify.Failure('Please enter vaild mobile or password')
              }
        
    }
    emailChange=(e)=>{
        // console.log(e.target.value)
        this.setState({
            email:e.target.value
        })
    }
    passwordChange=(e)=>{
        // console.log(e.target.value)
      
        this.setState({
            password:e.target.value
        })
    }

    render() {

        return (
            <React.Fragment>
                <div className="home-btn d-none d-sm-block">
                    <Link to="/" className="text-dark"><i className="bx bx-home h2"></i></Link>
                </div>
                <div className="account-pages my-5 pt-sm-5">
                    <Container>
                        <Row className="justify-content-center">
                            <Col md={8} lg={6} xl={5}>
                                <Card className="overflow-hidden">
                                    <div className="bg-soft-primary">
                                        <Row>
                                            <Col className="col-12">
                                                <div className="p-2 pl-4" style={{backgroundColor:"#880105"}}>
                                                    <h5 className="mt-3" style={{color:"#e1b761"}}>Welcome</h5>
                                                    <p style={{color:"#e1b761"}}>Login to continue to Shanthi Jewellers</p>
                                                </div>
                                            </Col>                                            
                                        </Row>
                                    </div>
                                    <CardBody className="pt-0">
                                        <div className={"mt-3 mb-2"} style={{marginLeft:'20%'}}>
                                           <img src="http://adminbox.shanthijewellers.com/images/logo.jpg"/>
                                        </div>
                                        <div className="p-2">

                                            <AvForm className="form-horizontal">

                                                {this.props.error && this.props.error ? <Alert color="danger">{this.props.error}</Alert> : null}

                                                <div className="form-group">
                                                    <AvField name="email" label="Mobile Number" value={this.state.Mobile} className="form-control"
                                                    onChange={(mobile)=>{
                                                        if((this.state.NumRegex.test(mobile.target.value)) && (mobile.target.value.length <= 10)){
                                                            this.setState({
                                                                Mobile:mobile.target.value
                                                            })
      
                                                        }
                                                        
                                                    }} placeholder="9999999999" required />
                                                </div>

                                                <div className="form-group">
                                                    <AvField name="password" label="Password" value={this.state.password} type={this.state.visible?'text':'password'} 
                                                    onChange={this.passwordChange} required placeholder="Enter Password"  />
                                                    <i className="bx bxs-show passwordicon"
                                                    onClick={()=>{
                                                        this.setState({
                                                            visible:!this.state.visible
                                                        })
                                                    }}></i>
                                                </div>

                                                                                                
                                                <div className="mt-3">
                                                 
                                                    <button className="btn btn-block waves-effect waves-light" type="submit"
                                                     style={{backgroundColor:'#880105', color:'#e1b761'}} onClick={this.loginbtn}>Log In</button>
                                                
                                                </div>
                                                

                                                <div className="mt-4 text-center">
                                                    <Link to="/forgot-password" className="text-muted"><i className="mdi mdi-lock mr-1"></i> Forgot your password?</Link>
                                                </div>
                                            </AvForm>
                                        </div>
                                    </CardBody>
                                </Card>
                                <div className="mt-5 text-center">
                                    <p>© {new Date().getFullYear()} Shanthi Jewellers. All Rights Reserved.</p>
                                    <p>Powered By Global Trendz</p>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </React.Fragment>
        );
    }
}

export default withRouter(Login);



// #880105 red
// 865a35 golden
// e1b761