import React, { Component } from "react";
import { Row, Col, Card, CardBody, FormGroup, Button, CardTitle, CardSubtitle, Label, Input ,Container, UncontrolledTooltip, Table} from "reactstrap";
import { Link } from "react-router-dom";
import { AvForm, AvField } from "availity-reactstrap-validation";



//Import Breadcrumb
import Breadcrumbs from '../components/Common/Breadcrumb';

//i18n
import { withNamespaces } from 'react-i18next';

class AddUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            gender:'',
            address:'',
            status:'',
            name:'',
            email:'',
            mob:''
        };
    }
    genderChange=(e)=>{
        this.setState({
            gender :e.target.value
        })
    }
    emailChange=(e)=>{
        this.setState({
            email :e.target.value
        })
    }
    mobChange=(e)=>{
        this.setState({
            mob :e.target.value
        })
    }

    subBtn=()=>{
        if(this.state.gender===''){
            document.getElementById('genderVal').innerHTML="Select Your Gender"
            document.getElementById("genderDiv").style.borderColor = "#f46a6a"               
        }else{
            document.getElementById('genderVal').innerHTML=""
            document.getElementById("genderDiv").style.borderColor = "" 
        }
        if(this.state.address===''){
            document.getElementById('addressText').innerHTML="Enter Your Complete Address"
            document.getElementById("addressDiv").style.borderColor = "#f46a6a"               
        }else{
            document.getElementById('addressText').innerHTML=""
            document.getElementById("addressDiv").style.borderColor = "" 
        }
        if(this.state.status===''){
            document.getElementById('statusVal').innerHTML="Select your status"
            document.getElementById("statusDiv").style.borderColor = "#f46a6a"               
        }else{
            document.getElementById('statusVal').innerHTML=""
            document.getElementById("statusDiv").style.borderColor = "" 
        }
        if(this.state.name===''){
            document.getElementById('nameVal').innerHTML="Select your name"
            document.getElementById("nameDiv").style.borderColor = "#f46a6a"               
        }else{
            document.getElementById('nameVal').innerHTML=""
            document.getElementById("nameDiv").style.borderColor = "" 
        }
        if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.state.email)){
            document.getElementById('emailVal').innerHTML=""
            document.getElementById("emailDiv").style.borderColor = ""               
        }else{
            document.getElementById('emailVal').innerHTML="Select your valid Id"
            document.getElementById("emailDiv").style.borderColor = "#f46a6a" 
        }
        if(this.state.mob.length!=10){
            document.getElementById('mobVal').innerHTML="Enter 10 digit number"
            document.getElementById("mobDiv").style.borderColor = "#f46a6a"               
        }else{
            document.getElementById('mobVal').innerHTML=""
            document.getElementById("mobDiv").style.borderColor = "" 
        }
    }


    adressChange=(e)=>{
        this.setState({
            address:e.target.value
        })
    }
    nameChange=(e)=>{
        this.setState({
            name:e.target.value
        })
    }



    render() {
        return (
            <React.Fragment>
                <div className="page-content">
                    <Container fluid>

                        {/* Render Breadcrumb */}
                        <Breadcrumbs title={this.props.t('User Managment')} breadcrumbItem={this.props.t('Add Users')} />
                    
                        <Card>
 
                        
                                    <CardBody>
                                    <CardTitle>Add Users</CardTitle>
                                    <CardSubtitle className="mb-3">
                                        Enter the personal details to add new user.
                                    </CardSubtitle>
                                        <AvForm className="needs-validation" >
                                            <Row>
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom03">Name</Label>
                                                        <AvField
                                                        name="Name"
                                                        placeholder="Enter Your Name "
                                                        type="text"
                                                        errorMessage="Enter your Name"
                                                        className="form-control"
                                                        validate={{ required: { value: true } }}
                                                        id="nameDiv"
                                                        value={this.state.name}
                                                        onchange={this.nameChange}
                                                        />
                                                        <p id="nameVal" style={{color:'#f46a6a', fontSize:"80%"}}></p>
                                                    </FormGroup>

                                                </Col>   
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">Email ID</Label>
                                                        <AvField
                                                        name="mail"
                                                        placeholder="Enter your mail"
                                                        type="email"
                                                        errorMessage="Enter a valid email"
                                                        className="form-control"
                                                        validate={{ required: { value: true } }}
                                                        id="emailDiv"
                                                        value={this.state.email}
                                                        onChange={this.emailChange}
                                                        />
                                                        <p id="emailVal" style={{color:'#f46a6a', fontSize:"80%"}}></p>
                                                    </FormGroup>
                                                </Col>    
                                                <Col md="4">
                                                    <FormGroup>
                                                    <Label htmlFor="validationCustom02">Mobile Number</Label>
                                                    <AvField
                                                      name="lastname"
                                                      placeholder="Enter 10 Digit Mobile Number"
                                                      type="number"
                                                      errorMessage="Enter Mobile Number"
                                                      className="form-control"
                                                      onchange={this.mobchange}
                                                      id="mobDiv"
                                                    />
                                                    <p id="mobVal" style={{color:'#f46a6a', fontSize:"80%"}}></p>
                                                    </FormGroup>
                                                </Col>                                       
                                            </Row>

                                            <Row>
                                                <Col md="12">
                                                <FormGroup>
                                                <Label htmlFor="validationCustom04">Address</Label>
                                                <textarea
                                                name="Address"
                                                placeholder="Enter Complete Address"
                                                type="text"
                                                errorMessage="Please provide a valid Address."
                                                className="form-control"
                                                validate={{ required: { value: true } }}
                                                id="addressDiv"
                                                style={{resize:'none'}}
                                                value={this.state.address}
                                                onChange={this.adressChange}
                                                />
                                                <p id="addressText" style={{color:'#f46a6a', fontSize:"80%"}}></p>
                                            </FormGroup>
                                                </Col>                                                
                                            </Row>

                                            <Row>
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom03">Date Of Birth</Label>
                                                        <AvField
                                                        name="birth"
                                                        placeholder="Enter Your DOB "
                                                        type="date"
                                                        errorMessage="Enter your DOB"
                                                        className="form-control"
                                                        validate={{ required: { value: true } }}
                                                        id="validationCustom01"
                                                        />
                                                    </FormGroup>
                                                </Col>   
                                                 
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom03">Gender</Label>
                                                        <select class="form-control" value={this.state.gender} onChange={this.genderChange} id="genderDiv">
                                                            <option>Select</option>
                                                            <option>Male</option>
                                                            <option>Female</option>
                                                            <option>Other</option>
                                                        </select>
                                                        <p id="genderVal" style={{color:'#f46a6a', fontSize:"80%"}}></p>
                                                    </FormGroup>
                                                </Col>                                       
                                            </Row>

                                           


                                            
                                           
                                        </AvForm>
                                    </CardBody>
                                </Card>


                                <Card>
                                <h4 className={"pt-2 pl-4 mb-0 mt-2"}>Details Related To Office</h4>
                                <p className={"ml-4"}>Click Add button to add user</p>
                                    <CardBody>
                                        <AvForm className="needs-validation" >  
                                            

                                        <Row>
                                        <Col md="4">
                                            <FormGroup>
                                                <Label htmlFor="validationCustom03">Joining Date</Label>
                                                <AvField
                                                name="joining"
                                                placeholder="Enter Your DOB "
                                                type="date"
                                                errorMessage="Enter your Joining Date"
                                                className="form-control"
                                                validate={{ required: { value: true } }}
                                                id="validationCustom01"
                                                />
                                            </FormGroup>
                                        </Col>   
                                        <Col md="4">
                                            <FormGroup>
                                                <Label htmlFor="validationCustom01">Department</Label>
                                                <AvField
                                                name="department"
                                                placeholder="Department Name"
                                                type="text"
                                                errorMessage="Enter your department"
                                                className="form-control"
                                                validate={{ required: { value: true } }}
                                                id="validationCustom01"
                                                />
                                            </FormGroup>
                                        </Col>   
                                        <Col md="4">
                                            <FormGroup>
                                                <Label htmlFor="validationCustom03">Status</Label>
                                                <select class="form-control" value={this.state.status} onChange={this.statusChange} id="statusDiv">
                                                    <option>Select</option>
                                                    <option>Active</option>
                                                    <option>Away</option>
                                                    <option>Other</option>
                                                </select>
                                                <p id="statusVal" style={{color:'#f46a6a', fontSize:"80%"}}></p>
                                            </FormGroup>
                                        </Col>                                    
                                    </Row>
                                    <Row>
                                        <Col md="4">
                                            <FormGroup>
                                                <Label htmlFor="validationCustom01">Designation</Label>
                                                <AvField
                                                name="designation"
                                                placeholder="Enter your designation"
                                                type="text"
                                                errorMessage="Enter your designation"
                                                className="form-control"
                                                validate={{ required: { value: true } }}
                                                id="validationCustom01"
                                                />
                                            </FormGroup>
                                        </Col>  
                                    </Row>


                                        <button className='btn' style={{backgroundColor:'#880105', color:'#e1b761',float:"right", marginTop:'.2rem'}} 
                                        onClick={this.subBtn} type="submit">Add User</button>
                                        </AvForm>
                                    </CardBody>
                                </Card>

                                




                    </Container>
                        
                </div>
          
                </React.Fragment>
        );
    }
}

export default withNamespaces()(AddUser);








 


