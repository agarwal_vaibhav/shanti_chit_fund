import React, { Component } from "react";
import { Row, Col, Card, CardBody, FormGroup, Button, CardTitle, CardSubtitle, Label, Input, textarea,Container, UncontrolledTooltip, Table} from "reactstrap";
import { Link } from "react-router-dom";
import { AvForm, AvField } from "availity-reactstrap-validation";
import './CssForAll.css'
import GetApiCall from '../API/GetAPI'
import PostApiCall from '../API/PostAPI'
import Notiflix from "notiflix";
import moment from 'moment';


//Import Breadcrumb
import Breadcrumbs from '../components/Common/Breadcrumb';

//i18n
import { withNamespaces } from 'react-i18next';

class EditClient extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newClient:{
                accountNumber:'',
                chitScheme:'',
                chitAmount:'',
                duration:'',
                user:''
            },
            clients:[],
            showData:true,
            arrIndex:'',
            editOption:false,
            dropdown:"",
            address:'',
            country:'',
            state:'',
            city:'',
            allClientSchema:[],
            //personal details
            name:'',
            username:'',
            Email:'',
            Mobile:'',
            landmark:'',
            pin:'',
            visiblity:true,
            visible:false,
            personalData:[],
            accValid:'form-control',
            NameValid:'form-control',
            EmailValid:'form-control',
            MobileValid:'form-control',
            chitSchemeValid:'form-control',
            chitAmountValid:'form-control',
            durationValid:'form-control',
            userValid:'form-control',
            paidMonthsValid:'form-control',
            freqentChitfundData:[ 
                {label:"Select Chit Scheme",value:''},
                {label:"Thanga Mazhai",value:'Thanga Mazhai'},
                {label:"Thanga Then",value:'Thanga Then'},
                {label:"Thanga Malar",value:'Thanga Malar'},
                {label:"My Gold Eleven",value:'My Gold Eleven'},
            ],
            freqentChitfundPriceData:[
                {label:"Select Chit Amount",value:''},
                {label:"500",value:'500'},
                {label:"1000",value:'1000'},
                {label:"2500",value:'2500'},
                {label:"5000",value:'5000'},
                {label:"10000",value:'10000'},
            ],
            freqentUserData:[
                {label:"Select User",value:''},
                {label:"New",value:'New'},
                {label:"Existing",value:'Existing'},
             
            ],
            freqentMonthData:[
                {label:"Select Months",value:''},
                {label:"01",value:'01'},
                {label:"02",value:'02'},
                {label:"03",value:'03'},
                {label:"04",value:'04'},
                {label:"05",value:'05'},
                {label:"06",value:'06'},
                {label:"07",value:'07'},
            ],
            freqentDurationData:[
                {label:"Select Chit Duration",value:''},
                {label:"11",value:'11'},
                {label:"15",value:'15'},
            ],
            PermanentSame : false,
            MonthlyClientId:'',
        };
    }

    componentDidMount=()=>{

        Notiflix.Loading.Init({
            svgColor : '#880105'
           
          });

        var abc= localStorage.getItem('ClientDetails')

    //   console.log(abc)
      this.setState({
        personalData:JSON.parse(abc), 
        
      })
  
      Notiflix.Loading.Dots('Please wait...'); 
     this.setState({
        name:JSON.parse(abc).name,
        Email:JSON.parse(abc).email,
        Mobile:JSON.parse(abc).mobNo,
        MonthlyClientId:JSON.parse(abc).id
     })

     Notiflix.Loading.Remove();

      Notiflix.Loading.Dots('Please wait...'); 
    
      PostApiCall.postRequest ({
        id:JSON.parse(abc).id
      },"Get_New_SpecificMonthlySchema").then((result) =>
    
      result.json().then(obj => {
          if(result.status == 200 || result.status == 201){
            //   console.log(obj.data)
              var CL=[]
              for(var i=0;i<obj.data.length;i++){
                  CL.push({
                    accountNumber:obj.data[i].fld_accountno,
                    chitScheme:obj.data[i].fld_chitschema,
                    chitAmount:obj.data[i].fld_chitamount,
                    duration:obj.data[i].fld_duration,
                    user:obj.data[i].fld_user,
                    paidMonths:obj.data[i].fld_paidmonths
                 })
                this.setState({
                    // allClientSchema:CL,
                    clients:CL
                  })
              }
             
          }
      })
      )
      Notiflix.Loading.Remove();
    }


    handleUserInput = (e) => {
        // console.log(e.target.name)
        // console.log(e.target.value)
        if(e.target.name==="exiting_user" && e.target.value==="Existing") {
            this.setState({
                visiblity:false
            })
        }else{
            this.setState({
                visiblity:true
            })
        }

        let input_data = Object.assign({}, this.state.newClient);
       
        const fields = e.target.id;
        input_data[fields] = e.target.value;
  
          this.setState({
               newClient : input_data,
               userValid:"form-control is-valid"
          })  
          
          if(e.target.value===''){
            this.setState({
                userValid:"form-control is-invalid",
                  
              })  
        }
    }
   
    handleChitScheme=(e)=>{
        let input_data = Object.assign({}, this.state.newClient);
       
        const fields = e.target.id;
        input_data[fields] = e.target.value;
        // console.log(e.target.value);
         this.setState({
            chitSchemeValid:"form-control is-valid",
               newClient : input_data
          })  
        //   console.log(this.state.newClient.chitScheme);

          if(e.target.value===''){
            this.setState({
                chitSchemeValid:"form-control is-invalid",
                  
              })  
        }
    }
    
     handleChitAmount=(e)=>{
        let input_data = Object.assign({}, this.state.newClient);
       
        const fields = e.target.id;
        input_data[fields] = e.target.value;
         this.setState({
            chitAmountValid:"form-control is-valid",
               newClient : input_data
          })  
        //   console.log(e.target.value);

        if(e.target.value===''){
            this.setState({
                chitAmountValid:"form-control is-invalid",
                  
              })  
        }
    }
    handlePaidMonth=(e)=>{
        let input_data = Object.assign({}, this.state.newClient);
       
        const fields = e.target.id;
        input_data[fields] = e.target.value;
        // console.log(e.target.value);
         this.setState({
            paidMonthsValid:"form-control is-valid",

               newClient : input_data
          })  
        //   console.log(this.state.newClient.chitScheme);

          if(e.target.value===''){
            this.setState({
                paidMonthsValid:"form-control is-invalid",
                  
              })  
        }
    }
    
    handleDuration=(e)=>{
        let input_data = Object.assign({}, this.state.newClient);
       
        const fields = e.target.id;
        input_data[fields] = e.target.value;
        console.log(e.target.value);
         this.setState({
            durationValid:"form-control is-valid",
               newClient : input_data
          })  
          if(e.target.value===''){
            this.setState({
                durationValid:"form-control is-invalid",
                  
              })  
        }
    }
    

    submitDataHandler = () => {   
        if(this.state.newClient.chitScheme==''&&this.state.newClient.chitAmount==''&&this.state.newClient.duration==''&&(this.state.newClient.user===''&&this.state.newClient.paidMonths=='')){
            this.setState({
                paidMonthsValid  :"form-control is-invalid",
                userValid  :"form-control is-invalid",
                chitAmountValid  :"form-control is-invalid",
                durationValid  :"form-control is-invalid",
                chitSchemeValid  :"form-control is-invalid",
                accValid  :"form-control is-invalid",
               
                  
              })
           }
      else  if(this.state.newClient.chitScheme==''){
            this.setState({
                chitSchemeValid:"form-control is-invalid",
                  
              })   
        }
      else  if(this.state.newClient.chitAmount==''){
        this.setState({
            chitAmountValid:"form-control is-invalid",
              
          })   }
     else   if(this.state.newClient.duration==''){
        this.setState({
            durationValid  :"form-control is-invalid",
              
          }) }
     else   if(this.state.newClient.user==''){
        this.setState({
            userValid  :"form-control is-invalid",
              
          })          
            if(this.state.newClient.paidMonths==''){
                this.setState({
                    paidMonthsValid  :"form-control is-invalid",
                      
                  }) }
        }
      else   if(this.state.newClient.chitScheme==''||this.state.newClient.chitAmount==''||this.state.newClient.duration==''||(this.state.newClient.user===''&&this.state.newClient.paidMonths=='')){
             return   
            }

        else{  
          


            if(this.state.editOption === true){
                // console.log('asdf');
                this.setState({
                    paidMonthsValid  :"form-control",
                    userValid  :"form-control",
                    chitAmountValid  :"form-control",
                    durationValid  :"form-control",
                    chitSchemeValid  :"form-control",
                    accValid  :"form-control",
                  })
              let {clients, arrIndex, newClient} = this.state;
              let new_data = Object.assign([], clients);
              new_data.splice(arrIndex, 1);
              new_data.push(newClient);
         
              this.setState({
                clients: new_data,
                newClient: {
                    accountNumber:'',
                    chitScheme:'',
                    chitAmount:'',
                    duration:'',
                    user:'',
                    paidMonths:''
            },
              }, () => {
                // console.log('state ', this.state.clients);
              })
             
            } else{
              this.state.clients.push(this.state.newClient)
              this.setState({
                paidMonthsValid  :"form-control",
                userValid  :"form-control",
                chitAmountValid  :"form-control",
                durationValid  :"form-control",
                chitSchemeValid  :"form-control",
                accValid  :"form-control",
               
                  
              })
              this.setState({
                showData: true,
                newClient: {
                    accountNumber:'',
                    chitScheme:'',
                    chitAmount:'',
                    duration:'',
                    user:'',
                    paidMonths:''
            },
         })
        }
       }
    }

      
       editForm(value, index){
        // console.log(value);
        this.setState({
          newClient: value,
          arrIndex: index,
          editOption: true,
        })
  
      }
  
  
      delForm = (index)=> {
        //   console.log("hello")
        let new_data = Object.assign([], this.state.clients);
        new_data.splice(index, 1);
        console.log(new_data)
        this.setState({
          clients: new_data
        })
      }

      dataDropdown=(e)=>{
          this.setState({
              dropdown:e.target.value
          })
      }

     

    adressChange=(e)=>{
        this.setState({
            address:e.target.value
        })
    }

    landmarkChange=(e)=>{
        this.setState({
            landmark:e.target.value
        })
    }

    mobChange=(e)=>{
        this.setState({
            Mobile:e.target.value
        })
    }
    nameChange=(e)=>{
        this.setState({
            name:e.target.value
        })
    }
    usernameChange=(e)=>{
        this.setState({
            username:e.target.value
        })
    }
    emailChange=(e)=>{
        this.setState({
            Email:e.target.value
        })
    }
    countryChange=(e)=>{
        this.setState({
            country :e.target.value
        })
    }
    stateChange=(e)=>{
        this.setState({
            state :e.target.value
        })
    }
    cityChange=(e)=>{
        this.setState({
            city :e.target.value
        })
    }
    pinChange=(e)=>{
        this.setState({
            pin :e.target.value
        })
    }

    editable=(e)=>{
        this.setState({
            visiblity:false
        })
    }

    updateClientBtn=()=>{
        if(this.state.name==''){
            Notiflix.Notify.Failure("Enter Name")
        }else if(this.state.Email==''){
            Notiflix.Notify.Failure("Enter Email")
        }else if(this.state.Mobile==''){
            Notiflix.Notify.Failure("Enter Mobile Number")
        }else{
            Notiflix.Loading.Dots('Please wait...'); 
            var det=localStorage.getItem('loginedUser')
            var detail=JSON.parse(det)
            PostApiCall.postRequest ({
                 id:this.state.MonthlyClientId,
                 name : this.state.name,
                 email: this.state.Email,               
                 mobile : this.state.Mobile,
                 updatedon : moment().format('lll'),
                 updatedby : detail[0].fld_userid,
                 termsconditions : this.state.PermanentSame,
                 clientStatus:'Approved'
              
            
            },"Update_NewMonthly_Client").then((result) =>
          
            result.json().then(obj => {
                if(result.status == 200 || result.status == 201){
                   
                    // console.log(obj)
                   this.onSubmitChitschem(obj);
                }else
                {
                    this.onSubmitChitschem(obj);
                }
            })
            )


        }
    }


    
    onSubmitChitschem(obj){
        var det=localStorage.getItem('loginedUser')
        var detail=JSON.parse(det)
            console.log(this.state.clients)
            Notiflix.Loading.Dots('Please wait...');      
            for(var i=0;i<this.state.clients.length;i++){
        
            PostApiCall.postRequest ({
               
                client: (JSON.parse(JSON.stringify(obj.data[0]))).ClientId,
                accountno :this.state.clients[i].accountNumber,
                chitschema : this.state.clients[i].chitScheme,
                chitamount :this.state.clients[i].chitAmount,
                duration : this.state.clients[i].duration,
                user :this.state.clients[i].user,
                paidmonths :this.state.clients[i].paidMonths,
                updatedon : moment().format('lll'),
                updatedby : detail[0].fld_userid,
                clientStatus:'Approved'
                
               
                
            },"Add_New_MonthlyClient_chitschema").then((result) =>
        
            result.json().then(obj => {
                if(result.status == 200 || result.status == 201){
                
                    // console.log(obj)
                    Notiflix.Loading.Remove();
                    Notiflix.Notify.Success('Chit successfully updated.')
                    window.location.href='/view-clients'
                
                }else
                {
                Notiflix.Loading.Remove();
                Notiflix.Notify.Failure('Something went wrong, try again later.')
                }
            })
            )
        }
      
      }



    handleInputAccount=(e)=>{
        const regex = /^[a-zA-Z]{2}\d*$/
    //    console.log(e.target.value)
 
            const pattern=e.target.value
            // console.log(regex.test(pattern))
            // console.log(e.target.id);

            if(!regex.test(pattern)){
                this.setState({accValid:"form-control is-invalid"})
                // console.log(` is not a valid character.`); 
            }
            else{
                let input_data = Object.assign({}, this.state.newClient);
                
                const fields = e.target.id;
                input_data[fields] = e.target.value;
                
                this.setState({accValid:"form-control is-valid",
                    newClient : input_data
                })  
            } 
    }

    render() {
        // console.log(this.state.allClientSchema)

        console.log('user data', this.state.clients);
        const userData = this.state.clients;
        return (
            <React.Fragment>
                <div className="page-content">
                    <Container fluid>

                        {/* Render Breadcrumb */}
                        <Breadcrumbs title={this.props.t('Manage Client')} breadcrumbItem={this.props.t('Edit Client Detail')} />
                        <AvForm className="needs-validation" >
                        <Card>
                             <h4 className={"pt-2 pl-4 mb-0 mt-2"}>Personal Information</h4>
                             <p className={"ml-4"}>Edit The Personal Information of Client</p>
                             <Button onClick={this.editable} style={{width:'15%', marginLeft:'80%', marginTop:'-5%'}}>Edit</Button>
                                    <CardBody>
                                        
                                            <Row>
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">Name<span style={{color:'red'}}>*</span></Label>
                                                        <AvField
                                                          name="Name"
                                                          placeholder="Enter Your Full Name"
                                                          type="text"
                                                          errorMessage="Enter Name"
                                                          className="form-control"
                                                          validate={{ required: { value: true } }}
                                                          id="validationCustom01"
                                                          value={this.state.name}
                                                          onChange={this.nameChange}
                                                          disabled={this.state.visiblity}
                                                        />
                                                    </FormGroup>
                                                </Col>
                                                

                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom03">Email<span style={{color:'red'}}>*</span></Label>
                                                        <AvField
                                                          name="Email"
                                                          placeholder="Eg: John@gmail.com"
                                                          type="email"
                                                          errorMessage=" Please provide a valid Email."
                                                          className="form-control"
                                                          validate={{ required: { value: true } }}
                                                          id="validationCustom03"
                                                          value={this.state.Email}
                                                          onChange={this.emailChange}
                                                          disabled={this.state.visiblity}
                                                        />
                                                    </FormGroup>
                                                </Col>   
                                                <Col md="4">
                                                <FormGroup>
                                                    <Label htmlFor="validationCustom02">Mobile Number<span style={{color:'red'}}>*</span></Label>
                                                    <AvField
                                                      name="lastname"
                                                      placeholder="Enter 10 Digit Mobile Number"
                                                      type="number"
                                                      errorMessage="Enter Mobile Number"
                                                      className="form-control"
                                                      validate={{
                                                        required: { value: true },
                                                        minLength: { value: 10, errorMessage: "Enter 10 Digit Mobile Number." },
                                                        maxLength: { value: 10, errorMessage: "Enter 10 Digit Mobile Number." }
                                                      }}
                                                      id="validationCustom02"
                                                      value={this.state.Mobile}
                                                      onChange={this.mobChange}
                                                      disabled={this.state.visiblity}
                                                    />
                                                </FormGroup>
                                            </Col>                                          
                                            </Row>
                                            
                                       
                                    </CardBody>
                                </Card>
                               
                                <Card>
                                <h4 className={"pt-2 pl-4 mb-0 mt-2"}>Chit Scheme Details</h4>
                                <p className={"ml-4"}>Click Add button to add single or multiple chit details</p>
                                <CardBody>
                                <AvForm className="needs-validation" >  
                                    <Row>
                                        <Col md="4">
                                            <FormGroup>
                                                <Label htmlFor="validationCustom04">Account Number</Label>
                                                <AvField
                                                  name="Account Number"
                                                  placeholder="Unique Account Number"
                                                  type="text"
                                                  pattern=""
                                                  errorMessage="Please provide a valid Account Number." 
                                                  className={`${this.state.accValid}`}
                                                  validate={{ required: { value: this.state.accValid==='form-control is-invalid'?true:false } }}
                                                  id="accountNumber"
                                                  value={this.state.newClient.accountNumber}
                                                  onChange = { this.handleInputAccount }
                                                />
                                                {this.state.accValid=='form-control is-invalid'&&(<div className='invalid-feedback'>Enter Correct Account Number</div>)}

                                            </FormGroup>
                                        </Col>
                                        <Col md="4">
                                            <FormGroup>
                                                <Label htmlFor="validationCustom05">Chit Scheme</Label>
                                                <select id="chitScheme" className={`${this.state.chitSchemeValid}`} value={this.state.newClient.chitScheme}  onChange = { this.handleChitScheme }>
                                                {this.state.freqentChitfundData.map(data=>{
                                                    return   <option key={data.label} value={data.value}>{data.label}</option>
                                                })}
                                                
                                                 
                                                </select>          
                                                {this.state.chitSchemeValid=='form-control is-invalid'&&(<div className='invalid-feedback'>Select Your Chit Scheme </div>)}
                                            </FormGroup>
                                        </Col>
                                        <Col md="4">
                                            <FormGroup>
                                                <Label htmlFor="validationCustom03">Chit Amount</Label>
                                                <select className={`${this.state.chitAmountValid}`} id="chitAmount" value={this.state.newClient.chitAmount}  onChange = { this.handleChitAmount }>
                                                {this.state.freqentChitfundPriceData.map(data=>{
                                                    return   <option key={data.label} value={data.value}>{data.label}</option>
                                                })}
                                                </select>
                                                {this.state.chitAmountValid=='form-control is-invalid'&&(<div className='invalid-feedback'>Select Your Chit Amount </div>)}
                 
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                    <Col md="4">
                                        <FormGroup>
                                            <Label htmlFor="validationCustom04">Duration</Label>
                                            <select  className={`${this.state.durationValid}`} id="duration" value={this.state.newClient.duration}  onChange = { this.handleDuration }>
                                            {this.state.freqentDurationData.map(data=>{
                                                    return   <option key={data.label} value={data.value}>{data.label}</option>
                                                })}
                                             </select>
                                             {this.state.durationValid=='form-control is-invalid'&&(<div className='invalid-feedback'>Select your Duration</div>)}
                                                </FormGroup>
                                    </Col>
                                    <Col md="4">
                                        <FormGroup>
                                            <Label htmlFor="validationCustom05">User</Label>
                                            <select className={`${this.state.userValid}`} id="user" value={this.state.newClient.user} name={'exiting_user'} onChange = { this.handleUserInput }>
                                            {this.state.freqentUserData.map(data=>{
                                                    return   <option key={data.label} value={data.value}>{data.label}</option>
                                                })}
                                            </select>
                                            {this.state.userValid=='form-control is-invalid'&&(<div className='invalid-feedback'>Select User Type </div>)}

                                        </FormGroup>
                                    </Col>

                                    <Col md="4" className={this.state.visiblity===true?"d-none":""}>
                                        <FormGroup>
                                            <Label htmlFor="validationCustom05">Paid months</Label>
                                            <select class={this.state.newClient.paidMonths!==''?"form-control is-valid":'form-control'} id="paidMonths" value={this.state.newClient.paidMonths} 
                                             onChange = { this.handlePaidMonth }>
                                               {this.state.freqentMonthData.map(data=>{
                                                    return   <option key={data.label} value={data.value}>{data.label}</option>
                                                })}
                                            </select>
                                            <p id="paidMonthsrVal" style={{color:'#f46a6a', fontSize:"80%"}}></p>
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <button className={"btn ml-2"} type="submit" 
                                    style={{backgroundColor:'#880105', color:'#e1b761'}} onClick={this.submitDataHandler} >Add Chit Scheme</button>
                                </Row>
                                </AvForm>
                            </CardBody>
                                </Card>

                                <Card >
                                    <CardBody>
                                    <Table className="table table-centered table-nowrap">
                                    <thead className="thead-light">
                                        <tr>
                                            <th>Account No.</th>
                                            <th>Chit Scheme</th>
                                            <th>Chit Amount</th>
                                            <th>Duration</th>
                                            <th>User</th>
                                            <th>Paid Months</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                           
                                    {
                                        
                                        this.state.allClientSchema.map((value, index) => {
                                            return (

                                            <tr>
                                                
                                                <td>{value.fld_accountno}</td>
                                                <td>{value.fld_chitschema}</td>
                                                <td>{value.fld_chitamount}</td>
                                                <td>{value.fld_duration}</td>
                                                <td>{value.fld_user}</td>
                                                <td>{value.fld_paidmonths}</td>
                                                <td>
                                                    <Link onClick={this.editForm.bind(this, value ,index)} className="mr-3 text-primary">
                                                        <i className="mdi mdi-pencil font-size-18 mr-3" id="edittooltip"></i>
                                                        <UncontrolledTooltip placement="top" target="edittooltip">
                                                            Edit
                                                        </UncontrolledTooltip>
                                                    </Link>
                                                    <Link onClick={this.delForm.bind(this, index)} className="text-danger">
                                                        <i className="mdi mdi-close font-size-18 mr-3" id="deletetooltip"></i>
                                                        <UncontrolledTooltip placement="top" target="deletetooltip">
                                                            Delete
                                                        </UncontrolledTooltip>
                                                    </Link>
                                                </td>
                                            </tr>

                                            )
                                        })
                                      }       






                                    {userData && userData.map((value, index) => {
                                                return (

                                                <tr>
                                                    
                                                    <td>{value.accountNumber}</td>
                                                    <td>{value.chitScheme}</td>
                                                    <td>{value.chitAmount}</td>
                                                    <td>{value.duration}</td>
                                                    <td>{value.user}</td>
                                                    <td>{value.paidMonths}</td>
                                                    
                                                    <td>
                                                        <Link onClick={this.editForm.bind(this, value ,index)} className="mr-3 text-primary">
                                                            <i className="mdi mdi-pencil font-size-18 mr-3" id="edittooltip"></i>
                                                            <UncontrolledTooltip placement="top" target="edittooltip">
                                                                Edit
                                                            </UncontrolledTooltip>
                                                        </Link>
                                                        <Link onClick={this.delForm.bind(this, index)} className="text-danger">
                                                            <i className="mdi mdi-close font-size-18 mr-3" id="deletetooltip"></i>
                                                            <UncontrolledTooltip placement="top" target="deletetooltip">
                                                                Delete
                                                            </UncontrolledTooltip>
                                                        </Link>
                                                    </td>
                                                </tr>

                                                )
                                            })
                                          }
                                            
                                        

                                    </tbody>
                                </Table>
                                    </CardBody>
                                </Card>

                                <Card>
                                   <CardBody className={"py-1"}>
                                        <AvForm className="needs-validation" >  
                                            <Row>
                                                <Col md="6">
                                                    <FormGroup className={"pt-1"}>
                                                    <div className="custom-control custom-checkbox pt-2">
                                                    <input type="checkbox"  class="form-check-input"
                                                    checked={this.state.PermanentSame}
                                                    onChange={()=>{
                                                        this.setState({
                                                            PermanentSame:!this.state.PermanentSame
                                                        })
                                                    }}
                                                   />
                                                    <label class="form-check-label" for="exampleCheck1" style={{verticalAlign:'middle'}}>Agree to terms and conditions</label>
                                                    </div>
                                                    </FormGroup>
                                                </Col>
                                                <Col md='6' style={{textAlign:"end", marginTop:'.2rem'}}>
                                                    <button type="button" className='btn' disabled={this.state.PermanentSame===true?false:true} style={{backgroundColor:'#880105', color:'#e1b761'}} onClick={this.updateClientBtn}>Update Client</button>
                                                    <button className={"btn ml-2"} style={{backgroundColor:'#880105', color:'#e1b761'}} type="button">Cancel</button>
                                                </Col>
                                            </Row>
                                        </AvForm>
                                    </CardBody>
                                </Card>
                                </AvForm>
                    </Container>
                        
                </div>
          
                </React.Fragment>
        );
    }
}

export default withNamespaces()(EditClient);

 
 

