import React, { Component } from "react";
import { Row, Col, Card, CardBody, FormGroup, Button, CardTitle, CardSubtitle, Label, Input ,Container, UncontrolledTooltip, Table} from "reactstrap";
import { Link } from "react-router-dom";
import { AvForm, AvField } from "availity-reactstrap-validation";



//Import Breadcrumb
import Breadcrumbs from '../components/Common/Breadcrumb';

//i18n
import { withNamespaces } from 'react-i18next';

class DailyCollectionPayment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            installment:''
        };
    }

    installmentChange=(e)=>{
        this.setState({
            installment :e.target.value
        })
    }

    handleSubmit=()=>{
        if(this.state.installment==''){            
            document.getElementById('installmentVal').innerHTML="Enter Your Installment Number"
            document.getElementById("installmentDiv").style.borderColor = "#f46a6a"
        }else{
            document.getElementById('installmentVal').innerHTML=""
            document.getElementById("installmentDiv").style.borderColor = ""
        }
    }
    render() {
        return (
            <React.Fragment>
                <div className="page-content">
                    <Container fluid>

                        {/* Render Breadcrumb */}
                        <Breadcrumbs title={this.props.t('Daily Collecton')} breadcrumbItem={this.props.t('Payment')} />
                    
                        <Card>
 
                        
                                    <CardBody>
                                    <CardTitle>Payment</CardTitle>
                                    <CardSubtitle className="mb-3">
                                        Enter the basic details which is rquired for the payment.
                                    </CardSubtitle>
                                        <AvForm className="needs-validation" >
                                            <Row>
                                                <Col md="12">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">Account Number</Label>
                                                        <AvField
                                                          name="Account"
                                                          placeholder="Enter Account Number"
                                                          type="text"
                                                          errorMessage="Enter Valid Account Number"
                                                          className="form-control"
                                                          validate={{ required: { value: true } }}
                                                          id="validationCustom01"
                                                        />
                                                    </FormGroup>
                                                </Col>
                                                
                                            </Row>

                                            <Row>
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">Amount To Pay</Label>
                                                        <AvField
                                                        name="Amount"
                                                        placeholder="Enter Your Amount "
                                                        type="number"
                                                        errorMessage="Enter amount to pay"
                                                        className="form-control"
                                                        validate={{ required: { value: true } }}
                                                        id="validationCustom01"
                                                        />
                                                    </FormGroup>
                                                </Col>   
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">Payment Date</Label>
                                                        <AvField
                                                        name="Date"
                                                        placeholder="Date"
                                                        type="date"
                                                        errorMessage="Enter amount to pay"
                                                        className="form-control"
                                                        validate={{ required: { value: true } }}
                                                        id="validationCustom01"
                                                        />
                                                    </FormGroup>
                                                </Col>    
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">Payment Mode</Label>
                                                        <AvField
                                                        name="Mode"
                                                        placeholder="Enter the Mode Of Payment"
                                                        type="text"
                                                        errorMessage="Enter the Payment Mode"
                                                        className="form-control"
                                                        validate={{ required: { value: true } }}
                                                        id="validationCustom01"
                                                        />
                                                    </FormGroup>
                                                </Col>                                       
                                            </Row>

                                            <Row>
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">Current Gold Rate</Label>
                                                        <AvField
                                                        name="GoldRate"
                                                        placeholder="Enter The Today's Gold Rate"
                                                        type="number"
                                                        errorMessage="Enter the current gold rate"
                                                        className="form-control"
                                                        validate={{ required: { value: true } }}
                                                        id="validationCustom01"
                                                        />
                                                    </FormGroup>
                                                </Col>   
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">Consolidated Weight</Label>
                                                        <AvField
                                                        name="Consolidated"
                                                        placeholder="Consolidated Weight"
                                                        type="number"
                                                        errorMessage="Enter Consolidated Weight"
                                                        className="form-control"
                                                        validate={{ required: { value: true } }}
                                                        id="validationCustom01"
                                                        />
                                                    </FormGroup>
                                                </Col>    
                                                
                                                <Col md="4">
                                                <FormGroup>
                                                <Label htmlFor="validationCustom03">Installment Number</Label>
                                                <select class="form-control" value={this.state.installment} onChange={this.installmentChange} id="installmentDiv"> 
                                                <option>Select Installment Number</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                                <option>6</option>
                                            </select>
                                            <p id="installmentVal" style={{color:'#f46a6a', fontSize:"80%"}}></p>
                                            </FormGroup>
                                                </Col>                                      
                                            </Row>
                                                                                       
                                            <button style={{backgroundColor:'#880105', color:'#e1b761',float:'right',  marginRight:'2%'}} type="submit" className={'btn'}
                                            onClick={this.handleSubmit}>Make Payment</button>
                                           
                                        </AvForm>
                                    </CardBody>
                                </Card>
                    </Container>
                        
                </div>
          
                </React.Fragment>
        );
    }
}

export default withNamespaces()(DailyCollectionPayment);








 

 
 