import React, { Component } from "react";
import { Container, Row, Col, Button, Card, CardBody, CardTitle, CardSubtitle, Modal, ModalHeader, ModalBody, ModalFooter, Media, Table } from "reactstrap";
import { Link } from "react-router-dom";
import { MDBDataTable } from "mdbreact";
//Import Breadcrumb
import Breadcrumbs from '../components/Common/Breadcrumb';
import GetApiCall from '../API/GetAPI'
import PostApiCall from '../API/PostAPI'
import Notiflix from "notiflix";
import moment from 'moment';

//i18n
import { withNamespaces } from 'react-i18next';
import GridTables from "./Dashboard/GridTables";

class ViewClients extends Component {
    constructor(props) {
        super(props);
        this.state = {
          allDetails:[],
          isdeleted:false
        };        
    }


    componentDidMount=()=>{  
      
      Notiflix.Loading.Init({
        svgColor : '#880105'
       
      });

      Notiflix.Loading.Dots('Please wait...'); 
    
      GetApiCall.getRequest("Get_MonthlyClientApproved").then(resultdes =>
        resultdes.json().then(obj => {
          console.log(obj.data)
            this.setState({
                allDetails:obj.data
        })
        Notiflix.Loading.Remove();
      }))
    }

    delrow = (id) => {
      
      this.setState({isdeleted:true})
      Notiflix.Loading.Dots('Please wait...'); 
      PostApiCall.postRequest ({
        id:id
      },"Delete_New_MonthlyClient").then((result) =>
   
     result.json().then(obj => {
         if(result.status == 200 || result.status == 201){
            
             console.log(obj)
             // Notiflix.Loading.Remove();
             Notiflix.Notify.Success('Client Deleted Sucessfully.')
             window.location.reload()        
         }else
         {
         //   Notiflix.Loading.Remove();
           Notiflix.Notify.Failure('Something went wrong, try again later.')
         }
     })
     )
    }

    viewdetail=(x)=>{
      if(this.state.isdeleted===false) {return( 
        localStorage.setItem('ClientDetails',[JSON.stringify(x)]),
              
           this.props.history.push('/edit-client')
        )}
      else if(this.state.isdeleted===true){
        return(
          localStorage.setItem('ClientDetails',[JSON.stringify(x)]),
           this.props.history.push('/view-clients')
        )
       
      }
}
    render() {
  


    let rowData=  this.state.allDetails.map(info=>{
      return{ name:info.fld_name,
       mobNo:info.fld_mobile,
       email:info.fld_email,
       id:info.fld_id}
   })

   

        const data = {
            columns: [
              {
                label: "Name",
                field: "name",
                sort: "asc",
                width: 150
              },
              {
                label: "Mobile Number",
                field: "mobNo",
                sort: "asc",
                width: 100
              },
              {
                label: "Email",
                field: "email",
                sort: "asc",
                width: 150
              },
              {
                label: "Action",
                field: "action",
                sort: "asc",
                width: 100
              }
            ],
            rows:
             rowData.map(x=>{return{ 
              name:x.name,
               mobNo:x.mobNo,
               email:x.email,
               clickEvent:() =>{
                if(this.state.isdeleted===true){
                  this.viewdetail(x)
                } 
                },
               action: <div><i className="bx bx-paint mr-4" onClick={()=>this.viewdetail(x)} style={{fontSize:"22px"}}></i>
               <i className="bx bx-trash" onClick={()=>this.delrow(x.id)} style={{fontSize:"22px"}}></i></div>
            }})
          };
        return (
            <React.Fragment>
                <div className="page-content">
                
                    <Container fluid>
                        <div className="px-3">
                            <Breadcrumbs title={this.props.t('Manage Clients')} breadcrumbItem={this.props.t('View Clients')} />
                        </div>
                        {/* Render Breadcrumb */}
                       
                            
                        <React.Fragment>
                        <div className="page-content p-0 m-0">
                          <div className="container">
                
                
                            <Row>
                              <Col className="col-12">
                                <Card>
                                  <CardBody>
                                    <CardTitle>Clients List</CardTitle>
                                    <CardSubtitle className="mb-3">
                                        List of all the approved clients.
                                    </CardSubtitle>
                
                                    <MDBDataTable responsive striped bordered data={data} />
                
                                  </CardBody>
                                </Card>
                              </Col>
                            </Row>
                          </div>
                        </div>
                      </React.Fragment>
                    </Container>
                </div>
          
                </React.Fragment>
        );
    }
}

export default withNamespaces()(ViewClients);
