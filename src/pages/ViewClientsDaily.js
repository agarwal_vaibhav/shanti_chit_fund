import React, { Component } from "react";
import { Container, Row, Col, Button, Card, CardBody, CardTitle, CardSubtitle, Modal, ModalHeader, ModalBody, ModalFooter, Media, Table } from "reactstrap";
import { Link } from "react-router-dom";
import { MDBDataTable } from "mdbreact";
//Import Breadcrumb
import Breadcrumbs from '../components/Common/Breadcrumb';
import GetApiCall from '../API/GetAPI'
import PostApiCall from '../API/PostAPI'
import Notiflix from "notiflix";

//i18n
import { withNamespaces } from 'react-i18next';
import GridTables from "./Dashboard/GridTables";

class ViewClientsDaily extends Component {
    constructor(props) {
        super(props);
        this.state = {
          allDetails:[],
          isdeleted:false
        };        
    }
    componentDidMount=()=>{     
      GetApiCall.getRequest("Get_New_DailyClientList").then(resultdes =>
        resultdes.json().then(obj => {
          // console.log(obj.data)
            this.setState({
                allDetails:obj.data
        })
      }))
    }

    
    delrow = (id) => {
      this.setState({isdeleted:true})
      // e.preventDefault();
    PostApiCall.postRequest ({
      id:id
    },"Delete_New_DailyClient").then((result) =>
 
   result.json().then(obj => {
       if(result.status == 200 || result.status == 201){          
           console.log(obj)
           Notiflix.Notify.Success('Client Deleted Sucessfully.')
           window.location.reload()        
       }else
       {
         Notiflix.Notify.Failure('Something went wrong, try again later.')
       }
   })
   )
    }

    viewdetail=()=>{
      if(this.state.isdeleted===false) {return(        
      this.props.history.push('/edit-clients-daily')
      )}
      else if(this.state.isdeleted===true){
        return(       
          this.props.history.push('/view-clients-daily')
        )       
      }
}

    render() {

    let rowData=  this.state.allDetails.map(info=>{
         return{ name:info.fld_name,
          mobNo:info.fld_mobile,
          user:info.fld_username,
          id:info.fld_id}          
      })
  
    

        const data = {
            columns: [
              {
                label: "Name",
                field: "name",
                sort: "asc",
                width: 150
              },              
              {
                label: "Mobile Number",
                field: "mobNo",
                sort: "asc",
                width: 100
              },              
              {
                label: "User Name",
                field: "user",
                sort: "asc",
                width: 150
              },
              {
                label: "Action",
                field: "action",
                sort: "asc",
                width: 100
              }
            ],
            rows:
             rowData.map(x=>{return{
               name:x.name,
               mobNo:x.mobNo,
               user:x.user,
               clickEvent:() =>{
                if(this.state.isdeleted===true){
                  this.viewdetail()
                } 
                },
               action: <div><i className="bx bx-paint mr-4" onClick={this.viewdetail} style={{fontSize:"22px"}}></i>
               <i className="bx bx-trash" onClick={()=>this.delrow(x.id)} style={{fontSize:"22px"}}></i></div>
            }})
          };
        return (
            <React.Fragment>
                <div className="page-content">
                
                    <Container fluid>
                        <div className="px-3">
                            <Breadcrumbs title={this.props.t('Manage Clients')} breadcrumbItem={this.props.t('View Clients')} />
                        </div>
                        {/* Render Breadcrumb */}
                       
                            
                        <React.Fragment>
                        <div className="page-content p-0 m-0">
                          <div className="container">
                
                
                            <Row>
                              <Col className="col-12">
                                <Card>
                                  <CardBody>
                                    <CardTitle>Clients List</CardTitle>
                                    <CardSubtitle className="mb-3">
                                        List of all the approved clients.
                                    </CardSubtitle>
                
                                    <MDBDataTable responsive striped bordered data={data} />
                
                                  </CardBody>
                                </Card>
                              </Col>
                            </Row>
                          </div>
                        </div>
                      </React.Fragment>
                    </Container>
                </div>
          
                </React.Fragment>
        );
    }
}

export default withNamespaces()(ViewClientsDaily);



