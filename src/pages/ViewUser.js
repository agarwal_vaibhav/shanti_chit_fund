import React, { Component } from "react";
import { Container, Row, Col, Button, Card, CardBody, CardTitle, CardSubtitle, Modal, ModalHeader, ModalBody, ModalFooter, Media, Table } from "reactstrap";
import { Link } from "react-router-dom";
import { MDBDataTable } from "mdbreact";
//Import Breadcrumb
import Breadcrumbs from '../components/Common/Breadcrumb';

//i18n
import { withNamespaces } from 'react-i18next';
import GridTables from "./Dashboard/GridTables";

class ViewUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
          
        };        
    }

    delrow = () => {
        
    }

    viewdetail=()=>{
      return(
        console.log("hello"),
      this.props.history.push('/edit-user')
      )}

    render() {

    let rowData=[
        {
            name: "Tiger Nixon",
            accNo: "pk1245t",
            mobNo: "7456921258",
            chitscheme: "abc@gmail.com",
            user: "new delhi",
           
          },
          {
            name: "Tiger Nixon",
            accNo: "pk1245t",
            mobNo: "7456921258",
            chitscheme: "abc@gmail.com",
            user: "new delhi",       
           },
          {
            name: "Tiger Nixon",
            accNo: "pk1245t",
            mobNo: "7456921258",
            chitscheme: "abc@gmail.com",
            user: "new delhi",
          },
          {
            name: "Tiger Nixon",
            accNo: "pk1245t",
            mobNo: "7456921258",
            chitscheme: "abc@gmail.com",
            user: "new delhi",
          },
          
      ]

        const data = {
            columns: [
              {
                label: "Name",
                field: "name",
                sort: "asc",
                width: 150
              },
              {
                label: "Account Number",
                field: "accNo",
                sort: "asc",
                width: 100
              },
              {
                label: "Mobile Number",
                field: "mobNo",
                sort: "asc",
                width: 100
              },
              {
                label: "Email id",
                field: "chitscheme",
                sort: "asc",
                width: 150
              },
              {
                label: "Address",
                field: "user",
                sort: "asc",
                width: 150
              },
              {
                label: "Action",
                field: "action",
                sort: "asc",
                width: 100
              }
            ],
            rows:
             rowData.map(x=>{return{
               name:x.name,
               accNo:x.accNo,
               mobNo:x.mobNo,
               chitscheme:x.chitscheme,
               user:x.user,
               clickEvent:() =>{this.viewdetail()},
               action: <div><i className="bx bx-paint mr-4" style={{fontSize:"22px"}}></i>
               <i className="bx bx-trash" onClick={this.delrow} style={{fontSize:"22px"}}></i></div>
            }})
          };
        return (
            <React.Fragment>
                <div className="page-content">
                    <Container fluid>

                    <div className="px-3">
                        <Breadcrumbs title={this.props.t('User Managment')} breadcrumbItem={this.props.t('View Users')} />
                    </div>        
                        <React.Fragment>
                        <div className="page-content p-0">
                          <div className="container-fluid">
                
                
                            <Row>
                              <Col className="col-12">
                                <Card>
                                  <CardBody>
                                    <CardTitle>Users List</CardTitle>
                                    <CardSubtitle className="mb-3">
                                        List of all the approved users.
                                    </CardSubtitle>
                
                                    <MDBDataTable responsive striped bordered data={data} />
                
                                  </CardBody>
                                </Card>
                              </Col>
                            </Row>
                          </div>
                        </div>
                      </React.Fragment>
                        </Container>
                </div>
          
                </React.Fragment>
        );
    }
}

export default withNamespaces()(ViewUser);
