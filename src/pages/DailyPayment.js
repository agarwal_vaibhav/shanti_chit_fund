import React, { Component } from "react";
import { Row, Col, Card, CardBody, FormGroup, Button, CardTitle, CardSubtitle, Label, Input ,Container, UncontrolledTooltip, Table} from "reactstrap";
import { Link } from "react-router-dom";
import { AvForm, AvField } from "availity-reactstrap-validation";
import PostApiCall from '../API/PostAPI';
import Notiflix from "notiflix";
import moment from 'moment'




//Import Breadcrumb
import Breadcrumbs from '../components/Common/Breadcrumb';

//i18n
import { withNamespaces } from 'react-i18next';

class DailyPayment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            accountNo:'',
            accountToPay:'',
            paymentDate:'',
            paymentMode:'',
            consolidatedWeight:'',

        };
    }

    makePayment =()=>{
        if(this.state.accountNo==''){
            Notiflix.Notify.Failure("Enter Account Number")
        }else if(this.state.accountToPay==''){
            Notiflix.Notify.Failure("Enter Amount To Pay")
        }else if(this.state.paymentDate==''){
            Notiflix.Notify.Failure("Select The Payment Date")
        }else if(this.state.paymentMode==''){
            Notiflix.Notify.Failure("Enter the Payment Mode")
        }else if(this.state.consolidatedWeight==''){
            Notiflix.Notify.Failure("Enter Consolidated Weight")
        }else{
            PostApiCall.postRequest({
                accountno:this.state.accountNo,
                paymentdate:this.state.paymentDate,
                paymentmode:this.state.paymentMode,
                amounttopay:this.state.amounttopay,
                consolatedweight:this.state.consolidatedWeight,
                updatedon:moment().format('lll'),
                updatedby:1
            }, "Add_monhlyOfflinePayments").then((result)=>
                result.json().then(obj => {
                if(result.status == 200 || result.status == 201){
                   
                    console.log(obj)
                    Notiflix.Notify.Success('Payment Completed.')
                    window.location.reload()
                  
                }else
                {
                  Notiflix.Notify.Failure('Something went wrong, try again later.')
                }
            })
            )
        }
    }

    render() {
        return (
            <React.Fragment>
                <div className="page-content">
                    <Container fluid>

                        {/* Render Breadcrumb */}
                        <Breadcrumbs title={this.props.t('OfflinePayment')} breadcrumbItem={this.props.t('Payment')} />
                    
                        <Card>
 
                        
                                    <CardBody>
                                    <CardTitle>Offline Payment</CardTitle>
                                    <CardSubtitle className="mb-3">
                                        Enter the payment details related to particular account.
                                    </CardSubtitle>
                                        <AvForm className="needs-validation" >
                                            <Row>
                                                <Col md="12">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">Account Number</Label>
                                                        <AvField
                                                          name="Account"
                                                          value={this.state.accountNo}
                                                          onChange={(text)=>{ 
                                                            this.setState({
                                                                accountNo : text.target.value
                                                            }) 
                                                        }}
                                                          placeholder="Enter Account Number"
                                                          type="text"
                                                          errorMessage="Enter Valid Account Number"
                                                          className="form-control"
                                                          validate={{ required: { value: true } }}
                                                          id="validationCustom01"
                                                        />
                                                    </FormGroup>
                                                </Col>
                                                
                                            </Row>

                                            <Row>
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom03">Amount To Pay</Label>
                                                        <AvField
                                                        name="Amount"
                                                        value={this.state.accountToPay}
                                                        onChange={(text)=>{ 
                                                            this.setState({
                                                                accountToPay : text.target.value
                                                            }) 
                                                        }}
                                                        placeholder="Enter Your Amount "
                                                        type="number"
                                                        errorMessage="Enter amount to pay"
                                                        className="form-control"
                                                        validate={{ required: { value: true } }}
                                                        id="validationCustom01"
                                                        />
                                                    </FormGroup>
                                                </Col>   
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">Payment Date</Label>
                                                        <AvField
                                                        name="Date"
                                                        value={this.state.paymentDate}
                                                        onChange={(text)=>{ 
                                                            this.setState({
                                                                paymentDate : text.target.value
                                                            }) 
                                                        }}
                                                        type="date"
                                                        errorMessage="Select The Payment Date "
                                                        className="form-control"
                                                        validate={{ required: { value: true } }}
                                                        id="validationCustom01"
                                                        />
                                                    </FormGroup>
                                                </Col>    
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">Payment Mode</Label>
                                                        <AvField
                                                        name="Mode"
                                                        placeholder="Enter the Mode Of Payment"
                                                        value={this.state.paymentMode}
                                                        onChange={(text)=>{ 
                                                            this.setState({
                                                                paymentMode : text.target.value
                                                            }) 
                                                        }}
                                                        type="text"
                                                        errorMessage="Enter the Payment Mode"
                                                        className="form-control"
                                                        validate={{ required: { value: true } }}
                                                        id="validationCustom01"
                                                        />
                                                    </FormGroup>
                                                </Col>                                       
                                            </Row>

                                            <Row>
                                                {/*<Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">Current Gold Rate</Label>
                                                        <AvField
                                                        name="GoldRate"
                                                        placeholder="Enter The Today's Gold Rate"
                                                        type="number"
                                                        errorMessage="Please enter the gold rate"
                                                        className="form-control"
                                                        validate={{ required: { value: true } }}
                                                        id="validationCustom01"
                                                        />
                                                    </FormGroup>
        </Col> */  }
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">Consolidated Weight</Label>
                                                        <AvField
                                                        name="Consolidated"
                                                        value={this.state.consolidatedWeight}
                                                        onChange={(text)=>{ 
                                                            this.setState({
                                                                consolidatedWeight : text.target.value
                                                            }) 
                                                        }}
                                                        placeholder="Consolidated Weight"
                                                        type="number"
                                                        errorMessage="Enter Consolidated Weight"
                                                        className="form-control"
                                                        validate={{ required: { value: true } }}
                                                        id="validationCustom01"
                                                        />
                                                    </FormGroup>
                                                </Col>    
                                                                                    
                                            </Row>
                                                                                       
                                            <button style={{backgroundColor:'#880105', color:'#e1b761', float:"right", marginRight:"2%"}} className='btn' type="submit" onClick={this.makePayment}>Save Payment Details</button>
                                           
                                        </AvForm>
                                    </CardBody>
                                </Card>
                    </Container>
                        
                </div>
          
                </React.Fragment>
        );
    }
}

export default withNamespaces()(DailyPayment);








 