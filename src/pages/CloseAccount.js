import React, { Component } from "react";
import { Row, Col, Card, CardBody, FormGroup, Button, CardTitle, CardSubtitle, Label, Input ,Container, UncontrolledTooltip, Table} from "reactstrap";
import { Link } from "react-router-dom";
import { AvForm, AvField } from "availity-reactstrap-validation";
//Import Breadcrumb
import Breadcrumbs from '../components/Common/Breadcrumb';

//i18n
import { withNamespaces } from 'react-i18next';
import PostApiCall from "../API/PostAPI";
import Notiflix from "notiflix";
import moment from 'moment';

class CloseAccount extends Component {
    constructor(props) {
        super(props);
        this.state = {
            remark:'',
            accountNo:''
        };
    }
    remarkChange=(e)=>{
        this.setState({
            remark:e.target.value
        })
    }
    accountChange=(e)=>{
        this.setState({
            accountNo:e.target.value
        })
    }
    handlesubmit=()=>{
        if(this.state.remark==''){
            document.getElementById('remarkVal').innerHTML="Enter Your Remark"
            document.getElementById("remark").style.borderColor = "#f46a6a"           
        }else{
            var det=localStorage.getItem('loginedUser')
            var detail=JSON.parse(det)
            Notiflix.Loading.Dots('Please wait...');
            PostApiCall.postRequest ({
                accountno : this.state.accountNo,
                remarks: this.state.remark,               
                status : "Closed",
                updatedon : moment().format('lll'),
                updatedby : detail[0].fld_userid,              
            
            },"Add_New_CloseAccount").then((result) =>
          
            result.json().then(obj => {
                if(result.status == 200 || result.status == 201){
                    console.log(obj)
                    Notiflix.Notify.Success('Account Closed Successfully')
                    Notiflix.Loading.Remove();
                    window.location.reload()
                }else
                {
                  Notiflix.Notify.Failure(obj.data)
                }
            })
            )
        }
    }    

    render() {
        return (
            <React.Fragment>
                <div className="page-content">
                    <Container fluid>

                        {/* Render Breadcrumb */}
                        <Breadcrumbs title={this.props.t('CloseAccount')} breadcrumbItem={this.props.t('Close Account')} />
                    
                        <Card>
                                    <CardBody>

                                        <CardTitle>Close Account</CardTitle>
                                        <CardSubtitle className="mb-3">
                                            Enter the basic details which is rquired for the Account Closer.
                                        </CardSubtitle>
                                        <AvForm className="needs-validation" >
                                            <Row>
                                                <Col md="12">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">Enter Account Number</Label>
                                                        <AvField
                                                          name="Account"
                                                          placeholder="Account Number"
                                                          type="text"
                                                          errorMessage="Enter Valid Account Number"
                                                          className="form-control"
                                                          validate={{ required: { value: true } }}
                                                          id="validationCustom01"
                                                          value={this.state.accountNo}
                                                          onChange={this.accountChange}
                                                        />
                                                    </FormGroup>
                                                </Col>                                                
                                            </Row>

                                            <Row>
                                                <Col md="12">
                                                <FormGroup>
                                                    <Label htmlFor="validationCustom01">Remarks</Label>
                                                    <textarea
                                                      style={{resize:'none'}}
                                                      name="Remarks"
                                                      placeholder="Remarks"
                                                      type="text"
                                                      errorMessage="Remarks"
                                                      className="form-control"
                                                      validate={{ required: { value: true } }}
                                                      id="remark"
                                                      value={this.state.remark}
                                                      onChange={this.remarkChange}
                                                    />
                                                    <p id="remarkVal" style={{color:'#f46a6a', fontSize:"80%"}}></p>
                                                </FormGroup>
                                                </Col>   
                                                                                       
                                            </Row>
                                                                                       
                                            <button className={'btn'} type="submit" 
                                            style={{backgroundColor:'#880105', color:'#e1b761',float:'right',  marginRight:'2%'}} onClick={this.handlesubmit}>Close Account</button>                                           
                                        </AvForm>
                                    </CardBody>
                                </Card>
                    </Container>
                        
                </div>
          
                </React.Fragment>
        );
    }
}

export default withNamespaces()(CloseAccount);



