import React, { Component } from "react";
import { Container, Row, Col, Button, Card, CardBody, CardSubtitle,CardTitle, Modal, ModalHeader, ModalBody, ModalFooter, Media, Table } from "reactstrap";
import { Link } from "react-router-dom";
import { MDBDataTable } from "mdbreact";
//Import Breadcrumb
import Breadcrumbs from '../components/Common/Breadcrumb';
//i18n
import { withNamespaces } from 'react-i18next';
import GetApiCall from "../API/GetAPI";

class CloseAccountList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allDetails:[]
        };
    }


    componentDidMount=()=>{   
      GetApiCall.getRequest("Get_New_AllClosedAccountList").then(resultdes =>
        resultdes.json().then(obj => {
          console.log(obj.data)
            this.setState({
                allDetails:obj.data
        })
      }))
    }


    render() {
      let rowData=  this.state.allDetails.map(info=>{
        return{ accountNumber:info.fld_accountNumber,
          closedate:info.fld_updatedon,
          remark:info.fld_remarks,
         id:info.fld_id}
     })

        const data = {
            columns: [
              {
                label: "S no.",
                field: "Sno",
                sort: "asc",
                width: 50
              },
              {
                label: "Account number",
                field: "accountNumber",
                sort: "asc",
                width: 270
              },
              {
                label: "Closed Date",
                field: "closedate",
                sort: "asc",
                width: 200
              },
              {
                label: "Remark",
                field: "remark",
                sort: "asc",
                width: 150
              }            
            ],
            rows: rowData.map((x,index)=>{return{ 
              Sno:index+1,
              accountNumber:x.accountNumber,
               closedate:x.closedate,
               remark:x.remark
            }})
          };

        return (
            <React.Fragment>
                <div className="page-content">
                    <Container fluid>
                      <div className="px-4">
                        <Breadcrumbs title={this.props.t('Close Account')} breadcrumbItem={this.props.t('Closed Account List')} />
                      </div>      
                        <div className="page-content pt-0 m-0">
                            <div className="container">     
                                <Row>
                                    <Col className="col-12">
                                    <Card>
                                        <CardBody> 
                                        <CardTitle>Closed Account List</CardTitle>
                                          <CardSubtitle className="mb-3">
                                              List of all the closed Account
                                          </CardSubtitle>
                                                                               
                                        <MDBDataTable responsive striped bordered data={data} />
                    
                                        </CardBody>
                                    </Card>
                                    </Col>
                                </Row>
                            </div>
                        </div>



                        </Container>
                </div>
          
                </React.Fragment>
        );
    }
}

export default withNamespaces()(CloseAccountList);



