import React, { Component } from 'react'
import Breadcrumbs from '../components/Common/Breadcrumb'
import { Container, Row, Col, Button, Card, CardBody, CardTitle, CardSubtitle, Modal, ModalHeader, ModalBody, ModalFooter, Media, Table } from "reactstrap";
import { MDBDataTable } from "mdbreact";
import Notiflix from "notiflix";

 class ViewStaff extends Component {
    render() {

        const data = {
            columns: [
              {
                label: "S.No",
                field: "s_no",
                sort: "asc",
                width: 150
              },              
              {
                label: "Account No",
                field: "accountNo",
                sort: "asc",
                width: 100
              },              
              {
                label: "Chit Scheme",
                field: "chit_scheme",
                sort: "asc",
                width: 150
              },
              {
                label: "Installment",
                field: "installment",
                sort: "asc",
                width: 150
              },
              {
                label: "Amount",
                field: "amount",
                sort: "asc",
                width: 150
              },
             
            ],
            rows:[
                {
                s_no:12345,
                accountNo:170303105,
                chit_scheme:'bawal Scheeme',
                installment:6,
                amount:10000
            },
                {
                s_no:12345,
                accountNo:170303105,
                chit_scheme:'Zeher Scheeme',
                installment:6,
                amount:10000
            },
                {
                s_no:12345,
                accountNo:170303105,
                chit_scheme:'Katil Scheeme',
                installment:6,
                amount:10000
            },
                {
                s_no:12345,
                accountNo:170303105,
                chit_scheme:'Fadu Scheeme',
                installment:6,
                amount:10000
            },
        ]
           
          };
        return (
            <React.Fragment>
            <div className="page-content">
            
                <Container fluid>
                    <div className="px-3">
                        <Breadcrumbs title={'Viwe Staffs'} breadcrumbItem={'Clients Details'} />
                    </div>
                    {/* Render Breadcrumb */}
                   
                        
                    <React.Fragment>
                    <div className="page-content p-0 m-0">
                      <div className="container">
            
            
                        <Row>
                          <Col className="col-12">
                            <Card>
                              <CardBody>
                                <CardTitle>Clients List</CardTitle>
                                <CardSubtitle className="mb-3">
                                    List of all the approved clients.
                                </CardSubtitle>
            
                                <MDBDataTable responsive striped bordered data={data} />
            
                              </CardBody>
                            </Card>
                          </Col>
                        </Row>
                      </div>
                    </div>
                  </React.Fragment>
                </Container>
            </div>
      
            </React.Fragment>
   
       
        )
    }
}
export default ViewStaff