import React, { Component } from "react";
import { Container, Row, Col, Button, Card, CardBody, Label, FormGroup, CardTitle, Modal, ModalHeader, ModalBody, ModalFooter, Media, Table, Form } from "reactstrap";
import { Link } from "react-router-dom";
import { MDBDataTable } from "mdbreact";
//Import Breadcrumb
import Breadcrumbs from '../components/Common/Breadcrumb';
//i18n
import { withNamespaces } from 'react-i18next';
import { AvForm, AvField } from "availity-reactstrap-validation";
import PostApiCall from '../API/PostAPI'
import Notiflix from "notiflix";
import moment from 'moment';

import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';

class RegistrationApproval extends Component {
    constructor(props) {
        super(props);
        this.state = {          
            modal_standard: false,
            personalData:[],
            name:'',
            mob:'',
            email:'',
            id:'',
            schemadata:[],
            Id:'',
            Client:'',
            AccountNumber:'',
            ChitAmount:'',
            ChitScheme:'',
            Duration:'',
            Accountregex : /^[a-zA-Z]{2}\d*$/,
            freqentChitfundData:[ 
              {label:"Select Chit Scheme",value:''},
              {label:"Thanga Mazhai",value:'Thanga Mazhai'},
              {label:"Thanga Then",value:'Thanga Then'},
              {label:"Thanga Malar",value:'Thanga Malar'},
              {label:"My Gold Eleven",value:'My Gold Eleven'},
          ],
          freqentChitfundPriceData:[
              {label:"Select Chit Amount",value:''},
              {label:"500",value:'500'},
              {label:"1000",value:'1000'},
              {label:"2500",value:'2500'},
              {label:"5000",value:'5000'},
              {label:"10000",value:'10000'},
          ],
        
       
          freqentDurationData:[
              {label:"Select Chit Duration",value:''},
              {label:"11",value:'11'},
              {label:"15",value:'15'},

          ],
          ClientID:''
        };
    }

    // handleClick = () =>{
    //     console.log("hello"),
        
    //     onClick:{this.tog_standard}
    //     datatoggle:"modal"
    //     datatarget:"#myModal"
    //     this.setState({...this.state,modal:true})
        
    // }


    componentDidMount=()=>{  
      
      Notiflix.Loading.Init({
        svgColor : '#880105'
       
      });
    }
    
    tog_standard() {
        this.setState(prevState => ({
          modal_standard: !prevState.modal_standard
        }));
        this.removeBodyCss();
    }
    removeBodyCss() {
        document.body.classList.add("no_padding");
      }
    reject=(id)=>{
      this.setState(prevState => ({
        modal_standard: !prevState.modal_standard
      }));

      PostApiCall.postRequest ({
        id:id
      },"Delete_MonthlyClientSpecificSchema").then((result) =>
   
     result.json().then(obj => {
         if(result.status == 200 || result.status == 201){
            
             console.log(obj)
             Notiflix.Notify.Success('Chit Schema Deleted Sucessfully.')
             window.location.reload()        
         }else
         {
           Notiflix.Notify.Failure('Something went wrong, try again later.')
         }
     })
     )

    }

    // approve=()=>{
    //   this.setState(prevState => ({
    //     modal_standard: !prevState.modal_standard
    //   }));
    // }

    componentDidMount=()=>{
      Notiflix.Loading.Init({
        svgColor : '#880105'
       
      });

     var abc= localStorage.getItem('Registered')

      // console.log(abc)
      this.setState({
        personalData:JSON.parse(abc), 
      })

      this.setState({
        ClientID:JSON.parse(abc).id
      })


      Notiflix.Loading.Dots('Please wait...'); 
      // console.log(JSON.parse(abc))

    PostApiCall.postRequest ({
        id:JSON.parse(abc).id
      },"Get_New_SpecificMonthlySchema").then((result) =>
    
      result.json().then(obj => {
          if(result.status == 200 || result.status == 201){
              // console.log(obj.data)
              this.setState({
                schemadata:obj.data
              })
          }
      })
      )
      Notiflix.Loading.Remove();
      
    }

    


    UpdateDaily(){
      if(this.state.AccountNumber!=''){
        if(this.state.Accountregex.test(this.state.AccountNumber)){
          if(this.state.ChitScheme!=''){
            if(this.state.ChitAmount!=''){
              if(this.state.Duration!=''){
                this.onUpdatePost();

              }
          else{
                  Notiflix.Notify.Failure('please enter duration')
                }
            }
     else{
          Notiflix.Notify.Failure('please enter chit amount')
        }
          }
        else{
          Notiflix.Notify.Failure('please enter chit scheme')
        }
        }
        else{
          Notiflix.Notify.Failure('please enter account number using alphabet')
        }

      }
      else{
        Notiflix.Notify.Failure('please enter account number')
      }
    }


    onUpdatePost= () => {

      
      var det=localStorage.getItem('loginedUser')
      var detail=JSON.parse(det)


  PostApiCall.postRequest ({
    id:this.state.Id,
    client: this.state.Client,
    accountno :this.state.AccountNumber,
    chitschema : this.state.ChitScheme,
    chitamount :this.state.ChitAmount,
    duration : this.state.Duration,
    updatedon : moment().format('lll'),
    updatedby : detail[0].fld_userid,

  
  },"Update_New_MonthlyClient_ChitSchema").then((result) =>

  result.json().then(obj => {
      if(result.status == 200 || result.status == 201){
        
          Notiflix.Loading.Remove();
          Notiflix.Notify.Success('Client Application Successfully updated.')
          window.location.reload()
      }else
      {
        Notiflix.Loading.Remove();
        Notiflix.Notify.Failure('Something went wrong, try again later.')
      }
  })
  )

    }

    

    approve(x){
      this.setState(prevState => ({
        modal_standard: !prevState.modal_standard
      }));
      confirmAlert({
      title: 'Confirm to Approve',
            message: 'Are you sure you want to approve Chit Scheme request.',
            buttons: [
              {
                label: 'Yes',
                onClick: () => {
    
         Notiflix.Loading.Dots('');
    
         var det=localStorage.getItem('loginedUser')
         var detail=JSON.parse(det)
         
            PostApiCall.postRequest(
                {
                   id:this.state.ClientID,
                   clientStatus:'Approved',
                   updatedon : moment().format('lll'),
                   updatedby : detail[0].fld_userid,
          
                },
                "Update_MonthlyClient_Status"
              ).then((results) =>
                results.json().then((obj) => {

                  if (results.status == 200 || results.status == 201) {
// console.log(obj.data)
                    PostApiCall.postRequest(
                      {
                         id:this.state.schemadata.fld_client,
                         clientStatus:'Approved',
                         updatedon : moment().format('lll'),
                         updatedby : detail[0].fld_userid,
                
                      },
                      "Update_New_MonthlyClient_ChitSchema_Status"
                    ).then((results1) =>
                      results1.json().then((obj1) => {
            if (results1.status == 200 || results1.status == 201) {
              // console.log(obj1.data)
                  Notiflix.Loading.Remove();
                   Notiflix.Notify.Success('Chit successfully Approved.')
                   window.location.href='/register-customers'
               
               }else
               {
               Notiflix.Loading.Remove();
               Notiflix.Notify.Failure('Something went wrong, try again later.')
               }
                }))
          
         
                }else
                {
                  Notiflix.Loading.Remove();
                  Notiflix.Notify.Failure('Something went wrong, try again later.')
               
                }
          }))
    
        }
      },
      {
        label: 'No',
        // onClick: () => alert('Click No')
      }
      ]
      });
    }


    

    render() {
     
      



      let rowData =  this.state.schemadata.map((info, index)=>{
        console.log(info)
        return{ Sno:index+1,
                accountNumber:info.fld_accountno,
                chitscheme:info.fld_chitschema,
                chitamount:info.fld_chitamount,
                duration:info.fld_duration,
                id:info.fld_id,
                client:info.fld_client,
              }
     })

        const data = {
            columns: [
              {
                label: "S no.",
                field: "Sno",
                sort: "asc",
                width: 50
              },
              {
                label: "Account number",
                field: "accountNumber",
                sort: "asc",
                width: 270
              },
              {
                label: "Chit Scheme",
                field: "chitscheme",
                sort: "asc",
                width: 200
              },
              {
                label: "Chit Amount",
                field: "chitamount",
                sort: "asc",
                width: 150
              },
              {
                label: "Duration",
                field: "duration",
                sort: "asc",
                width: 150
              },
              {
                label: "Action",
                field: "action",
                sort: "asc",
                width: 250
              }                  
            ],
          //   rows: [
          //     {
          //       Sno: "1",
          //       accountNumber: "20215493345",
          //       chitscheme: "scheme1",
          //       chitamount: "61",
          //       duration: "11 months",
          //       clickEvent:() =>{this.tog_standard()},
          //       dataToggle:"modal",
          //       dataTarget:"#myModal",
          //       action:<div className="btn-group"><Button>Edit</Button> <button className="btn btn-danger ml-1" onClick={this.reject}>Reject</button>
          //       <button className="btn btn-success ml-1" onClick={this.approve}>Approve</button></div>,
          //   },
          //   {
          //       Sno: "2",
          //       accountNumber: "20215493345",
          //       chitscheme: "scheme1",
          //       chitamount: "61",
          //       duration: "11 months",
          //   }
          //   ]
          // };


        
          rows:
          
                   rowData.map(x=>{return{ 
                     Sno:x.Sno,
                     accountNumber:x.accountNumber,
                     chitscheme:x.chitscheme,
                     chitamount:x.chitamount,
                     duration:x.duration,
                      clickEvent:() =>{this.tog_standard()},
                          dataToggle:"modal",
                          dataTarget:"#myModal",
                          action:<div className="btn-group">
                      <Button
                          onClick={()=>{
                            this.setState({
                                  // Sno: 1
                                  AccountNumber: x.accountNumber,
                                  ChitAmount: x.chitamount,
                                  ChitScheme: x.chitscheme,
                                  Duration: x.duration,
                                  Id: x.id,
                                  Client:x.client
                            })
                            // console.log(x.client)
                             
                            // this.props.setArticalTag(data.fld_tag)
                       
                          }}>Edit</Button> 
                           
                           
                           <button className="btn btn-danger ml-1" onClick={()=>this.reject(x.id)}>Reject</button>
                          <button className="btn btn-success ml-1" onClick={()=>this.approve(x)}>Approve</button></div>       
                  }})
                };

        return (
            <React.Fragment>
                <div className="page-content">
                    <Container fluid>

                        {/* Render Breadcrumb */}
                        <Breadcrumbs title={this.props.t('Registration Approval')} breadcrumbItem={this.props.t('Registration Application')} />
                            
                        <div className="page-content pt-0">
                        


                      <Card className={"px-3"} style={{width:'97%', marginLeft:"1%"}}>
                        <CardBody className={"p-1"}>
                        <h5 className={"mt-2"}> Client Personal Details</h5>
                            <AvForm className="needs-validation ml-2">
                              <Row>
                                  <Col md="4">
                                      <FormGroup>
                                          <Label htmlFor="validationCustom01">Name</Label>
                                          <AvField
                                            name="Name"
                                            placeholder="John"
                                            type="text"
                                            errorMessage="Enter Name"
                                            className="form-control"
                                            value={this.state.personalData.name}
                                            validate={{ required: { value: true } }}
                                            id="validationCustom01" disabled
                                          />
                                      </FormGroup>
                                  </Col>
                                  <Col md="4">
                                      <FormGroup>
                                          <Label htmlFor="validationCustom01">Mobile Number</Label>
                                          <input
                                            name="Mobile"
                                            placeholder="1234567890"
                                            type="number"
                                            errorMessage="Enter Mobile Number"
                                            value={this.state.personalData.mobNo}
                                            className="form-control"
                                            validate={{ required: { value: true } }}
                                            id="validationCustom01"
                                            disabled
                                          />
                                      </FormGroup>
                                   </Col>
                                   <Col md="4">
                                    <FormGroup>
                                        <Label htmlFor="validationCustom01">Email</Label>
                                        <input
                                          name="Email"
                                          placeholder="John@gmail.com"
                                          type="text"
                                          value={this.state.personalData.email}
                                          errorMessage="Enter User Name"
                                          className="form-control"
                                          validate={{ required: { value: true } }}
                                          id="validationCustom01"
                                          disabled
                                        />
                                    </FormGroup>
                                  </Col>
                                </Row>
                            </AvForm>
                         </CardBody>
                     </Card>

                            <div className="container-fluid">     
                                <Row>
                                    <Col className="col-12">
                                    <Card>
                                        <CardBody> 
                                                                               
                                        <MDBDataTable responsive striped bordered data={data} />
                    
                                        </CardBody>
                                    </Card>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                        
                    <Modal isOpen={this.state.modal_standard} toggle={this.tog_standard}>
                        <div className="modal-header">
                          <h5 className="modal-title mt-0" id="myModalLabel">Approval Registration</h5>
                          <button type="button" onClick={() =>this.setState({ modal_standard: false })}
                            className="close" data-dismiss="modal" aria-label="Close" >
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div className="modal-body">
                          <h5>Details of the Chit</h5>
                                                    
                            <div className={"form-group mt-4"}>
                                <label for={"account"}>Account Number</label>
                                <input type="text" className="form-control" placeholder="Account Number"
                                value={this.state.AccountNumber}
                                onChange={(text)=>{
                                  this.setState({
                                    AccountNumber:text.target.value
                                  })
                                }}
                                />
                            </div>
                            <div className={"form-group"}>
                                <label for={"account"}>Chit Scheme</label>
                                
                                <select className="form-control"  value={this.state.Duration}
                                value={this.state.ChitScheme}
                                onChange={(text)=>{
                                  this.setState({
                                    ChitScheme:text.target.value
                                  })
                                }}>
                                {this.state.freqentChitfundData.map(year => (
                                  <option key={year.value} value={year.value}>
                                          {year.label}
                                      </option>
                                      ))}
                                </select>
                            </div>
                            <div className={"form-group"}>
                                <label for={"account"}>Chit Amount</label>
                                
                                <select className="form-control"  value={this.state.Duration}
                                value={this.state.ChitAmount}
                                onChange={(text)=>{
                                  this.setState({
                                    ChitAmount:text.target.value
                                  })
                                }}>
                                {this.state.freqentChitfundPriceData.map(year => (
                                  <option key={year.value} value={year.value}>
                                          {year.label}
                                      </option>
                                      ))}
                                </select>
                            </div>
                            <div className={"form-group"}>
                                <label for={"account"}>Duration</label>
                                
                                <select className="form-control"  value={this.state.Duration}
                                onChange={(text)=>{
                                  this.setState({
                                    Duration:text.target.value
                                  })
                                }}>
                                {this.state.freqentDurationData.map(year => (
                                  <option key={year.value} value={year.value}>
                                          {year.label}
                                      </option>
                                      ))}
                                </select>
                            </div>
                          
                            <br/>
                          <button type="button" className="btn btn-success ml-4"
                          onClick={this.UpdateDaily.bind(this)}
                          
                          >Update</button>
                          <button type="button" className="btn btn-danger ml-4"
                          onClick={() =>this.setState({ modal_standard: false })} 
                          data-dismiss="modal" aria-label="Close"
                          >Cancel</button>
                          <br/>
                        </div>
                 
                    </Modal>

                    </Container>
                </div>
          
                </React.Fragment>
        );
    }
}

export default withNamespaces()(RegistrationApproval);




