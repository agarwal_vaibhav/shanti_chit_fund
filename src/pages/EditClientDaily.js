import React, { Component } from "react";
import { Row, Col, Card, CardBody, FormGroup, Button, CardTitle, CardSubtitle, Label, Input, textarea,Container, UncontrolledTooltip, Table} from "reactstrap";
import { Link } from "react-router-dom";
import { AvForm, AvField } from "availity-reactstrap-validation";
import './CssForAll.css'


//Import Breadcrumb
import Breadcrumbs from '../components/Common/Breadcrumb';

//i18n
import { withNamespaces } from 'react-i18next';

class EditClientDaily extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newClient:{
                accountNumber:'',
                chitScheme:'',
                chitAmount:'',
                duration:'',
                user:''
            },
            clients:[],
            showData:true,
            arrIndex:'',
            editOption:false,
            dropdown:"",
            address:'',
            country:'',
            state:'',
            city:'',
            area:'',
            //personal details
            name:'vaibhav',
            username:'vaibhav@21',
            Email:'vaibhav@gmail.com',
            Mobile:'8092123722',
            landmark:'',
            pin:'',
            visiblity:true
        };
    }

    handleInput = (e) => {
        let input_data = Object.assign({}, this.state.newClient);
       
        const fields = e.target.id;
        input_data[fields] = e.target.value;
  
          this.setState({
               newClient : input_data
          })       
    }

    submitDataHandler = () => {   
        
        if(this.state.newClient.chitScheme==''){
            document.getElementById('schemeVal').innerHTML="Select Your Chit Scheme"
            document.getElementById("chitScheme").style.borderColor = "#f46a6a"           
        }
        if(this.state.newClient.chitAmount==''){
            document.getElementById('amountVal').innerHTML="Select Your Chit Amount"
            document.getElementById("chitAmount").style.borderColor = "#f46a6a"           
        }
        if(this.state.newClient.chitAmount==''){
            document.getElementById('durationVal').innerHTML="Select your Duration"
            document.getElementById("duration").style.borderColor = "#f46a6a"           
        }
        if(this.state.newClient.chitAmount==''){
            document.getElementById('userVal').innerHTML="Select User Type"
            document.getElementById("user").style.borderColor = "#f46a6a"           
        }

        else{                
            if(this.state.editOption === true){
              let {clients, arrIndex, newClient} = this.state;
              let new_data = Object.assign([], clients);
              new_data.splice(arrIndex, 1);
              new_data.push(newClient);
              this.setState({
                clients: new_data,
                newClient: {
                    accountNumber:'',
                    chitScheme:'',
                    chitAmount:'',
                    duration:'',
                    user:''
            },
              }, () => {
                console.log('state ', this.state.clients);
              })
             
            } else{
              this.state.clients.push(this.state.newClient)
              this.setState({
                showData: true,
                newClient: {
                    accountNumber:'',
                    chitScheme:'',
                    chitAmount:'',
                    duration:'',
                    user:''
            },
         })
        }
       }
    }

      
       editForm(value, index){
        console.log(value);
        this.setState({
          newClient: value,
          arrIndex: index,
          editOption: true,
        })
  
      }
  
  
      delForm = (index)=> {
          console.log("hello")
        let new_data = Object.assign([], this.state.clients);
        new_data.splice(index, 1);
        console.log(new_data)
        this.setState({
          clients: new_data
        })
      }

      dataDropdown=(e)=>{
          this.setState({
              dropdown:e.target.value
          })
      }

      addClientBtn=()=>{
// console.log(this.state.state)

        if(this.state.address===''){
            document.getElementById('addressText').innerHTML="Enter Your Complete Address"
            document.getElementById("addressDiv").style.borderColor = "#f46a6a"               
        }else{
            document.getElementById('addressText').innerHTML=""
            document.getElementById("addressDiv").style.borderColor = "" 
        }
       
        if(this.state.address==''|| this.state.Email=='' ||this.state.Mobile=='' ){

                this.inputRef.current.focus()
        }else{
            console.log("good")
        }
      }

    adressChange=(e)=>{
        this.setState({
            address:e.target.value
        })
    }

    landmarkChange=(e)=>{
        this.setState({
            landmark:e.target.value
        })
    }

    mobChange=(e)=>{
        this.setState({
            Mobile:e.target.value
        })
    }
    nameChange=(e)=>{
        this.setState({
            name:e.target.value
        })
    }
    usernameChange=(e)=>{
        this.setState({
            username:e.target.value
        })
    }
    emailChange=(e)=>{
        this.setState({
            Email:e.target.value
        })
    }
    countryChange=(e)=>{
        this.setState({
            country :e.target.value
        })
    }
    stateChange=(e)=>{
        this.setState({
            state :e.target.value
        })
    }
    cityChange=(e)=>{
        this.setState({
            city :e.target.value
        })
    }
    pinChange=(e)=>{
        this.setState({
            pin :e.target.value
        })
    }

    editable=(e)=>{
        this.setState({
            visiblity:false
        })
    }

    areaChange=(e)=>{
        this.setState({
            area:e.target.value
        })
    }

    render() {
        console.log('user data', this.state.clients);
        const userData = this.state.clients;
        return (
            <React.Fragment>
                <div className="page-content">
                    <Container fluid>

                        {/* Render Breadcrumb */}
                        <Breadcrumbs title={this.props.t('Manage Client')} breadcrumbItem={this.props.t('Edit Client Detail')} />
                        <AvForm className="needs-validation" >
                        <Card>
                             <h4 className={"pt-2 pl-4 mb-0 mt-2"}>Personal Information</h4>
                             <p className={"ml-4"}>Edit The Personal Information Of Client</p>
                             <Button onClick={this.editable} style={{width:'15%', marginLeft:'80%', marginTop:'-5%'}}>Edit</Button>
                                    <CardBody>
                                        
                                            <Row>
                                                <Col md="12">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">Name<span style={{color:'red'}}>*</span></Label>
                                                        <AvField
                                                          name="Name"
                                                          placeholder="Enter Your Full Name"
                                                          type="text"
                                                          errorMessage="Enter Name"
                                                          className="form-control"
                                                          validate={{ required: { value: true } }}
                                                          id="validationCustom01"
                                                          value={this.state.name}
                                                          onChange={this.nameChange}
                                                          disabled={this.state.visiblity}
                                                        />
                                                    </FormGroup>
                                                </Col>
                                                

                                            </Row>

                                            <Row>
                                                <Col md="6">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">User Name<span style={{color:'red'}}>*</span></Label>
                                                        <AvField
                                                        name="UserName"
                                                        placeholder="Eg: JohnRockstar121"
                                                        type="text"
                                                        errorMessage="Enter User Name"
                                                        className="form-control"
                                                        validate={{ required: { value: true } }}
                                                        id="validationCustom01"
                                                        value={this.state.username}
                                                        onChange={this.usernameChange}
                                                        disabled={this.state.visiblity}
                                                        />
                                                    </FormGroup>
                                                </Col>
                                                <Col md="6">
                                                <FormGroup>
                                                    <Label htmlFor="validationCustom02">Mobile Number<span style={{color:'red'}}>*</span></Label>
                                                    <AvField
                                                      name="lastname"
                                                      placeholder="Enter 10 Digit Mobile Number"
                                                      type="number"
                                                      errorMessage="Enter Mobile Number"
                                                      className="form-control"
                                                      validate={{
                                                        required: { value: true },
                                                        minLength: { value: 10, errorMessage: "Enter 10 Digit Mobile Number." },
                                                        maxLength: { value: 10, errorMessage: "Enter 10 Digit Mobile Number." }
                                                      }}
                                                      id="validationCustom02"
                                                      value={this.state.Mobile}
                                                      onChange={this.mobChange}
                                                      disabled={this.state.visiblity}
                                                    />
                                                </FormGroup>
                                            </Col>                                          
                                            </Row>
                                            <Row>
                                                <Col md="12">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom04">Address<span style={{color:'red'}}>*</span></Label>
                                                        <textarea
                                                        name="Address"
                                                        placeholder="Enter Complete Address"
                                                        type="text"
                                                        errorMessage="Please provide a valid Address."
                                                        className="form-control"
                                                        validate={{ required: { value: true } }}
                                                        id="addressDiv"
                                                        style={{resize:'none'}}
                                                        value={this.state.address}
                                                        onChange={this.adressChange}
                                                        ref={this.inputRef}
                                                        disabled={this.state.visiblity}
                                                        />
                                                        <p id="addressText" style={{color:'#f46a6a', fontSize:"80%"}}></p>
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md="12">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom05">Landmark</Label>
                                                        <input
                                                        name="Landmark"
                                                        placeholder="Enter any nearby area."
                                                        type="text"
                                                        errorMessage=" Please provide a valid Landmark."
                                                        className="form-control"
                                                        validate={{ required: { value: true } }}
                                                        id="validationCustom05"
                                                        value={this.state.landmark}
                                                        onChange={this.landmarkChange}
                                                        disabled={this.state.visiblity}
                                                        />
                                                    </FormGroup>
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col md="12">
                                                  <FormGroup>
                                                      <Label htmlFor="validationCustom05">Area</Label>
                                                      <input
                                                      name="Area"
                                                      placeholder="Enter any nearby area."
                                                      type="text"
                                                      errorMessage=" Please provide a valid Area."
                                                      className="form-control"
                                                      validate={{ required: { value: true } }}
                                                      id="validationCustom05"
                                                      value={this.state.area}
                                                      onChange={this.areaChange}
                                                      />
                                                  </FormGroup>
                                               </Col>
                                          </Row>
                                       
                                    </CardBody>
                                </Card>
                               
                                <Card>
                                <h4 className={"pt-2 pl-4 mb-0 mt-2"}>Chit Scheme Details</h4>
                                <p className={"ml-4"}>Click Add button to add single or multiple chit details</p>
                                    <CardBody>
                                        <AvForm className="needs-validation" >  
                                            <Row>
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom04">Account Number</Label>
                                                        <AvField
                                                          name="Account Number"
                                                          placeholder="Unique Account Number"
                                                          type="text"
                                                          errorMessage="Please provide a valid Account Number."
                                                          className="form-control"
                                                          validate={{ required: { value: true } }}
                                                          id="accountNumber"
                                                          value={this.state.newClient.accountNumber}
                                                          onChange = { this.handleInput }
                                                          disabled={this.state.visiblity}
                                                        />
                                                    </FormGroup>
                                                </Col>
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom05">Chit Scheme</Label>
                                                        <select id="chitScheme" class="form-control" value={this.state.newClient.chitScheme} 
                                                        disabled={this.state.visiblity} onChange = { this.handleInput }>
                                                          <option>Select Chit Scheme</option>
                                                          <option>Thanga Mazhai</option>
                                                          <option>Thanga Then</option>
                                                          <option>Thanga Malar</option>
                                                          <option>My Gold Eleven</option>
                                                        </select>          
                                                        <p id="schemeVal" style={{color:'#f46a6a', fontSize:"80%"}}></p>                                          
                                                    </FormGroup>
                                                </Col>
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom03">Chit Amount</Label>
                                                        <select class="form-control" id="chitAmount" value={this.state.newClient.chitAmount} 
                                                        disabled={this.state.visiblity} onChange = { this.handleInput }>
                                                              <option>Select Chit Amount</option>
                                                              <option>500</option>
                                                              <option>1000</option>
                                                              <option>2500</option>
                                                              <option>5000</option>
                                                              <option>10000</option>
                                                        </select>
                                                        <p id="amountVal" style={{color:'#f46a6a', fontSize:"80%"}}></p>
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <Row>
                                            <Col md="4">
                                                <FormGroup>
                                                    <Label htmlFor="validationCustom04">Duration</Label>
                                                    <select class="form-control" id="duration" value={this.state.newClient.duration} 
                                                    disabled={this.state.visiblity} onChange = { this.handleInput }>
                                                        <option>Select Chit Duration</option>
                                                        <option>15</option>
                                                        <option>11</option>
                                                     </select>
                                                     <p id="durationVal" style={{color:'#f46a6a', fontSize:"80%"}}></p>
                                                </FormGroup>
                                            </Col>
                                            <Col md="4">
                                                <FormGroup>
                                                    <Label htmlFor="validationCustom05">User</Label>
                                                    <select class="form-control" id="user" value={this.state.newClient.user} 
                                                    disabled={this.state.visiblity} onChange = { this.handleInput }>
                                                        <option>Select User</option>
                                                        <option>New</option>
                                                        <option>Existing</option>
                                                    </select>
                                                    <p id="userVal" style={{color:'#f46a6a', fontSize:"80%"}}></p>
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <button className={"ml-2 btn"} style={{backgroundColor:'#880105', color:'#e1b761'}} type="submit" onClick={this.submitDataHandler} >Add Chit Scheme</button>
                                        </Row>
                                        </AvForm>
                                    </CardBody>
                                </Card>

                                <Card className={this.state.clients.length==0?'d-none':""}>
                                    <CardBody>
                                    <Table className="table table-centered table-nowrap">
                                    <thead className="thead-light">
                                        <tr>
                                            <th>S no </th>
                                            <th>Account No.</th>
                                            <th>Chit Scheme</th>
                                            <th>Chit Amount</th>
                                            <th>Duration</th>
                                            <th>User</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                        
                                            userData && userData.map((value, index) => {
                                                return (

                                                <tr>
                                                    <td>{1+index}</td>
                                                    <td>{value.accountNumber}</td>
                                                    <td>{value.chitScheme}</td>
                                                    <td>{value.chitAmount}</td>
                                                    <td>{value.duration}</td>
                                                    <td>{value.user}</td>
                                                    
                                                    <td>
                                                        <Link onClick={this.editForm.bind(this, value ,index)} className="mr-3 text-primary">
                                                            <i className="mdi mdi-pencil font-size-18 mr-3" id="edittooltip"></i>
                                                            <UncontrolledTooltip placement="top" target="edittooltip">
                                                                Edit
                                                            </UncontrolledTooltip >
                                                        </Link>
                                                        <Link onClick={this.delForm.bind(this, index)} className="text-danger">
                                                            <i className="mdi mdi-close font-size-18 mr-3" id="deletetooltip"></i>
                                                            <UncontrolledTooltip placement="top" target="deletetooltip">
                                                                Delete
                                                            </UncontrolledTooltip >
                                                        </Link>
                                                    </td>
                                                </tr>

                                                )
                                            })
                                          }
                                            
                                        

                                    </tbody>
                                </Table>
                                    </CardBody>
                                </Card>

                                <Card>
                                   <CardBody className={"py-1"}>
                                        <AvForm className="needs-validation" >  
                                            <Row>
                                                <Col md="6">
                                                    <FormGroup className={"pt-1"}>
                                                        <div className="custom-control custom-checkbox pt-2">
                                                        <AvField
                                                          name="condition"
                                                          type="checkbox"
                                                          errorMessage="You must agree before submitting."
                                                          className="custom-control-input"
                                                        />
                                                            <Label className="custom-control-label" htmlFor="invalidCheck">Agree to terms and conditions</Label>
                                                            
                                                        </div>
                                                    </FormGroup>
                                                </Col>
                                                <Col md='6' style={{textAlign:"end", marginTop:'.2rem'}}>
                                                    <button type="button" className='btn' style={{backgroundColor:'#880105', color:'#e1b761'}}>Update Client</button>
                                                    <button className={"btn ml-2"} style={{backgroundColor:'#880105', color:'#e1b761'}} type="button">Cancel</button>
                                                </Col>
                                            </Row>
                                        </AvForm>
                                    </CardBody>
                                </Card>
                                </AvForm>
                    </Container>
                        
                </div>
          
                </React.Fragment>
        );
    }
}

export default withNamespaces()(EditClientDaily);

 
 






