import React, { Component } from "react";
import { Row, Col, Card, CardBody, FormGroup, Button, CardTitle, CardSubtitle, Label, Input, textarea,Container, UncontrolledTooltip, Table} from "reactstrap";
import { Link } from "react-router-dom";
import { AvForm, AvField } from "availity-reactstrap-validation";
import './CssForAll.css';
import PostApiCall from '../API/PostAPI';
import Notiflix from "notiflix";
import moment from 'moment';


//Import Breadcrumb
import Breadcrumbs from '../components/Common/Breadcrumb';

//i18n
import { withNamespaces } from 'react-i18next';

class AddClientDaily extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newClient:{
                accountNumber:'',
                chitScheme:'',
                chitAmount:'',
                duration:'',
               
            },
            clients:[],
            showData:true,
            arrIndex:'',
            editOption:false,
            dropdown:"",
            address:'',
            country:'',
            state:'',
            city:'',
            visiblity:true,
            //personal details
            name:'',
            username:'',
            Email:'',
            Mobile:'',
            landmark:'',
            pin:'',
            area:'',
            PermanentSame : false,
            accValid:'form-control',
            NameValid:'form-control',
            EmailValid:'form-control',
            MobileValid:'form-control',
            chitSchemeValid:'form-control',
            chitAmountValid:'form-control',
            durationValid:'form-control',
            userValid:'form-control',
            paidMonthsValid:'form-control',
           ChitSchemaData : [
                {label : 'Thanga Mazhai',value:'Thanga Mazhai'},
                {label : 'Thanga Then',value:'Thanga Then'},
                {label : 'Thanga Malar',value:'Thanga Malar'},
                {label : 'My Gold Eleven',value:'My Gold Eleven'},
               ],
            ChitAmountData:[
                {label : '500',value:'500'},
                {label : '1000',value:'1000'},
                {label : '2500',value:'2500'},
                {label : '5000',value:'5000'},
                {label : '10000',value:'10000'},
                
            ],
            DurationData:[
                {label : '15',value:'15'},
                {label : '11',value:'11'},
            ],
          
        };
        this.inputRef=React.createRef()
    }

    handleUserInput = (e) => {
        // console.log(e.target.name)
        // console.log(e.target.value)
        if(e.target.name==="exiting_user" && e.target.value==="Existing") {
            this.setState({
                visiblity:false
            })
        }else{
            this.setState({
                visiblity:false
            })
        }

        let input_data = Object.assign({}, this.state.newClient);
       
        const fields = e.target.id;
        input_data[fields] = e.target.value;
  
        this.setState({
            newClient : input_data,
            userValid:"form-control is-valid"
       })  
       
       if(e.target.value===''){
         this.setState({
             userValid:"form-control is-invalid",
               
           })  
     }      
    }



    handleChitScheme=(e)=>{
        let input_data = Object.assign({}, this.state.newClient);
       
        const fields = e.target.id;
        input_data[fields] = e.target.value;
        console.log(e.target.value);
         this.setState({
            chitSchemeValid:"form-control is-valid",
               newClient : input_data
          })  
          console.log(this.state.newClient.chitScheme);

          if(e.target.value===''){
            this.setState({
                chitSchemeValid:"form-control is-invalid",
                  
              })  
        }
    }
    
     handleChitAmount=(e)=>{
        let input_data = Object.assign({}, this.state.newClient);
       
        const fields = e.target.id;
        input_data[fields] = e.target.value;
         this.setState({
            chitAmountValid:"form-control is-valid",
               newClient : input_data
          })  
          console.log(e.target.value);

        if(e.target.value===''){
            this.setState({
                chitAmountValid:"form-control is-invalid",
                  
              })  
        }
    }
  
    handleDuration=(e)=>{
        let input_data = Object.assign({}, this.state.newClient);
       
        const fields = e.target.id;
        input_data[fields] = e.target.value;
        console.log(e.target.value);
         this.setState({
            durationValid:"form-control is-valid",
               newClient : input_data
          })  

          

          if(e.target.value===''){
            this.setState({
                durationValid:"form-control is-invalid",
                  
              })  
        }
    }
    






    submitDataHandler = () => {   
        if(this.state.newClient.chitScheme==''&&this.state.newClient.chitAmount==''&&this.state.newClient.duration==''&&(this.state.newClient.user===''&&this.state.newClient.paidMonths=='')){
            this.setState({
                paidMonthsValid  :"form-control is-invalid",
                userValid  :"form-control is-invalid",
                chitAmountValid  :"form-control is-invalid",
                durationValid  :"form-control is-invalid",
                chitSchemeValid  :"form-control is-invalid",
                accValid  :"form-control is-invalid",
               
                  
              })
           }
      else  if(this.state.newClient.chitScheme==''){
            this.setState({
                chitSchemeValid:"form-control is-invalid",
                  
              })   
        }
      else  if(this.state.newClient.chitAmount==''){
        this.setState({
            chitAmountValid:"form-control is-invalid",
              
          })   }
     else   if(this.state.newClient.duration==''){
        this.setState({
            durationValid  :"form-control is-invalid",
              
          }) }
     else   if(this.state.newClient.user==''){
        this.setState({
            userValid  :"form-control is-invalid",
              
          })          
            if(this.state.newClient.paidMonths==''){
                this.setState({
                    paidMonthsValid  :"form-control is-invalid",
                      
                  }) }
        }
      else   if(this.state.newClient.chitScheme==''||this.state.newClient.chitAmount==''||this.state.newClient.duration==''||(this.state.newClient.user===''&&this.state.newClient.paidMonths=='')){
             return   
            }

        else{  
          


            if(this.state.editOption === true){
                console.log('asdf');
                this.setState({
                    paidMonthsValid  :"form-control",
                    userValid  :"form-control",
                    chitAmountValid  :"form-control",
                    durationValid  :"form-control",
                    chitSchemeValid  :"form-control",
                    accValid  :"form-control",
                   
                      
                  })
              let {clients, arrIndex, newClient} = this.state;
              let new_data = Object.assign([], clients);
              new_data.splice(arrIndex, 1);
              new_data.push(newClient);
         
              this.setState({
                clients: new_data,
                newClient: {
                    accountNumber:'',
                    chitScheme:'',
                    chitAmount:'',
                    duration:'',
                    user:'',
                    paidMonths:''
            },
              }, () => {
                console.log('state ', this.state.clients);
              })
             
            } else{
              this.state.clients.push(this.state.newClient)
              this.setState({
                paidMonthsValid  :"form-control",
                userValid  :"form-control",
                chitAmountValid  :"form-control",
                durationValid  :"form-control",
                chitSchemeValid  :"form-control",
                accValid  :"form-control",
               
                  
              })
              this.setState({
                showData: true,
                newClient: {
                    accountNumber:'',
                    chitScheme:'',
                    chitAmount:'',
                    duration:'',
                    user:'',
                    paidMonths:''
            },
         })
        }
        for(var i=0;i<this.state.clients.length;i++){
            console.log(this.state.clients[i].accountNumber)
        }
       }
    }

    onPost = () => {

        // Notiflix.Loading.Dots('Please wait...');
      
        var det=localStorage.getItem('loginedUser')
        var detail=JSON.parse(det)
        PostApiCall.postRequest ({
             name : this.state.name,
             username : this.state.username,           
             mobile : this.state.Mobile,
             address :this.state.address,
             landmark : this.state.landmark,
             area : this.state.area,
             updatedon : moment().format('lll'),
             updatedby : detail[0].fld_userid,
             agree : this.state.PermanentSame,
        },"Add_New_Dailyclient").then((result) =>
      
        result.json().then(obj => {
            if(result.status == 200 || result.status == 201){
               
                console.log(obj)
               this.onSubmitChitschem(obj);
            }else
            {
            //   Notiflix.Loading.Remove();
              Notiflix.Notify.Failure(obj.data)
            }
        })
        )
      
      }


      onSubmitChitschem(obj){

        var det=localStorage.getItem('loginedUser')
        var detail=JSON.parse(det)

        // Notiflix.Loading.Dots('Please wait...');      
        for(var i=0;i<this.state.clients.length;i++){
            console.log(this.state.clients[i].accountNumber,this.state.clients[i].chitScheme)
        PostApiCall.postRequest ({
             client: (JSON.parse(JSON.stringify(obj.data[0]))).ClientId,
             accountno :this.state.clients[i].accountNumber,
             chitschema : this.state.clients[i].chitScheme,
             chitamount :this.state.clients[i].chitAmount,
             duration : this.state.clients[i].duration,
             updatedon : moment().format('lll'),
            updatedby : detail[0].fld_userid,
             
        },"Add_New_Dailyclient_chitschema").then((result) =>
      
        result.json().then(obj => {
            if(result.status == 200 || result.status == 201){
               
                console.log(obj)
                // Notiflix.Loading.Remove();
                Notiflix.Notify.Success('Chit successfully added.')
                window.location.reload()              
            }else
            {
            //   Notiflix.Loading.Remove();
              Notiflix.Notify.Failure('Something went wrong, try again later.')
            }
        })
        )
    }
      
      }
      

      
       editForm(value, index){
        // console.log(value);
        this.setState({
          newClient: value,
          arrIndex: index,
          editOption: true,
        })
  
      }
  
  
      delForm = (index)=> {
        //   console.log("hello")
        let new_data = Object.assign([], this.state.clients);
        new_data.splice(index, 1);
        // console.log(new_data)
        this.setState({
          clients: new_data
        })
      }

      dataDropdown=(e)=>{
          this.setState({
              dropdown:e.target.value
          })
      }

      addClientBtn=()=>{

        if(this.state.address===''){
            document.getElementById('addressText').innerHTML="Enter Your Complete Address"
            document.getElementById("addressDiv").style.borderColor = "#f46a6a"               
        }else{
            document.getElementById('addressText').innerHTML=""
            document.getElementById("addressDiv").style.borderColor = "" 
        }
        // if(this.state.country==''){            
        //     document.getElementById('countryVal').innerHTML="Select Your country"
        //     document.getElementById("countryDiv").style.borderColor = "#f46a6a"
        // }else{
        //     document.getElementById('countryVal').innerHTML=""
        //     document.getElementById("countryDiv").style.borderColor = ""
        // }
        // if(this.state.state==''){            
        //     document.getElementById('stateVal').innerHTML="Enter Your Current State"
        //     document.getElementById("stateDiv").style.borderColor = "#f46a6a"
        // }else{
        //     document.getElementById('stateVal').innerHTML=""
        //     document.getElementById("stateDiv").style.borderColor = ""
        // }
        // if(this.state.city==''){            
        //     document.getElementById('cityVal').innerHTML="Enter Your Current city"
        //     document.getElementById("cityDiv").style.borderColor = "#f46a6a"
        // }else{
        //     document.getElementById('cityVal').innerHTML=""
        //     document.getElementById("cityDiv").style.borderColor = ""
        // }
        if(this.state.address==''|| this.state.name=='' || this.state.username=='' ||this.state.Mobile=='' ){

                this.inputRef.current.focus()
               
        }else{

            this.onPost();
        }
      }

    adressChange=(e)=>{
        this.setState({
            address:e.target.value
        })
    }

    landmarkChange=(e)=>{
        this.setState({
            landmark:e.target.value
        })
    }

    areaChange=(e)=>{
        this.setState({
            area:e.target.value
        })
    }

    mobChange=(e)=>{
        this.setState({
            Mobile:e.target.value
        })
    }
    nameChange=(e)=>{
        this.setState({
            name:e.target.value
        })
    }
    usernameChange=(e)=>{
        this.setState({
            username:e.target.value
        })
    }
    emailChange=(e)=>{
        this.setState({
            Email:e.target.value
        })
    }
    countryChange=(e)=>{
        this.setState({
            country :e.target.value
        })
    }
    stateChange=(e)=>{
        this.setState({
            state :e.target.value
        })
    }
    cityChange=(e)=>{
        this.setState({
            city :e.target.value
        })
    }
    pinChange=(e)=>{
        this.setState({
            pin :e.target.value
        })
    }


    handleInputAccount=(e)=>{
        const regex = /^[a-zA-Z]{2}\d*$/
       console.log(e.target.value)
 
const pattern=e.target.value
console.log(regex.test(pattern))
console.log(e.target.id);

 if(!regex.test(pattern)){
    this.setState({accValid:"form-control is-invalid"})
    // console.log(` is not a valid character.`); 
  }
  else{
    let input_data = Object.assign({}, this.state.newClient);
       
    const fields = e.target.id;
    input_data[fields] = e.target.value;
    
      this.setState({accValid:"form-control is-valid",
           newClient : input_data
      })  
  } 
    }

    render() {
        // console.log('user data', this.state.clients);
        const userData = this.state.clients;
        return (
            <React.Fragment>
                <div className="page-content">
                    <Container fluid>

                        {/* Render Breadcrumb */}
                        <Breadcrumbs title={this.props.t('Manage Clients')} breadcrumbItem={this.props.t('Add New Client')} />
                        <AvForm className="needs-validation" >
                        <Card>
                             <h4 className={"pt-2 pl-4 mb-0 mt-2"}>Personal Information</h4>
                             <p className={"ml-4"}>Enter Personal Information Of Client</p>
                                    <CardBody>
                                        
                                            <Row>
                                                <Col md="12">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">Name<span style={{color:'red'}}>*</span></Label>
                                                        <AvField
                                                          name="Name"
                                                          placeholder="Enter Your Full Name"
                                                          type="text"
                                                          errorMessage="Enter Name"
                                                          className="form-control"
                                                          validate={{ required: { value: true } }}
                                                          id="validationCustom01"
                                                          value={this.state.name}
                                                          onChange={this.nameChange}
                                                        />
                                                    </FormGroup>
                                                </Col>
                                              

                                            </Row>

                                            <Row>
                                            <Col md="6">
                                            <FormGroup>
                                                <Label htmlFor="validationCustom01">User Name<span style={{color:'red'}}>*</span></Label>
                                                <AvField
                                                  name="UserName"
                                                  placeholder="Eg: JohnRockstar121"
                                                  type="text"
                                                  errorMessage="Enter User Name"
                                                  className="form-control"
                                                  validate={{ required: { value: true } }}
                                                  id="validationCustom01"
                                                  value={this.state.username}
                                                  onChange={this.usernameChange}
                                                />
                                            </FormGroup>
                                        </Col>
                                                  
                                                <Col md="6">
                                                <FormGroup>
                                                    <Label htmlFor="validationCustom02">Mobile Number<span style={{color:'red'}}>*</span></Label>
                                                    <AvField
                                                      name="lastname"
                                                      placeholder="Enter 10 Digit Mobile Number"
                                                      type="number"
                                                      errorMessage="Enter Mobile Number"
                                                      className="form-control"
                                                      validate={{
                                                        required: { value: true },
                                                        minLength: { value: 10, errorMessage: "Enter 10 Digit Mobile Number." },
                                                        maxLength: { value: 10, errorMessage: "Enter 10 Digit Mobile Number." }
                                                      }}
                                                      id="validationCustom02"
                                                      value={this.state.Mobile}
                                                      onChange={this.mobChange}
                                                    />
                                                </FormGroup>
                                            </Col>                                          
                                            </Row>
                                            <Row>
                                                <Col md="12">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom04">Address<span style={{color:'red'}}>*</span></Label>
                                                        <textarea
                                                        name="Address"
                                                        placeholder="Enter Complete Address"
                                                        type="text"
                                                        errorMessage="Please provide a valid Address."
                                                        className="form-control"
                                                        validate={{ required: { value: true } }}
                                                        id="addressDiv"
                                                        style={{resize:'none'}}
                                                        value={this.state.address}
                                                        onChange={this.adressChange}
                                                        ref={this.inputRef}
                                                        />
                                                        <p id="addressText" style={{color:'#f46a6a', fontSize:"80%"}}></p>
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md="12">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom05">Landmark</Label>
                                                        <input
                                                        name="Landmark"
                                                        placeholder="Enter any nearby area."
                                                        type="text"
                                                        errorMessage=" Please provide a valid Landmark."
                                                        className="form-control"
                                                        validate={{ required: { value: true } }}
                                                        id="validationCustom05"
                                                        value={this.state.landmark}
                                                        onChange={this.landmarkChange}
                                                        />
                                                    </FormGroup>
                                                </Col>
                                            </Row>

                                            <Row>
                                              <Col md="12">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom05">Area</Label>
                                                        <input
                                                        name="Area"
                                                        placeholder="Enter any nearby area."
                                                        type="text"
                                                        errorMessage=" Please provide a valid Area."
                                                        className="form-control"
                                                        validate={{ required: { value: true } }}
                                                        id="validationCustom05"
                                                        value={this.state.area}
                                                        onChange={this.areaChange}
                                                        />
                                                    </FormGroup>
                                                 </Col>
                                            </Row>

                                       
                                    </CardBody>
                                </Card>
                               
                                <Card>
                                <h4 className={"pt-2 pl-4 mb-0 mt-2"}>Chit Scheme Details</h4>
                                <p className={"ml-4"}>Click Add button to add single or multiple chit details</p>
                                    <CardBody>
                                        <AvForm className="needs-validation" >  
                                            <Row>
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom04">Account Number</Label>
                                                        <AvField
                                                          name="Account Number"
                                                          placeholder="Unique Account Number"
                                                          type="text"
                                                          errorMessage="Please provide a valid Account Number."
                                                          className={`${this.state.accValid}`}
                                                          validate={{ required: { value: this.state.accValid==='form-control is-invalid'?true:false } }}

                                                          id="accountNumber"
                                                          value={this.state.newClient.accountNumber}
                                                          onChange = { this.handleInputAccount }

                                                        />
                                                    </FormGroup>
                                                </Col>
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom05">Chit Scheme</Label>
                                                        <select id="chitScheme" className={`${this.state.chitSchemeValid}`}
                                                         value={this.state.newClient.chitScheme} 
                                                         onChange = { this.handleChitScheme }>
                                                          <option value=''>Select Chit Scheme</option>
                                                          {this.state.ChitSchemaData.map(
                                                            schedule => (
                                                                <option
                                                                key={schedule.label}
                                                                value={schedule.value}
                                                                >
                                                                {schedule.label}
                                                                </option>
                                                            )
                                                            )}
                                                        </select>          
                                                        {this.state.chitSchemeValid=='form-control is-invalid'&&(<div className='invalid-feedback'>Select Your Chit Scheme </div>)}
                                        
                                                    </FormGroup>
                                                </Col>
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom03">Chit Amount</Label>
                                                        <select className={`${this.state.chitAmountValid}`} id="chitAmount" 
                                                        value={this.state.newClient.chitAmount}  onChange = { this.handleChitAmount }>
                                                              <option value=''>Select Chit Amount</option>
                                                              {this.state.ChitAmountData.map(
                                                                schedule => (
                                                                    <option
                                                                    key={schedule.label}
                                                                    value={schedule.value}
                                                                    >
                                                                    {schedule.label}
                                                                    </option>
                                                                )
                                                                )}
                                                        </select>
                                                        {this.state.chitAmountValid=='form-control is-invalid'&&(<div className='invalid-feedback'>Select Your Chit Amount </div>)}

                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <Row>
                                            <Col md="4">
                                                <FormGroup>
                                                    <Label htmlFor="validationCustom04">Duration</Label>
                                                    <select  className={`${this.state.durationValid}`} id="duration" 
                                                    value={this.state.newClient.duration}  onChange = { this.handleDuration }>
                                                        <option value=''>Select Chit Duration</option>
                                                        {this.state.DurationData.map(
                                                            schedule => (
                                                                <option
                                                                key={schedule.label}
                                                                value={schedule.value}
                                                                >
                                                                {schedule.label}
                                                                </option>
                                                            ))}
                                                     </select>
                                                     {this.state.durationValid=='form-control is-invalid'&&(<div className='invalid-feedback'>Select your Duration</div>)}

                                                </FormGroup>
                                            </Col>
                                          
       
                                        </Row>
                                        <Row>
                                            <button className={"btn ml-2"} type="submit" 
                                            style={{backgroundColor:'#880105', color:'#e1b761'}} onClick={this.submitDataHandler} >Add Chit Scheme</button>
                                        </Row>
                                        </AvForm>
                                    </CardBody>
                                </Card>

                                <Card className={this.state.clients.length==0?'d-none':""}>
                                    <CardBody>
                                    <Table className="table table-centered table-nowrap">
                                    <thead className="thead-light">
                                        <tr>
                                            <th>S no </th>
                                            <th>Account No.</th>
                                            <th>Chit Scheme</th>
                                            <th>Chit Amount</th>
                                            <th>Duration</th>
                                          
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                        
                                            userData && userData.map((value, index) => {
                                                return (

                                                <tr>
                                                    <td>{1+index}</td>
                                                    <td>{value.accountNumber}</td>
                                                    <td>{value.chitScheme}</td>
                                                    <td>{value.chitAmount}</td>
                                                    <td>{value.duration}</td>
                                                   
                                                    
                                                    <td>
                                                        <Link onClick={this.editForm.bind(this, value ,index)} className="mr-3 text-primary">
                                                            <i className="mdi mdi-pencil font-size-18 mr-3" id="edittooltip"></i>
                                                            <UncontrolledTooltip placement="top" target="edittooltip">
                                                                Edit
                                                            </UncontrolledTooltip >
                                                        </Link>
                                                        <Link onClick={this.delForm.bind(this, index)} className="text-danger">
                                                            <i className="mdi mdi-close font-size-18 mr-3" id="deletetooltip"></i>
                                                            <UncontrolledTooltip placement="top" target="deletetooltip">
                                                                Delete
                                                            </UncontrolledTooltip >
                                                        </Link>
                                                    </td>
                                                </tr>

                                                )
                                            })
                                          }
                                            
                                        

                                    </tbody>
                                </Table>
                                    </CardBody>
                                </Card>

                                <Card>
                                   <CardBody className={"py-1"}>
                                        <AvForm className="needs-validation" >  
                                            <Row>
                                                <Col md="6">
                                                    <FormGroup className={"pt-1"}>
                                                        <div className="custom-control custom-checkbox pt-2">
                                                        <input type="checkbox"  class="form-check-input"
                                                        checked={this.state.PermanentSame}
                                                        onChange={()=>{
                                                            this.setState({
                                                                PermanentSame:!this.state.PermanentSame
                                                            })
                                                        }}
                                                       />
                                                        <label class="form-check-label" for="exampleCheck1" style={{verticalAlign:'middle'}}>Agree to terms and conditions</label>
                                                            
                                                        </div>
                                                    </FormGroup>
                                                </Col>
                                                <Col md='6' style={{textAlign:"end", marginTop:'.2rem'}}>
                                                    <button disabled={this.state.PermanentSame===true?false:true} className={"btn"} type="submit" style={{backgroundColor:'#880105', color:'#e1b761'}} onClick={this.addClientBtn}>Add Client</button>
                                                    <button className={"ml-2 btn"} style={{backgroundColor:'#880105', color:'#e1b761'}} type="button">Cancel</button>
                                                </Col>
                                            </Row>
                                        </AvForm>
                                    </CardBody>
                                </Card>
                                </AvForm>
                    </Container>
                        
                </div>
          
                </React.Fragment>
        );
    }
}

export default withNamespaces()(AddClientDaily);

 
 

