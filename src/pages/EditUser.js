import React, { Component } from "react";
import { Row, Col, Card, CardBody, FormGroup, Button, CardTitle, CardSubtitle, Label, Input ,Container, UncontrolledTooltip, Table} from "reactstrap";
import { Link } from "react-router-dom";
import { AvForm, AvField } from "availity-reactstrap-validation";



//Import Breadcrumb
import Breadcrumbs from '../components/Common/Breadcrumb';

//i18n
import { withNamespaces } from 'react-i18next';

class EditUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visiblity:true
        };
    }

    editable=(e)=>{
        this.setState({
            visiblity:false
        })
    }


    render() {
        return (
            <React.Fragment>
                <div className="page-content">
                    <Container fluid>

                        {/* Render Breadcrumb */}
                        <Breadcrumbs title={this.props.t('User Managment')} breadcrumbItem={this.props.t('Update Users')} />
                    
                        <Card>
 
                        
                                    <CardBody>
                                    <CardTitle>Edit Users</CardTitle>
                                    <CardSubtitle className="mb-3">
                                        Update the user details.
                                    </CardSubtitle>
                                    <Button onClick={this.editable} style={{width:'15%', marginLeft:'80%', marginTop:'-5%'}}>Edit</Button>
                                        <AvForm className="needs-validation" >
                                            <Row>
                                                <Col md="12">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">Account Number</Label>
                                                        <AvField
                                                          name="Account"
                                                          placeholder="Enter Account Number"
                                                          type="text"
                                                          errorMessage="Enter Valid Account Number"
                                                          className="form-control"
                                                          validate={{ required: { value: true } }}
                                                          id="validationCustom01"
                                                          disabled={this.state.visiblity}
                                                        />
                                                    </FormGroup>
                                                </Col>                                                
                                            </Row>

                                            <Row>
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom03">Name</Label>
                                                        <AvField
                                                        name="Name"
                                                        placeholder="Enter Your Name "
                                                        type="number"
                                                        errorMessage="Enter your Name"
                                                        className="form-control"
                                                        validate={{ required: { value: true } }}
                                                        id="validationCustom01"
                                                        disabled={this.state.visiblity}
                                                        />
                                                    </FormGroup>
                                                </Col>   
                                                <Col md="4">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">Email ID</Label>
                                                        <AvField
                                                        name="mail"
                                                        placeholder="Enter your mail"
                                                        type="email"
                                                        errorMessage="Enter a valid email"
                                                        className="form-control"
                                                        validate={{ required: { value: true } }}
                                                        id="validationCustom01"
                                                        disabled={this.state.visiblity}
                                                        />
                                                    </FormGroup>
                                                </Col>    
                                                <Col md="4">
                                                    <FormGroup>
                                                    <Label htmlFor="validationCustom02">Mobile Number</Label>
                                                    <AvField
                                                      name="lastname"
                                                      placeholder="Enter 10 Digit Mobile Number"
                                                      type="number"
                                                      errorMessage="Enter Mobile Number"
                                                      className="form-control"
                                                      validate={{
                                                        required: { value: true },
                                                        minLength: { value: 10, errorMessage: "Enter 10 Digit Mobile Number." },
                                                        maxLength: { value: 10, errorMessage: "Enter 10 Digit Mobile Number." }
                                                      }}
                                                      id="validationCustom02"
                                                      disabled={this.state.visiblity}
                                                    />
                                                    </FormGroup>
                                                </Col>                                       
                                            </Row>

                                            <Row>
                                                <Col md="12">
                                                    <FormGroup>
                                                        <Label htmlFor="validationCustom01">Address</Label>
                                                        <AvField
                                                          name="Address"
                                                          placeholder="Enter Your Address"
                                                          type="text"
                                                          errorMessage="Enter Your Address"
                                                          className="form-control"
                                                          validate={{ required: { value: true } }}
                                                          id="validationCustom01"
                                                          disabled={this.state.visiblity}
                                                        />
                                                    </FormGroup>
                                                </Col>                                                
                                            </Row>
                                            
                                            <button style={{backgroundColor:'#880105', color:'#e1b761'}} className="btn" type="submit">Update</button>
                                           
                                        </AvForm>
                                    </CardBody>
                                </Card>
                    </Container>
                        
                </div>
          
                </React.Fragment>
        );
    }
}

export default withNamespaces()(EditUser);








 






