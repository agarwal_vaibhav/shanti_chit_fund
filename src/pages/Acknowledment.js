import React, { Component } from "react";
import { Container, Row, Col, Button, Card, CardSubtitle, CardBody, CardTitle, Modal, ModalHeader, ModalBody, ModalFooter, Media, Table } from "reactstrap";
import { Link } from "react-router-dom";
import { MDBDataTable } from "mdbreact";
//Import Breadcrumb
import Breadcrumbs from '../components/Common/Breadcrumb';
//i18n
import { withNamespaces } from 'react-i18next';

class Acknowledment extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }


    render() {

        const data = {
            columns: [
              {
                label: "Sno.",
                field: "Sno",
                sort: "asc",
                width: 100
              },
              {
                label: "Account Number",
                field: "accountNumber",
                sort: "asc",
                width: 270
              },
              {
                label: "Transaction ID",
                field: "transaction",
                sort: "asc",
                width: 200
              },
              {
                label: "Paid Amount",
                field: "paidAmount",
                sort: "asc",
                width: 100
              },
              // {
              //   label: "Current Gold Rate",
              //   field: "goldrate",
              //   sort: "asc",
              //   width: 100
              // },
              {
                label: "Calculated Weight(g)",
                field: "weight",
                sort: "asc",
                width: 100
              },
              {
                label: "Paid Date",
                field: "paid",
                sort: "asc",
                width: 150
              },
              {
                label: "Action",
                field: "action",
                sort: "asc",
                width: 100
              }               
            ],
            rows: [
              {
                Sno: "1",
                accountNumber: "20215493345",
                transaction: "6547",
                paidAmount: "61",
                goldrate:"Rs 5000",
                weight:"200",
                paid: "25/11/2018",
                action:<button style={{backgroundColor:'#880105', color:'#e1b761'}} className={"btn"}>Send Acknowledment</button>
            },
              {
                Sno: "2",
                accountNumber: "20215493345",
                transaction: "3674",
                paidAmount: "63",
                goldrate:"Rs 5000",
                weight:"200",
                paid: "25/11/2018",
                action:<button style={{backgroundColor:'#880105', color:'#e1b761'}} className={"btn"}>Send Acknowledment</button>
            },
              {
                Sno: "3",
                accountNumber: "20215493345",
                transaction: "7568",
                paidAmount: "66",
                goldrate:"Rs 5000",
                weight:"200",
                paid: "25/11/2018",
                action:<button style={{backgroundColor:'#880105', color:'#e1b761'}} className={"btn"}>Send Acknowledment</button>
            },
              {
                Sno: "4",
                accountNumber: "20215493345",
                transaction: "3625",
                paidAmount: "22",
                goldrate:"Rs 5000",
                weight:"200",
                paid: "25/11/2018",
                action:<button style={{backgroundColor:'#880105', color:'#e1b761'}} className={"btn"}>Send Acknowledment</button>
            },
              {
                Sno: "5",
                accountNumber: "20215493345",
                transaction: "9856",
                paidAmount: "33",
                goldrate:"Rs 5000",
                weight:"200",
                paid: "25/11/2018",
                action:<button style={{backgroundColor:'#880105', color:'#e1b761'}} className={"btn"}>Send Acknowledment</button>
            },
              {
                Sno: "6",
                accountNumber: "20215493345",
                transaction: "7415",
                paidAmount: "61",
                goldrate:"Rs 5000",
                weight:"200",
                paid: "25/11/2018",
                action:<button style={{backgroundColor:'#880105', color:'#e1b761'}} className={"btn"}>Send Acknowledment</button>
            },
              {
                Sno: "7",
                accountNumber: "20215493345",
                transaction: "7895",
                paidAmount: "66",
                goldrate:"Rs 5000",
                weight:"200",
                paid: "25/11/2018",
                action:<button style={{backgroundColor:'#880105', color:'#e1b761'}} className={"btn"}>Send Acknowledment</button>
            },
              {
                Sno: "8",
                accountNumber: "20215493345",
                transaction: "1255",
                paidAmount: "27",
                goldrate:"Rs 5000",
                weight:"200",
                paid: "25/11/2018",
                action:<button style={{backgroundColor:'#880105', color:'#e1b761'}} className={"btn"}>Send Acknowledment</button>
             }
            ]
          };

        return (
            <React.Fragment>
                <div className="page-content">
                    <Container fluid>
                      <div className="px-3">
                          <Breadcrumbs title={this.props.t('Transactions')} breadcrumbItem={this.props.t('Transactions')} />
                      </div>
                       
                            
                        <div className="page-content p-0 m-0">
                            <div className="container ">     
                                <Row>
                                    <Col className="col-12">
                                    <Card>
                                        <CardBody> 
                                        <CardTitle>Transactions</CardTitle>
                                        <CardSubtitle className="mb-3">
                                             List of all the transaction.
                                        </CardSubtitle>                        
                                        <MDBDataTable responsive striped bordered data={data} />
                    
                                        </CardBody>
                                    </Card>
                                    </Col>
                                </Row>
                            </div>
                        </div>



                        </Container>
                </div>
          
                </React.Fragment>
        );
    }
}

export default withNamespaces()(Acknowledment);



