import React, { Component } from "react";
import { Row, Col, Collapse } from "reactstrap";
import { Link, withRouter } from "react-router-dom";
import classname from "classnames";

//i18n
import { withNamespaces } from 'react-i18next';

class Navbar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            UserDetails : []
        };
    }

    componentDidMount() {
        var matchingMenuItem = null;
        var ul = document.getElementById("navigation");
        var items = ul.getElementsByTagName("a");
        for (var i = 0; i < items.length; ++i) {
            if (this.props.location.pathname === items[i].pathname) {
                matchingMenuItem = items[i];
                break;
            }
        }
        if (matchingMenuItem) {
            this.activateParentDropdown(matchingMenuItem);
        }

        var isAuthenticated=JSON.parse(localStorage.getItem('loginedUser'));

        this.setState({...this.state,
            UserDetails : isAuthenticated[0]
            })

    }

    activateParentDropdown = item => {
        item.classList.add("active");
        const parent = item.parentElement;
        if (parent) {
            parent.classList.add("active"); // li
            const parent2 = parent.parentElement;
            parent2.classList.add("active"); // li
            const parent3 = parent2.parentElement;
            if (parent3) {
                parent3.classList.add("active"); // li
                const parent4 = parent3.parentElement;
                if (parent4) {
                    parent4.classList.add("active"); // li
                    const parent5 = parent4.parentElement;
                    if (parent5) {
                        parent5.classList.add("active"); // li
                        const parent6 = parent5.parentElement;
                        if (parent6) {
                            parent6.classList.add("active"); // li
                        }
                    }
                }
            }
        }
        return false;
    };

    render() {
        if(this.state.UserDetails.fld_userid != undefined){
        return (
            <React.Fragment>
                <div className="topnav">
                    <div className="container" >
                        <nav className="navbar navbar-light navbar-expand-lg topnav-menu" id="navigation">
                            <Collapse isOpen={this.props.menuOpen} className="navbar-collapse" id="topnav-menu-content">
                                <ul className="navbar-nav">
                                    <li className="nav-item dropdown">
                                        <Link className="nav-link dropdown-toggle arrow-none" >
                                            <i className="bx bxs-group mr-2"></i>{this.props.t('Monthly Collection')} {this.props.menuOpen}<div className="arrow-down"></div>
                                        </Link>
                                        <div className={classname("dropdown-menu", { show: this.state.isDashboard })}>
                                           
                                          

                                            <li className="nav-item dropdown">
                                            <Link className="nav-link dropdown-toggle arrow-none">
                                                <i className="bx bxs-group mr-2"></i>{this.props.t('Manage Clients')} {this.props.menuOpen}<div className="arrow-down"></div>
                                            </Link>
                                            <div className={classname("dropdown-menu", { show: this.state.isDashboard })}>

                                            <Link to="/add-clients" className="dropdown-item">{this.props.t('Add New Clients') }</Link>
                                            <Link to="/view-clients" className="dropdown-item">{this.props.t('View Clients') }</Link>
                                            </div>
                                        </li>

                                          
                                            <Link to="/acknowledgement" className="dropdown-item">{this.props.t('Transactions') }</Link>
                                            <Link to="/register-counstomer" className="dropdown-item">{this.props.t('Registered Customers') }</Link>
                                            


                                        <li className="nav-item dropdown">
                                            <Link className="nav-link dropdown-toggle arrow-none" >
                                                <i className="bx bxs-wallet mr-2"></i>{this.props.t('Offline Payment')} {this.props.menuOpen}<div className="arrow-down"></div>
                                            </Link>
                                            <div className={classname("dropdown-menu", { show: this.state.isDashboard })}>
                                                <Link to="daily-payment" className="dropdown-item">{this.props.t('Payment') }</Link>
                                                <Link to="/offline-acknowledgement" className="dropdown-item">{this.props.t('Acknowledment') }</Link>
                                            </div>
                                        </li>



                                            <li className="nav-item dropdown">
                                                <Link className="nav-link dropdown-toggle arrow-none">
                                                    <i className="bx bx-x-circle mr-2"></i>{this.props.t('Close Account')} {this.props.menuOpen}<div className="arrow-down"></div>
                                                </Link>
                                                <div className={classname("dropdown-menu", { show: this.state.isDashboard })}>
                                                    <Link to="/close-account" className="dropdown-item">{this.props.t('Close Account') }</Link>
                                                    <Link to="/close-account-list" className="dropdown-item">{this.props.t('Closed Account List') }</Link>
                                                </div>
                                            </li>

                                        </div>
                                    </li>


{/*
                                    <li className="nav-item ">
                                        <Link to="/acknowledgement" className="nav-link dropdown-toggle arrow-none">
                                            <i className="bx bx-stats mr-2"></i>{this.props.t('Transactions')} 
                                        </Link>
                                    </li>

                                    <li className="nav-item ">
                                        <Link to="/register-counstomer" className="nav-link dropdown-toggle arrow-none">
                                            <i className="bx bxs-data mr-2"></i>{this.props.t('Registered Customers')} 
                                        </Link>
                                    </li>


                                    <li className="nav-item dropdown">
                                        <Link className="nav-link dropdown-toggle arrow-none" >
                                            <i className="bx bxs-wallet mr-2"></i>{this.props.t('Offline Payment')} {this.props.menuOpen}<div className="arrow-down"></div>
                                        </Link>
                                        <div className={classname("dropdown-menu", { show: this.state.isDashboard })}>
                                            <Link to="daily-payment" className="dropdown-item">{this.props.t('Payment') }</Link>
                                            <Link to="/offline-acknowledgement" className="dropdown-item">{this.props.t('Acknowledment') }</Link>
                                        </div>
                                    </li>*/}

                                    <li className="nav-item dropdown">
                                        <Link className="nav-link dropdown-toggle arrow-none" >
                                            <i className="bx bxs-folder-open mr-2"></i>{this.props.t('Daily Collection')} {this.props.menuOpen}<div className="arrow-down"></div>
                                        </Link>
                                        <div className={classname("dropdown-menu", { show: this.state.isDashboard })}>

                                        <li className="nav-item dropdown">
                                            <Link className="nav-link dropdown-toggle arrow-none">
                                                <i className="bx bxs-group mr-2"></i>{this.props.t('Manage Clients')} {this.props.menuOpen}<div className="arrow-down"></div>
                                            </Link>
                                            <div className={classname("dropdown-menu", { show: this.state.isDashboard })}>
                                                <Link to="/add-clients-daily" className="dropdown-item">{this.props.t('Add New Client') }</Link>
                                                <Link to="/view-clients-daily" className="dropdown-item">{this.props.t('View Clients') }</Link>
                                            </div>
                                        </li>

                                            <Link to="/daily-collection-payment" className="dropdown-item">{this.props.t('Payment') }</Link>
                                            <Link to="/scanQR" className="dropdown-item">{this.props.t('ScanQR') }</Link>
                                            <Link to="/collection-list" className="dropdown-item">{this.props.t('CollectionList') }</Link>
                                        </div>
                                    </li>

                                    {/*<li className="nav-item dropdown">
                                        <Link className="nav-link dropdown-toggle arrow-none">
                                            <i className="bx bx-x-circle mr-2"></i>{this.props.t('Close Account')} {this.props.menuOpen}<div className="arrow-down"></div>
                                        </Link>
                                        <div className={classname("dropdown-menu", { show: this.state.isDashboard })}>
                                            <Link to="/close-account" className="dropdown-item">{this.props.t('Close Account') }</Link>
                                            <Link to="/close-account-list" className="dropdown-item">{this.props.t('Closed Account List') }</Link>
                                        </div>
                                    </li>*/}



                                    <li className="nav-item ">
                                        <Link to="/" className="nav-link dropdown-toggle arrow-none">
                                            <i className="bx bx-power-off text-danger mr-2"></i>{this.props.t('Logout')} 
                                        </Link>
                                    </li>


                                </ul>
                            </Collapse>
                        </nav>
                    </div>
                </div>
            </React.Fragment>
       
        );
    }else
    {
        return (
       
            <React.Fragment>
            <div className="topnav">
                <div className="container" >
                    <nav className="navbar navbar-light navbar-expand-lg topnav-menu" id="navigation">
                        <Collapse isOpen={this.props.menuOpen} className="navbar-collapse" id="topnav-menu-content">
                            <ul className="navbar-nav">
                             
                                   
                                       
                        
                                    <li className="nav-item "><Link
                                    className="nav-link dropdown-toggle arrow-none"
                                    to="/dashboard">
                                        <i className="bx bx-home-circle"></i>
                                        <span>{this.props.t('Dashboard') }</span></Link></li>

                                        <li className="nav-item ">
                                    <Link to="/chits" className="nav-link dropdown-toggle arrow-none">
                                        <i className="bx bxs-wallet"></i>{this.props.t('Chits')} 
                                    </Link>
                                </li> 


                                <li className="nav-item ">
                                    <Link to="/" className="nav-link dropdown-toggle arrow-none">
                                        <i className="bx bx-power-off text-danger mr-2"></i>{this.props.t('Logout')} 
                                    </Link>
                                </li>


                            </ul>
                        </Collapse>
                    </nav>
                </div>
            </div>
        </React.Fragment>
   

       ); 
    }
    }
}

export default withRouter(withNamespaces()(Navbar));
