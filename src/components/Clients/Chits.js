import React, { Component } from 'react'
import Breadcrumbs from '../Common/Breadcrumb'
import { Container, Row, Col, Button, Card, CardBody, CardTitle, CardSubtitle, Modal, ModalHeader, ModalBody, ModalFooter, Media, Table } from "reactstrap";
import { MDBDataTable } from "mdbreact";
import Notiflix from "notiflix";

import GetApiCall from '../../API/GetAPI';
import PostApiCall from '../../API/PostAPI'



 class Chits extends Component {
  constructor(props) {
    super(props);
    this.state = {
        accountNo:'',
        ChitSchema:[], 
        dropdownArray:[]    
     };
    }

     componentDidMount=()=>{  
      
      Notiflix.Loading.Init({
        svgColor : '#880105'
       
      });

      // Notiflix.Loading.Dots('Please wait...');
      var det=localStorage.getItem('loginedUser')
      var detail=JSON.parse(det)
      console.log(detail[0])

      PostApiCall.postRequest(
        {
          id:detail[0].fld_id
          },
        "Get_Client_ChitScheme"
      ).then((results) =>
        results.json().then((obj) => {
          if (results.status == 200 || results.status == 201) {
             this.setState({
              ChitSchema : obj.data,
                            
            })
            Notiflix.Loading.Remove();

         
        }}))


    
      // GetApiCall.getRequest("Get_New_Client_chitSchema").then(resultdes =>
      //   resultdes.json().then(obj => {
      //     // console.log(obj.data)
      //       this.setState({
      //           ChitSchema:obj.data
      //   })
      //   Notiflix.Loading.Remove();
      // })).then(res=>{
      //   let tempArray=[]
      //   this.state.ChitSchema.map(data=>{
      //     tempArray.push({value:data.fld_accountno,label:data.fld_accountno})
      //   })
      //  this.setState({dropdownArray:tempArray})
      
      // })


      // GetApiCall.getRequest("Get_New_ClientChitSchemaData").then(resultdes =>
      //   resultdes.json().then(obj => {
      //     // console.log(obj.data)
      //       this.setState({
      //           allDetails:obj.data
      //   })
      //   Notiflix.Loading.Remove();
      // }))
    

    }

    viewdetail(data){
      
        localStorage.setItem('ChitDetails',JSON.stringify(data))
        this.props.history.push(`/viewchitdetails/${
          data.id}`)
     }


    render() {

// console.log(this.state.ChitSchema);
      let rowData=  this.state.ChitSchema.map(info=>{
        return{ name:info.fld_name,
          accountNo:info.fld_accountno,
          chit_scheme:info.fld_chitschema,
          duration:info.fld_duration,
          amount:info.fld_chitamount,
         id:info.fld_id}
     })
        

        const data = {
            columns: [
              {
                label: "S.No",
                field: "s_no",
                sort: "asc",
                width: 150
              },              
              {
                label: "Account No",
                field: "accountNo",
                sort: "asc",
                width: 100
              } ,               
              {
                label: "Chit Scheme",
                field: "chit_scheme",
                sort: "asc",
                width: 150
              },
              {
                label: "Duration",
                field: "duration",
                sort: "asc",
                width: 150
              },
              {
                label: "Installement Amount",
                field: "amount",
                sort: "asc",
                width: 150
              },
             
             
            ],
            rows:rowData.filter(data=>{
              if(this.state.accountNo==''){
                 return data
              }
              else if(data.accountNo==this.state.accountNo){
                 return data
              }
            }).map((data,i)=>{
                  return  {
                    s_no:i,
                    accountNo:data.accountNo,
                    chit_scheme:data.chit_scheme,
                    amount:data.amount,
                    duration:data.duration,
             
                      clickEvent:() =>{this.viewdetail(data)},
                    payNow:<button className='btn btn-info'>Pay Now</button>
                }
            })
           
          };

         {this.state.accountNo!==''&&data.columns.push( { 
            label: "Pay Now",
            field: "payNow",
            sort: "asc",
            width: 100
          })} 
        return (
            <React.Fragment>
            <div className="page-content">
            
                <Container fluid>
                    <div className="px-3">
                        <Breadcrumbs title={'Manage Clients'} breadcrumbItem={'Clients Details'} />
                    </div>
                    {/* Render Breadcrumb */}
                   
                        
                    <React.Fragment>
                    <div className="page-content p-0 m-0">
                      <div className="container">
            
            
                        <Row>
                          <Col className="col-12">
                            <Card>
                              <CardBody>
                          <CardTitle>{this.state.accountNo!==''?'Current Chit Detail\'s':"Chit Details"}</CardTitle>
                              <span className='d-flex justify-content-end'> 
                                    </span>
                                <MDBDataTable responsive striped bordered data={data} />
            
                              </CardBody>
                            </Card>
                          </Col>
                        </Row>
                      </div>
                    </div>
                  </React.Fragment>
                </Container>
            </div>
      
            </React.Fragment>
   
        )
    }
}
export default Chits