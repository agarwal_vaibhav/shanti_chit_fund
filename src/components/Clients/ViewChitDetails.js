import React, { Component } from 'react'
import Breadcrumbs from '../Common/Breadcrumb'
import { Container, Row, Col, Button, Card, CardBody, CardTitle, CardSubtitle, Modal, ModalHeader, ModalBody, ModalFooter, Media, Table } from "reactstrap";
import { MDBDataTable } from "mdbreact";
import Notiflix from "notiflix";
import GetApiCall from '../../API/GetAPI';
import PostApiCall from '../../API/PostAPI';


export default class ViewChitDetails extends Component {
  state={
    ChitSchema:[],
    Details:[],
    ChitData:[],
    AccountNo:'',
    Duration:'',
    Amount:'',
    ChitSchemas:''
  }

  
  componentDidMount=()=>{  
      
    Notiflix.Loading.Init({
      svgColor : '#880105'
     
    });

    // {"name":"Shivi","accountNo":"SR52616626","chit_scheme":"Thanga Mazhai","duration":"15","amount":"10000"}

    // var abc= localStorage.getItem('ChitDetails')

    const det={
        fld_id:this.props.match.params.chitid
    }

     localStorage.setItem('ChitDetails',JSON.stringify(det))

      console.log(this.props.match.params.chitid)


      Notiflix.Loading.Dots('Please wait...');

      PostApiCall.postRequest(
        {
          id:this.props.match.params.chitid
          },
        "Get_Approval_ChitData"
      ).then((results) =>
        results.json().then((obj) => {
          if (results.status == 200 || results.status == 201) {
             this.setState({
                ChitData : obj.data,
                AccountNo:obj.data[0].fld_accountno,
                Duration:obj.data[0].fld_duration,
                Amount:obj.data[0].fld_chitamount,
                ChitSchemas:obj.data[0].fld_chitschema
                                
            })
            // console.log(obj.data[0].fld_accountno)
          
        }}))

       


    Notiflix.Loading.Dots('Please wait...'); 
  
    GetApiCall.getRequest("Get_New_Client_chitSchema").then(resultdes =>
      resultdes.json().then(obj => {
        // console.log(obj.data)
          this.setState({
              ChitSchema:obj.data,
             
      })
      Notiflix.Loading.Remove();
    }))

  

  

  }


//   fld_accountno: "SR52616626"
// fld_chitamount: "10000"
// fld_chitschema: "Thanga Mazhai"
// fld_client: 46
// fld_clientStatus: "Approved"
// fld_duration: "15"
// fld_id: 36
// fld_paidmonths: ""
// fld_updatedby: 1
// fld_updatedon: "Dec 5, 2020 12:35 PM"
// fld_user: "New"



    render() {

        const data = {
            columns: [
              {
                label: "S.No",
                field: "s_no",
                sort: "asc",
                width: 150
              },              
              {
                label: "Account No",
                field: "accountNo",
                sort: "asc",
                width: 100
              },              
              {
                label: "Chit Scheme",
                field: "chit_scheme",
                sort: "asc",
                width: 150
              },
           
             
              {
                label: "Duration(months)",
                field: "duration",
                sort: "asc",
                width: 150
              },
              {
                label: "Installment Number",
                field: "installmentnumber",
                sort: "asc",
                width: 150

              },
              {
                label: "Installment Amount",
                field: "amount",
                sort: "asc",
                width: 150

              },
              {
                label: "Date",
                field: "date",
                sort: "asc",
                width: 150

              },
              {
                label: "Status",
                field: "status",
                sort: "asc",
                width: 150

              }
             
            ],
            rows: this.state.ChitSchema.map((info,i)=>{
              return{ name:info.fld_name,
                accountNo:info.fld_accountno,
                chit_scheme:info.fld_chitschema,
                duration:info.fld_duration,
                installmentnumber:"234",
                amount:info.fld_chitamount,
                date:"11-12-2020",
                status:"Paid",

               id:info.fld_id,
               s_no:i}
           })
           
          };
        return (
            <React.Fragment>
            <div className="page-content">
            
                <Container fluid>
                    <div className="px-3">
                        <Breadcrumbs title={'Manage Clients'} breadcrumbItem={'View Chit Details'} />
                    </div>
                    {/* Render Breadcrumb */}
                   
                        
                    <React.Fragment>
                    <div className="page-content p-0 m-0">
                      <div className="container">
                     
                      <Row>
                      <Col className="col-12">
                            <Card style={{background: 'rgba(136, 1, 5, 0.3)', border: '1px solid #880105'}}>
                              <CardBody>
                              <div className="row">
                                <div className="col-6"><p style={{fontSize: '15px', color:'#880105',}}>Account Number : <span style={{color:'#fff'}}>{this.state.AccountNo}</span></p></div>
                                <div className="col-6"><p style={{fontSize: '15px', color:'#880105',}}>Duration : <span style={{color:'#fff'}}>{this.state.Duration}</span></p></div>
                               
                                <div className="col-6"><p style={{fontSize: '15px', color:'#880105',}}> Chit Scheme : <span style={{color:'#fff'}} >{this.state.ChitSchemas}</span></p></div>
                                <div className="col-6"><p style={{fontSize: '15px', color:'#880105',}}> Amount : <span style={{color:'#fff'}}>{this.state.Amount}</span></p></div>
                               </div>
                              </CardBody>
                            </Card>
                          </Col>
                      </Row>
                      
                      <Row>
                      <Col className="col-12">
                        <Card>
                          <CardBody>
                            <CardTitle>Chit Details</CardTitle>
                           <table className="table table-border">
                              <thead>
                              <tr>
                                <th>Account Number</th>
                                <th>Name</th>
                                <th>Chit Scheme</th>
                                <th>Installment Number</th>
                                <th>Installment Amount</th>
                                <th>Pay Now</th>
                              </tr>
                              </thead>
                              <tbody>
                                <tr>
                                   <td>12464646</td>
                                   <td></td>
                                   <td></td>
                                   <td></td>
                                   <td></td>
                                   <td><button className='btn btn-info'>Pay Now</button></td>
                                </tr>
                              
                              </tbody>
                           </table>
                          </CardBody>
                        </Card>
                      </Col>
                    </Row>
            
            
                        <Row>
                          <Col className="col-12">
                            <Card>
                              <CardBody>
                                <CardTitle>Payment History</CardTitle>
                                <CardSubtitle className="mb-3">
                                    List of all the payment.
                                </CardSubtitle>
            
                                <MDBDataTable responsive striped bordered data={data} />
            
                              </CardBody>
                            </Card>
                          </Col>
                        </Row>
                      </div>
                    </div>
                  </React.Fragment>
                </Container>
            </div>
      
            </React.Fragment>
   
      
        )
    }
}