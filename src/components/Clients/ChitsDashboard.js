import React, { Component } from 'react'
import Breadcrumbs from '../Common/Breadcrumb'
import { Container, Row, Col, Button, Card, CardBody, CardTitle, CardSubtitle, Modal, ModalHeader, ModalBody, ModalFooter, Media, Table } from "reactstrap";
import { MDBDataTable } from "mdbreact";
import Notiflix from "notiflix";
import GetApiCall from '../../API/GetAPI';
import PostApiCall from '../../API/PostAPI'


export default class ChitsDashboard extends Component {
  state={
    ChitSchema:[],
    Details:[]
  }

  
  componentDidMount=()=>{  
      
    Notiflix.Loading.Init({
      svgColor : '#880105'
     
    });

    // var det=localStorage.getItem('loginedUser')
    // var detail=JSON.parse(det)
    // this.setState({
    //   Details:detail[0]
    // })

    Notiflix.Loading.Dots('Please wait...'); 
  
    var det=localStorage.getItem('loginedUser')
      var detail=JSON.parse(det)
      this.setState({
      Details:detail[0]
    })
      // console.log(detail[0])

      PostApiCall.postRequest(
        {
          id:detail[0].fld_id
          },
        "Get_Client_ChitScheme"
      ).then((results) =>
        results.json().then((obj) => {
          if (results.status == 200 || results.status == 201) {
             this.setState({
              ChitSchema : obj.data,
                
            })
            Notiflix.Loading.Remove();

            
        }}))


   
  }



    render() {

        const data = {
            columns: [
              {
                label: "S.No",
                field: "s_no",
                sort: "asc",
                width: 150
              },              
              {
                label: "Account No",
                field: "accountNo",
                sort: "asc",
                width: 100
              },              
              {
                label: "Chit Scheme",
                field: "chit_scheme",
                sort: "asc",
                width: 150
              },
           
             
              {
                label: "Installment",
                field: "amount",
                sort: "asc",
                width: 150
              },
             
            ],
            rows: this.state.ChitSchema.map((info,i)=>{
              return{ name:info.fld_name,
                accountNo:info.fld_accountno,
                chit_scheme:info.fld_chitschema,
                duration:info.fld_duration,
                amount:info.fld_chitamount,
               id:info.fld_id,
               s_no:i}
           })
           
          };
        return (
            <React.Fragment>
            <div className="page-content">
            
                <Container fluid>
                    <div className="px-3">
                        <Breadcrumbs title={'Manage Clients'} breadcrumbItem={'Dashboard'} />
                    </div>
                    {/* Render Breadcrumb */}
                   
                        
                    <React.Fragment>
                    <div className="page-content p-0 m-0">
                      <div className="container">
                      <h4>Welcome Shivani Rawat</h4>
                      <Row>
                      <Col className="col-12">
                            <Card style={{background: 'rgba(136, 1, 5, 0.3)', border: '1px solid #880105'}}>
                              <CardBody>
                              <div className="row">
        <div className="col-6"><p style={{fontSize: '15px', color:'#880105',}}> Name : <span style={{color:'#fff'}}>{this.state.Details.fld_name}</span></p></div>
                                <div className="col-6"><p style={{fontSize: '15px', color:'#880105',}}> Email : <span style={{color:'#fff'}}>{this.state.Details.fld_email}</span></p></div>
                                <div className="col-6"><p style={{fontSize: '15px', color:'#880105',}}> Mobile : <span style={{color:'#fff'}}>{this.state.Details.fld_mobile}</span></p></div>
                                <div className="col-6"><p style={{fontSize: '15px', color:'#880105',}}> Status : <span style={{color:'#fff'}}>Approved</span></p></div>
                                <div className="col-6"><p style={{fontSize: '15px', color:'#880105', marginBottom: '0'}}> Registered On : <span style={{color:'#fff'}}>{this.state.Details.fld_updatedon}</span></p></div>
                              </div>
                              </CardBody>
                            </Card>
                          </Col>
                      </Row>
                      
            
            
                        <Row>
                          <Col className="col-12">
                            <Card>
                              <CardBody>
                                <CardTitle>Chit Details</CardTitle>
                                <CardSubtitle className="mb-3">
                                    List of all the approved clients.
                                </CardSubtitle>
            
                                <MDBDataTable responsive striped bordered data={data} />
            
                              </CardBody>
                            </Card>
                          </Col>
                        </Row>
                      </div>
                    </div>
                  </React.Fragment>
                </Container>
            </div>
      
            </React.Fragment>
   
      
        )
    }
}