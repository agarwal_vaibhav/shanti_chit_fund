import React, { Component } from "react";

// MetisMenu
import MetisMenu from "metismenujs";
import { withRouter } from "react-router-dom";
import { Link } from "react-router-dom";

//i18n
import { withNamespaces } from 'react-i18next';

class SidebarContent extends Component {

    constructor(props) {
        super(props);
        this.state = {
UserDetails : []
        };
    }

    componentDidMount() {
        this.initMenu();

        var isAuthenticated=JSON.parse(localStorage.getItem('loginedUser'));

        this.setState({...this.state,
            UserDetails : isAuthenticated[0]
            })
    }

    componentDidUpdate(prevProps) {
        if (this.props.type !== prevProps.type) {
            this.initMenu();
        }
    }

    initMenu() {
            new MetisMenu("#side-menu");

            var matchingMenuItem = null;
            var ul = document.getElementById("side-menu");
            var items = ul.getElementsByTagName("a");
            for (var i = 0; i < items.length; ++i) {
                if (this.props.location.pathname === items[i].pathname) {
                    matchingMenuItem = items[i];
                    break;
                }
            }
            if (matchingMenuItem) {
                this.activateParentDropdown(matchingMenuItem);
            }
    }

    activateParentDropdown = item => {
        item.classList.add("active");
        const parent = item.parentElement;

        if (parent) {
            parent.classList.add("mm-active");
            const parent2 = parent.parentElement;

            if (parent2) {
                parent2.classList.add("mm-show");

                const parent3 = parent2.parentElement;

                if (parent3) {
                    parent3.classList.add("mm-active"); // li
                    parent3.childNodes[0].classList.add("mm-active"); //a
                    const parent4 = parent3.parentElement;
                    if (parent4) {
                        parent4.classList.add("mm-active");
                    }
                }
            }
            return false;
        }
        return false;
    };

    render() {

        if(this.state.UserDetails.fld_userid != undefined){
            return (
                <React.Fragment>
                     <div id="sidebar-menu">
                    <ul className="metismenu list-unstyled" id="side-menu">
                        <li className="menu-title">{this.props.t('Menu') }</li>
                      
                                        <li><Link to="/dashboard">
                                        <i className="bx bx-home-circle"></i>
                                        <span>{this.props.t('Dashboard') }</span></Link></li>
                         
    
                        
                                    <li>
                                        <Link to="/#" className="has-arrow waves-effect">
                                            <i className="bx bxs-wallet"></i>
                                            <span>Monthly Collection</span>
                                        </Link>
                                        <ul className="sub-menu" aria-expanded="false">
    
                                        <li>
                                <Link to="/#" className="has-arrow waves-effect">
                                    <i className="bx bxs-group d-none"></i>
                                    <span>Manage Staff</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li><Link to="/add-staff">{this.props.t('Add New Staff') }</Link></li>
                                    <li><Link to="/view-staff">{this.props.t('View Staff') }</Link></li>
                                </ul>
                            </li>
    
                                           <li>
                                                <Link to="/#" className="has-arrow waves-effect">
                                                    <i className="bx bxs-group d-none"></i>
                                                    <span>Manage Clients</span>
                                                </Link>
                                                <ul className="sub-menu" aria-expanded="false">
                                                    <li><Link to="/add-clients">{this.props.t('Add New Client') }</Link></li>
                                                    <li><Link to="/view-clients">{this.props.t('View Clients') }</Link></li>
                                                </ul>
                                           </li>
    
                                            {/*
                                            <li><Link to="/add-clients">{this.props.t('Add New Client') }</Link></li>
                                            <li><Link to="/view-clients">{this.props.t('View Clients') }</Link></li>*/}
                                            
                                            <li><Link to="/acknowledgement">
                                            <i className="bx bx-stats d-none"></i>
                                            <span>{this.props.t('Transactions') }</span></Link></li>
    
                                            <li><Link to="/register-customers">
                                            <i className="bx bxs-data d-none"></i>
                                            <span>{this.props.t('Registered Customers') }</span></Link></li>
                                            {/*
                                            <li><Link to="daily-payment">{this.props.t('Payment') }</Link></li>
                                            <li><Link to="/offline-acknowledgement">{this.props.t('Acknowledgement') }</Link></li>*/}
    
    
                                            <li>
                                            <Link to="/#" className="has-arrow waves-effect">
                                                <i className="bx bxs-wallet d-none"></i>
                                                <span>Offline Payment</span>
                                            </Link>
                                            <ul className="sub-menu" aria-expanded="false">
                                                <li><Link to="daily-payment">{this.props.t('Payment') }</Link></li>
                                                <li><Link to="/offline-acknowledgement">{this.props.t('Acknowledgement') }</Link></li>
                                            </ul>
                                            </li>
    
                                    {      /* <li><Link to="/close-account">{this.props.t('Close Account') }</Link></li>
                                        <li><Link to="/close-account-list">{this.props.t('Closed Account List') }</Link></li> */}
    
                                            <li>
                                            <Link to="/#" className="has-arrow waves-effect">
                                                <i className="bx bx-x-circle d-none"></i>
                                                <span>Close Account</span>
                                            </Link>
                                            <ul className="sub-menu" aria-expanded="false">
                                                <li><Link to="/close-account">{this.props.t('Close Account') }</Link></li>
                                                <li><Link to="/close-account-list">{this.props.t('Closed Account List') }</Link></li>
                                            </ul>
                                            </li>
    
    
                                        </ul>
                                    </li>
    
    
    
    
                       {/* <li>
                            <Link to="/#" className="has-arrow waves-effect">
                                <i className="bx bxs-group"></i>
                                <span>Manage Clients</span>
                            </Link>
                            <ul className="sub-menu" aria-expanded="false">
                                <li><Link to="/add-clients">{this.props.t('Add New Client') }</Link></li>
                                <li><Link to="/view-clients">{this.props.t('View Clients') }</Link></li>
                            </ul>
                       </li> */}
                       
                        {/*<li>
                            <Link to="/#" className="has-arrow waves-effect">
                                <i className="bx bx-user-circle"></i>
                                <span>User Managment</span>
                            </Link>
                            <ul className="sub-menu" aria-expanded="false">
                                <li><Link to="/add-user">{this.props.t('Add User') }</Link></li>
                                <li><Link to="/view-user">{this.props.t('View User') }</Link></li>
                                <li><Link to="#">{this.props.t('User Rights') }</Link></li>
                            </ul>
                        </li>*/}
    
    
                        {/*<li><Link to="/acknowledgement">
                        <i className="bx bx-stats"></i>
                        <span>{this.props.t('Transactions') }</span></Link></li>
    
                        <li><Link to="/register-counstomer">
                        <i className="bx bxs-data"></i>
                    <span>{this.props.t('Registered Customers') }</span></Link></li>*/}
    
                    {/* <li>
                        <Link to="/#" className="has-arrow waves-effect">
                            <i className="bx bxs-wallet"></i>
                            <span>Offline Payment</span>
                        </Link>
                        <ul className="sub-menu" aria-expanded="false">
                            <li><Link to="daily-payment">{this.props.t('Payment') }</Link></li>
                            <li><Link to="/offline-acknowledgement">{this.props.t('Acknowledgement') }</Link></li>
                        </ul>
                    </li>*/}
    
                        
                        <li>
                            <Link to="/#" className="has-arrow waves-effect">
                                <i className="bx bxs-folder-open"></i>
                                <span>Daily Collection</span>
                            </Link>
                            <ul className="sub-menu" aria-expanded="false">
    
    
                          
    
                            <li>
                                <Link to="/#" className="has-arrow waves-effect">
                                    <i className="bx bxs-group d-none"></i>
                                    <span>Manage Clients</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li><Link to="/add-clients-daily">{this.props.t('Add New Client') }</Link></li>
                                    <li><Link to="/view-clients-daily">{this.props.t('View Clients') }</Link></li>
                                </ul>
                            </li>
    
    
    
                                {/*<li><Link to="/add-clients-daily">{this.props.t('Add New Client') }</Link></li>
                <li><Link to="/view-clients-daily">{this.props.t('View Clients') }</Link></li>*/}
                                
                                <li><Link to="/daily-collection-payment">{this.props.t('Payment') }</Link></li>
                                <li><Link to="/scanQR">{this.props.t('ScanQR') }</Link></li>
                               
                                {/*<li><Link to="/payment-acknowledgement">{this.props.t('Acknowledgement') }</Link></li>*/}
                                <li><Link to="/collection-list">{this.props.t('Collection List') }</Link></li>
                            </ul>
                        </li>
    
    {                    /*<li>
                            <Link to="/#" className="has-arrow waves-effect">
                                <i className="bx bx-x-circle"></i>
                                <span>Close Account</span>
                            </Link>
                            <ul className="sub-menu" aria-expanded="false">
                                <li><Link to="/close-account">{this.props.t('Close Account') }</Link></li>
                                <li><Link to="/close-account-list">{this.props.t('Closed Account List') }</Link></li>
                            </ul>
                        </li>*/}
    
                         
                            {/* <li>
                                <Link to="/dashboard-c" className="">
                                    <i className="bx bxs-group d-none"></i>
                                    <span>Dashboard C</span>
                                </Link>
                               
                            </li>
                            <li>
                                <Link to="/chits" className="">
                                    <i className="bx bxs-group d-none"></i>
                                    <span>Chits C</span>
                                </Link>
                               
                            </li> */}
                        
                        <li><Link 
                        // to="/" 
                        onClick={()=>{
                            localStorage.removeItem('loginedUser')
                            // this.props.history.push('/login')
                            window.location.href = '/login'
                        }}
                        >
                        <i className="bx bx-power-off text-danger"></i>
                        <span>{this.props.t('Logout') }</span></Link></li>
                        
    
                        {/*
                        <li><Link to="/Login">
                        <i className="bx bx-stats"></i>
                        <span>{this.props.t('Login') }</span></Link></li>*/}
                        
                    </ul>
                </div>
                </React.Fragment>
            );
        }else
        {
            return (
                <React.Fragment>
                     <div id="sidebar-menu">
                    <ul className="metismenu list-unstyled" id="side-menu">
                        <li className="menu-title">{this.props.t('Menu') }</li>
                      
                                     
    {                    /*<li>
                            <Link to="/#" className="has-arrow waves-effect">
                                <i className="bx bx-x-circle"></i>
                                <span>Close Account</span>
                            </Link>
                            <ul className="sub-menu" aria-expanded="false">
                                <li><Link to="/close-account">{this.props.t('Close Account') }</Link></li>
                                <li><Link to="/close-account-list">{this.props.t('Closed Account List') }</Link></li>
                            </ul>
                        </li>*/}
    
                         
                                 <li><Link to="/dashboard">
                                        <i className="bx bx-home-circle"></i>
                                        <span>{this.props.t('Dashboard') }</span></Link></li>
                       

                                <li>
                                        <Link to="/chits" >
                                            <i className="bx bxs-wallet"></i>
                                            <span>{this.props.t('Chits') }</span>
                                        </Link>
                               
                            </li>
                        
                        <li><Link 
                        // to="/" 
                        onClick={()=>{
                            localStorage.removeItem('loginedUser')
                            // this.props.history.push('/login')
                            window.location.href = '/login'
                        }}
                        >
                        <i className="bx bx-power-off text-danger"></i>
                        <span>{this.props.t('Logout') }</span></Link></li>
                        
    
                        {/*
                        <li><Link to="/Login">
                        <i className="bx bx-stats"></i>
                        <span>{this.props.t('Login') }</span></Link></li>*/}
                        
                    </ul>
                </div>
                </React.Fragment>
            );
        }
       
    }
}

export default withRouter(withNamespaces()(SidebarContent));
