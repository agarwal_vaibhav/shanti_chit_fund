import React from "react";
import { Redirect } from "react-router-dom";

  // Dashboard
import Dashboard from "../pages/Dashboard/index";
import ViewClients from "../pages/ViewClients";
import Acknowledment from "../pages/Acknowledment";
import RegisterCounstomer from "../pages/RegisterCounstomer";
import OfflinePayment from "../pages/OfflinePayment";
import CloseAccount from "../pages/CloseAccount";
import AddClients from "../pages/AddClients";
import DailyCollection from "../pages/DailyCollection";
import DailyPayment from "../pages/DailyPayment";
import OfflineAcknowledment from "../pages/OfflineAcknowledment";
import PaymentAcknowledment from "../pages/PaymentAcknowledment";
import CloseAccountList from "../pages/CloseAccountList";
import RegistrationApproval from "../pages/RegistrationApproval";
import DailyCollectionPayment from "../pages/DailyCollectionPayment";
import CollectionList from "../pages/CollectionList";
import Login from "../pages/Login";
import App from "../App";
import ViewUser from "../pages/ViewUser";
import AddUser from "../pages/AddUser";
import EditClient from "../pages/EditClient";
import EditUser from "../pages/EditUser";
import AddClientDaily from "../pages/AddClientDaily";
import ViewClientsDaily from "../pages/ViewClientsDaily";
import EditClientDaily from "../pages/EditClientDaily";
import ScanQR from "../pages/ScanQR";
import AddStaff from "../pages/AddStaff";
import Chits from "../components/Clients/Chits";
import ChitsDashboard from "../components/Clients/ChitsDashboard";
import ViewStaff from "../pages/ViewStaff";
import ViewChitDetails from "../components/Clients/ViewChitDetails";


const authProtectedRoutes = [
	{ path: "/dashboard", component: Dashboard },
	{ path: "/view-clients", component: ViewClients },
	{ path: "/acknowledgement", component: Acknowledment },
	{ path: "/register-customers", component: RegisterCounstomer },
	{ path: "/offline-payment", component: OfflinePayment },
	{ path: "/close-account", component: CloseAccount },
	{ path: "/add-clients", component: AddClients },
	{ path: "/daily-collection", component: DailyCollection},
	{ path: "/daily-payment", component: DailyPayment},
	{ path: "/offline-acknowledgement", component: OfflineAcknowledment},
	{ path: "/payment-acknowledgement", component: PaymentAcknowledment},
	{ path: "/close-account-list", component: CloseAccountList},
	{ path: "/registration-approval", component: RegistrationApproval},
	{ path: "/daily-collection-payment", component: DailyCollectionPayment},
	{ path: "/collection-list", component: CollectionList},
	{ path: "/view-user", component: ViewUser},	
	{ path: "/add-user", component: AddUser},	
	{ path: "/edit-client", component: EditClient},	
	{ path: "/edit-user", component: EditUser},	
	{ path: "/add-clients-daily", component: AddClientDaily},
	{ path: "/add-staff", component: AddStaff},
	{ path: "/view-staff", component: ViewStaff},
	{ path: "/view-clients-daily", component: ViewClientsDaily},	
	{ path: "/edit-clients-daily", component: EditClientDaily},	
	{ path: "/scanQR", component: ScanQR},	
	
		
		// this route should be at the end of all other routes
	{ path: "/", exact: true, component: () => <Redirect to="/dashboard" /> }
];

const publicRoutes = [
	// { path: "/logout", component: Logout },
	{ path: "/login", component: Login },

];

const authProtectedRoutesClient = [

	{ path: "/dashboard", component: ChitsDashboard},	
	{ path: "/chits", component: Chits},
	{ path: "/viewchitdetails/:chitid", component: ViewChitDetails},	
	

			// this route should be at the end of all other routes
			{ path: "/", exact: true, component: () => <Redirect to="/dashboard" /> }
		];

export { authProtectedRoutes, publicRoutes,authProtectedRoutesClient };
